<?php
//4.5抽象类
/**如果一个类里面有抽象方法，那么必须是抽象类 //如果一个类不是抽象类，那么必须实现抽象方法
 * 
 */
abstract class animal{
    //在抽象类中说明一个抽象类
    abstract public function shout();
}
//定义dog类继承animal类
class dog extends animal{
    //实现抽象方法
     public function shout(){
        echo "汪汪汪......<br>";
    }
}
//定义cat类继承animal类
class cat extends animal{
    public function shout(){
         echo "喵喵喵......<br>";
    }
}

$dog = new dog();
$dog->shout();
$cat = new cat();
$cat->shout();

?>