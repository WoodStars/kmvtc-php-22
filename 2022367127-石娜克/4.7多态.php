<?php

abstract class animal{
    //在抽象类中说明一个抽象类
    abstract public function shout();
}
//定义dog类,实现抽象类中的方法
class dog extends animal{
    //实现抽象方法
     public function shout(){
        echo "汪汪汪......<br>";
    }
}
//定义cat类,实现抽象类中的方法
class cat extends animal{
    public function shout(){
         echo "喵喵喵......<br>";
    }
}

function animalshout($obj){
    if($obj instanceof animal){
        $obj->shout();
    }else{
        echo "Error:对象错误!<br>";
    }
    
}
$dog = new dog();
$cat = new cat();
animalshout($dog);
animalshout($cat);
animalshout($crow);


?>