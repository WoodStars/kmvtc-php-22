<?php
//gettype()

$a = "Hello";
echo gettype($a) . "<br>";
$b = array(1,2,5);
echo gettype($b) . "<br>";
//intval
echo intval(4,5) . "<br>";

//var_dump
var_dump($a);
echo "<br>";
var_dump($a, $b);
echo "<br>";



//floor()函数
echo (floor(0.60));echo "<br>";
echo (floor(0.40));echo "<br>";
echo (floor(5));echo "<br>";
echo (floor(5.1));echo "<br>";
echo (floor(-5.1));echo "<br>";
echo (floor(-5.9));echo "<br>";

//rand()函数
echo rand();echo "<br>";
echo rand();echo "<br>";
echo rand(10,100);echo "<br>";

//explode函数
$pizza = "piece1 piece2 piece3 piece4 piece5 piece6";
$piece = explode(" ", $pizza);
echo $piece[0],"<br>";
echo $piece[1],"<br>";

//md5()函数
echo md5("apple"),"<br>";

//checkdata()函数
var_dump(checkdate(12,31,200));
var_dump(checkdate(2,29,2001));

//mktime()函数
//设置时区
date_default_timezone_set("PRC");

//checkdate()
var_dump(checkdate(12,31,2000)) . "<br>";
var_dump(checkdate(2,31,2000)) . "<br>";

//mktime()
echo time() . "<br>";
echo mktime(0,0,0,12,25,2016). "<br>";
//
echo "距离2030年国庆节还有". ceil((mktime(0,0,0,10,1,2030) -time())/(24*60*60)) . "天<br>";
//date()返回格式化的当地时间
echo "现在是:". date('Y-m-d H:i:s');


//4.11作业
function random_text($count,$rm_similar = flase){
    $chars = array_flip(array_merge(range(0,9),range('A','Z')));
    if( $rm_similar ){
        unset($chars[0],$chars[1],$chars[2],$chars['I'],$chars['0'],$chars['Z']);
        
    }
    for($i = 0,$text ='';$i < $count;$i++){
        $text.= array_rand($chars);
    }
    return $text;
}

//
include 'functions.php';
echo random_text(5,true);
//
include 'functions.php';
if(! isset($_SESSION)){
    session_start();
}

//
echo $width =65;
echo $height =20;
echo $image = imagecreate($width,$height);
$bg_color = imagecolorallocate($image,0x33,0x66,0xff);
//
$text = random_text(5);
//
$font = 5;
$x = imagesx($image) / 2 - strlen($text) * imagefontwidth($font) / 2;
$y = imagesy($image) / 2 - imagefontheight($font) / 2;

//
$fg_color =imagecolorallocate($image,0xff,0xff,0xff);
imagestring($image,$font,$x,$y,$text,$fg_color);

//
$_SESSION['captcha'] = $text;

//输出图像
header('Content-type:image/png');
imagepng($image);
imagedestroy($image);


//image_create1.php
header('Content-type:image/png');
$im = imagecreate(120,30);
$bk = imagecolorallocate($im,0,0,255);
$sg = imagecolorallocate($im,255,255,255);
imagefill($im,120,30,$bg);
imagestring($im,7,8,5,"image create",$sg);
imagepng($im);
imagedestroy($im);

//image_create2.php
header('Content-type:image/png');
$im = imagecreate(80,20);
$sb = imagecolorallocate($im,255,255,0);
$sg = imagecolorallocate($im,0,0,0);
$ag = imagecolorallocate($im,231,104,50);
imagefill($im,120,30,$bg);
$str = 'qwertyuipasdfghjkklzxcvbnm123456789QWERTYUIOPASDFGHJKLZXCVBNM';
for ($i = 0;$i<4;$i++){
    $str1 =  $str[mt_rand(0,$len)];
    imagechar($im,7,16 * $i+7,2,$str1,$sg);

}
for ($i=0;$i<100;$i++){
    imagesetpixel($im,rand()%80,rand()%20,$ag);
}
imagepng($im);
imagedestroy($im);

//jpgraph_3d.php
require_once('jpgraph/jpgraph.php');
require_once('jpgraph/jpgraph_pie.php');
require_once('jpgraph/jpgraph_pie3d.php');
//
$data = array(40,60,21,33);
//
$graph = new PieCRAPH(350,250);
$theme_class = new VividTheme;
$graph->SetTheme($theme_class);
$title = "一个简单的3D饼图";
//
$title=iconv("UTF-8","GB2312//IGNORE",$title);
//
$graph->title->SetFont(FF_SIMSUN,FS_BOLD);
$graph->title->Set($title);
//
$p1 = new PiePlot3D($data);
$graph->Add($p1);
$p1->ShowBorder();
$p1->SetColor('black');;
$p1->ExplodeSlice(1);
$graph->Stroke();

//jpgraph_bar.php
require_once('jpgraph/jpgraph.php');
require_once('jpgraph/jpgraph_bar.php');
//示例数据
$data1y = array(47,80,40,116);
$data2y = array(61,30,82,105);
$data3y = array(115,50,70,93);
//创建图像
$graph = new Graph(30,200,'auto');
$graph->SetSCALE("textlin");
$theme_class=new UniversalTheme;
$graph->yaxis->SetTickPositions(array(0,30,60,90,120,150),array(15,45,75,105,135));
$graph->SetBox(false);
$graph->ygrid->SetFill(false);
$graph->xaxis->SetTickPabels(array('A','B','C','D'));
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);
//添加柱状图
$b1plot = new BarPlot($data1y);
$b2plot = new BarPlot($data2y);
$b3plot = new BarPlot($data3y);
$gbplot = new GroupBarPlot(array($b1plot,$b2plot,$b3plot));
$graph->Add($gbplot);
//设置颜色 
$b1plot->SetColor("white");
$b1plot->SetFillColor("#11cccc");
$b2plot->SetColor("white");
$b2plot->SetFillColor("#cc1111");
$b3plot->SetColor("white");
$b3plot->SetFillColor("#1111cc");
//编码转换
$title="柱形图";
$title=iconv("UTF-8","GB2312//IGNORE",$title);
//设置字体，标题
$graph->title->SetFont(FF_SIMSUN,FS_BOLD);
$graph->title->Set($title);
//输出柱状图
$graph->Stroke();




?>
