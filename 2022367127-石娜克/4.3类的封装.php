<?php
class animal2 {

// ====================成员属性========================
private $name;
private $color;
private $age;

// ====================成员属性========================
public function getInfo() {
    echo "动物的名称:". $this->name  . "<br>";
    echo "动物的颜色:". $this->color . "<br>";
    echo "动物的年龄:". $this->age   . "<br>";
}

// 构造函数 
public function __construct($name, $color, $age) {
    $this->name  = $name;
    $this->color = $color;
    $this->age   = $age;
}

//4.3.2.1 get  (取值)
public function __get($property_name){
    // 检查属性 this-> 当前类  的 property_name 属性 是否存在
    if(isset($this->$property_name)){
        return (this->$property_name);
    }else {
        return(NULL);
    }
}
//4.3.2.2 set  (设置赋值)
public function __set( $property_name, $value){
    $this->$property_name = $value;
}

//4.3.2.3 isset  (用来检查属性)
public function __isset($property_name){
    return isset( $this-> $property_name);
}

//4.3.2.4 unset  (用来删除属性)
public function __unset( $property_name){
    unset( $this-> $property_name);
}
}

$pig = new animal();
/**
* private[私有的]
* 1仅针对当前class享有访问权限当前类的子类不允许访问类的外部均不允许访问
*/

// __get __set 方法写了以后就会自动对private属性取值或者赋值


$pig->name="猪猪";
$pig->color="粉色";
$pig->age=12;

echo $pig->name;
echo $pig->color;
echo $pig->age;




//4.3.2.4 unset 测试用例
echo $pig->name . "<br>";//"猪猪";
unset($pig->name);
echo $pig->name. "<br>";// 空输出


?>