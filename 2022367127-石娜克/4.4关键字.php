<?php

//4.4.1 static 静态的
// 新建一个[动物]类
class animal {

    // ====================成员属性========================
    private $name;
    private $color;
    private $age;
    private static $sex = "雄性"; 
    public static function getInfo() {
        echo"动物是雄性的。";
    }
   
}

echo animal::$sex;//错误，静态属性私有，外部不能访问
animal::getInfo();//调用静态方法

//4.4.2final关键字
class animal{
    final public function getInfo(){

    }
    class bird extends animal(){
        public function getInfo(){

       }
    }
}
$crow = new bird();
$crow->getInfo();


//4.4.3self关键字
//一般用来指向类中的静态变量
class animal{
    private static $firstCry= 0;//私有的静态变量
    private $lastCry;
    //构造方法
    function __construct(){
        $this->lastCry = ++self::$firstCry;//++0 = 0 + 1
        $this->lastCry = self::$firstCry + 1;

    }
    function printLastCry(){
        var _dump($this->lastCry);
        
    }
}
$bird = new animal();//实例化对象
$bird->printLastCry();//输出int（）


//4.4.4const关键字
//
class Mylcass2{
    //定义一个常量
    const constant = '我是一个常量！';
    function showConstant(){
        echo self::$constant. "<br>";//类中访问常量
    }    
}
echo Myclass2::constant."<br>"//使用类名来访问常量
$class = new Myclass2();
$class->showConstant();



//4.4.5_toString()方法
class testtoString{
    public $foo;
    //function成员方法
    public function__construct($foo){
        $this->foo = $foo;
    }
    //定义一个__toString()方法，返回一个成员属性$foo
    public function __toString(){
        return $this->foo;
    }    
} 
$class=new testtoString('Hello');
//直接输出对象
echo $class;

//4.4.6__clone()方法
class animal{

    //私有成员属性
    private $name;
    private $color;
    private $age;

  

    // 构造函数 
    public function __construct($name, $color, $age)
    {
        $this->name = $name;
        $this->color = $color;
        $this->age = $age;
    }
    public function getInfo(){
        echo '名字'. $this->name . '<br>';
        echo '颜色'. $this->color . '<br>';
        echo '年龄'. $this->age . '<br>';
    } 
    public function __clone(){
        $this->name="羊";
        $this->color = "白色";
        $this->age = "3岁";
    }
}    
$pig=new animal('猪猪','粉色','8个月');
$pig2=clone $pig;//克隆对象
$pig2->getInfo();

//4.4.7__call()方法
//该方法在调用的方法不存在时会自动调用,程序任会继续执行下去
class callTest{
    function__call($function_name, $args){
        print "你所调用的函数：$function_name(参数";
        print_r "$args";
        print ")不存在！<br>";
    }

}
$test=new callTest();
$test->demo("a","b","c");

//4.4.8自动加载类




?>