<?php
class plant {
    public $name;
    public $color;
    public $age;
    public function __construct( $name, $color, $age) {
        $this->name=$name;
        $this->color=$color;
        $this->age=$age;
    }
    public function getInfo() {
        echo "植物的名称：" . $this->name . "<br>";
        echo "植物的颜色：" . $this->color . "<br>";
        echo "植物的年龄：" . $this->age . "<br>";
    }
  /**
   * 
   */
    public function __destruct(){
        echo"再见:". $this->name."<br>";
    }
}
$rose=new plant('玫瑰','白色',1);
$rose->getInfo();
$willow=new plant('柳树','绿色',8);
$willow->getInfo();
?>