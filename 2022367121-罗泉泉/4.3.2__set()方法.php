<?php
header("Content-Type:test/html;charset=utf-8");
class plant {
    private $name;  //私有的成员属性
    private $color;
    private $age;
    //__get()方法用来设置私有属性的值
    public function __get($property_name) {
        if (isset($this->$property_name)) {
            return $this->$property_name;
        } else {
            return(NULL);
        }
    }
    //__set()方法用来设置私有属性的值
    public function __set($property_name, $value) {
        $this->$property_name=$value;
    }
}
$rose=new plant();
$rose->name="玫瑰"; //自动调用__set()方法
$rose->color="白色";
$rose->age="1";
echo $rose->name. "<br>"; //自动调用__get()方法
echo $rose->color. "<br>";
echo $rose->age. "<br>";
?>