第一章第一次作业
<?php
// 【布尔型】定义一个变量a，赋值为false
$a = false;
// 【字符串】定义一个变量username，赋值为“Mike”
$username = "Mikes";
// echo命令打印，显示
echo "the username is  ", $username, "<br>";
// 打印你好，今天是星期四
// echo "<h1>你\a好\n\r今天是\\疯狂星期四,v我\$50</h1><br>";

$a = 10;
echo "a的值是$a<br>";
echo "a的值是\$a<br>";
// ==是作比较
// = 赋值
if ($username == "Mike") {
    echo "Hi, Mike <br>";
    # code...
} else {
    echo "you are not Mike<br>";

}


if ($a == true) {
    echo "a is true";
} 
else {
    echo "a is false";
}

echo "<br>";
//整型
$n1 = 123;
echo "n1=", $n1, "<br>";
$n2 = 0;
echo "n2=", $n2, "<br>";
$n3 = -123;
echo "n3=", $n3, "<br>";
$n4 = 0123;
echo "n4=", $n4, "<br>";
$n5 = 0xFF;
echo "n5=", $n5, "<br>";
$n6 = 0b11111111;
echo "n6=", $n6, "<br>";
//浮点型
$pi = 3.1415926;
echo "pi=", $pi, "<br>";
$width = 3.3e4;
echo "width=", $width, "<br>";
$var = 3e-2;
echo "var=", $var, "<br>";

echo 3 ** 4;
// 数组
$arr1 = array(1, 2, 3, 4, 5, 6, 7, 8);
$arr2 = array("animals" => "dog", "color" => "red");
// 格式化代码 alt+shift+F
echo "<br>", $arr1[0];//1
echo "<br>", $arr1[7];//1
echo "<br>", $arr2["color"];//1

//=====================================================
// 对象
class Animal
{
    //基本属性
    var $name;
    var $age;
    var $weight;
    var $sex;
    function __construct($name)
    { // 构造函数
        $this->name = $name; // 将传入的参数赋值给属性 name
    }
    //基本方法
    public function run()
    {
        echo "haha,", $this->name, " is running<br>";
    }
    public function eat()
    {
        echo "haha,i am eating";
    }
}
$dog = new Animal("dog");
$cat = new Animal("cat");
$dog->run();
$cat->run();

// 定义一个英雄类
class hero
{
    //血量
    public $hp = 100;
    //姓名
    public $name;
    //攻击力
    public $attack;

    public $shield = 0.2;

    function __construct($name, $attack)
    {
        $this->name = $name;
        $this->attack = $attack;
    }

    // 攻击
    function attack($hero)
    {
        echo $this->name, "攻击了", $hero->name,"<br>";
        $hero->hp -= $this->attack;

        $hero->hp = $hero->hp - $this->attack*(1-$hero->shield);
        echo $hero->name,"的剩余血量：", $hero->hp,"<br>";
    }
}

$hero1 = new hero("瑟提",6);
$hero2 = new hero("鲁班",10);

$hero1->attack($hero2);
$hero2->attack($hero1);
$hero2->attack($hero1);
$hero2->attack($hero1);
$hero2->attack($hero1);
?>
?>
第一章第二次作业计算器
<!DOCTYPE html>
<html>
<head>
    <title>PHP实现简单计算器</title>
    <meta charset="UTF-8">
</head>
<body>
    <table align="center" border="1" width="500">
        <caption><h1>caption我的计算器</h1></caption>
        <form action="">
            <tr>
                <td>
                    <input type="text" size="5" name="num1" value="<?php echo $_GET["num1"] ?? ''; ?>">
                </td>
                <td>
                    <select name="ysf">
                        <option value="+" <?php echo ($_GET["ysf"] ?? '') === "+" ? "selected" : ""; ?>>+</option>
                        <option value="-" <?php echo ($_GET["ysf"] ?? '') === "-" ? "selected" : ""; ?>>-</option>
                        <option value="x" <?php echo ($_GET["ysf"] ?? '') === "x" ? "selected" : ""; ?>>x</option>
                        <option value="/" <?php echo ($_GET["ysf"] ?? '') === "/" ? "selected" : ""; ?>>/</option>
                        <option value="%" <?php echo ($_GET["ysf"] ?? '') === "%" ? "selected" : ""; ?>>%</option>
                    </select>
                </td>
                <td>
                    <input type="text" size="5" name="num2" value="<?php echo $_GET["num2"] ?? ''; ?>">
                </td>
                <td><input type="submit" name="sub" value="计算"></td>
            </tr>
            <?php if (isset($_GET["sub"])): ?>
            <tr><td colspan="4">
            <?php
                $num1 = $_GET["num1"] ?? 0;
                // echo "用户输入的第1个值是",$num1;
                $num2 = $_GET["num2"] ?? 0;
                // echo "用户输入的第2个值是",$num2;
                $message = "";
                if (is_numeric($_GET["num1"]) && is_numeric($_GET["num2"])) {
                    $num1 = (float)$num1;
                    $num2 = (float)$num2;

                    $result = 0;
                    switch ($_GET["ysf"]) {
                        case '+': $result = $num1 + $num2; break;
                        case '-': $result = $num1 - $num2; break;
                        case 'x': $result = $num1 * $num2; break;
                        case '/': 
                            if ($num2 != 0) {
                                $result = $num1 / $num2;
                            } else {
                                $message = "除数不能为0。";
                                echo $message;
                            }
                            break;
                        case '%': 
                            if ($num2 != 0) {
                                $result = $num1 % $num2;
                            } else {
                                $message = "除数不能为0。";
                                echo $message;
                            }
                            break;
                    }
                    echo  "结果是：" . $result;
                } else {
                    $message = "请输入有效的数字。";
                    echo $message;
                }
            ?>
            </td></tr>
            <?php endif; ?>
        </form>
    </table>
</body>
</html> 
第一章第二次作业乘法表
<?php
       $i =$j = 0;
       echo '<table>';
       for ( $i =  1; $j =9; $j++){
        echo'<tr>';
        for ($J = 1; $j <= $i; $j++){
            echo "<td>". getCN($j).getCN($i).( $i * $j<10 ?'得' ).getCN($i *  $j).'</td>';
        }
        echo'</tr>';
       }
       echo'<table>';
       function getCN($mum){
        $cns = array('零','一','二','三','四','五','六','七','八','九','十',);
        if ($num < 10){
            return $cns[$num];
        }
        elseif ($num<100) {//只对小于100的值进行计算
           $r = str_split($num);
           $ln = $cns[ $r[0]] . $cns[10];
           if( $r[1]>0) {
                 $ln.= $cns[ $r[1]];

           }
             return $ln;

        } else{
              return $num;

        }
       }
       ?>
第一章第三次作业
<?php
include'wode.php';
if ( ! isset ($_SESSION)) {
    session_start();
}
$width = 65;
$height = 20;
$image = imagecreate( $width, $height);
$bg_color = imagecolorallocate( $image, 0x33, 0x66, 0xff);
$text = random_text(5);
$font = 5;
$x = imagesx( $image) / 2 - strlen( $text) * imagefontwidth( $font) / 2;
$y = imagesy( $image) / 2 - imagefontheight( $font) / 2;
$fg_color = imagecolorallocate( $image, 0xff, 0xff, 0xff);
imagestring( $image, $font, $x, $y, $text, $fg_color);
$_SESSION['captcha'] = $text;
header('Content-type:image/png'); //定义header,声明图片文件
imagepng( $image);
imagedestroy( $image);
?>











        

