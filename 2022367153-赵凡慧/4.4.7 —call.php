<?php
// --call()方法[该方法在调用的方法不存在时会自动调用]
class Test{
    //$name 参数是要调用的方法名称
    //$rguments 参数是一个数组
    public function __call($function_name,$args) {
        print "你所调用的函数：$function_name(参数：";
        print_r( $args);
        print ")不存在！ <br>";

    }
}
$test=new Test();
$test->demo("one", "two", "three");
?>
<?php
spl_autoload_register(function ( $class_name) {
    require_once $class_name . '.class.php';
});
$obj =new MyClassl();
print_r( $obj);
$obj2=new MyClass2();
print_r( $obj2);
?>