<?php
class animal {
    //成员属性
    public $name;
    public $color;
    public $age;
    //构造函数（创造对象时为成员属性赋初值，完成初始化工作）
public function __construct ( $name, $color, $age ) {
    //echo "我是构造方法";
    $this->name= $name;
    $this->color= $color;
    $this->age= $age;
}
//析构函数（在对象销毁时自动调用）
public function getImfo() {
    echo "动物的名称：" . $this->name . "<br>";
    echo "动物的颜色：" . $this->color . "<br>";
    echo "动物的年龄：" . $this->age . "<br>";
}
//类的重载（继承类的方法的继承写法[parent::]
class bird extends animal {
    public $wing;//bird类的自有属性 $wing
    public function getInfo() {
        //使用[parent::]来获取父类的成员方法或属性
        parent::getInfo();
        //echo "动物的名称:". $this->name . "<br>";
        echo "鸟类有" . $this->wing . '翅膀<br>';
        $this->fly();
    } 
        punlic function fly() {
            //鸟类自有的方法
            echo "我会飞翔！！！";
        }
    }
    public function __construct( $name, $color, $age, $swing) {
        parent::__construct( $name, $color, $age,);
        $this->wing= $wing;
        }
}
$sparrow=new bird("麻雀","灰色",4,"漂亮的")；
$crow->getImfo();
?>