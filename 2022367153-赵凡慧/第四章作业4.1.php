第四章第一次作业
<?php
class fruit {
    //成员属性
    public $name;
    public $color;
    public $taste;
    //构造函数（创造对象时为成员属性赋初值，完成初始化工作）
public function __construct ( $name, $color, $taste ) {
    //echo "我是构造方法";
    $this->name= $name;
    $this->color= $color;
    $this->taste= $taste;
}
//成员属性
public function getImfo() {
    echo "水果的名称：" . $this->name . "<br>";
    echo "水果的颜色：" . $this->color . "<br>";
    echo "水果的味道：" . $this->taste . "<br>";
}
//析构函数（在对象销毁时自动调用）
public function __destruct() {
    echo "再见:". $this->name . "<br>";
}
}
$apple = new fruit("苹果","红色","甜");//通过new关键字实例化出一个对象，名称为apple
$apple->getImfo();
$plum = new fruit("李子","绿色","酸甜");//通过new关键字实例化出一个对象，名称为plum
$plum->getImfo();
$litchi = new fruit("荔枝","浅红色","甜");//通过new关键字实例化出一个对象，名称为litchi
$litchi->getImfo();
?>