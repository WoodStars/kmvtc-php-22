<?php
//定义animal接口
interface animal{
// 接口中的方法不能有方法体,只能有方法名和参数
function run();
function shout();
}
//定义landanimal接口
interface landanimal{
    public function liveonland();
}
//定义dog类，实现animal接口和landanimal接口
class dog implements animal， landanimal{
    //实现接口中的抽象方法
    public function run() {
        echo "小狗在奔跑<br>";

    }
    public function shout() {
        echo "汪汪<br>";
    }
}
//定义cat类，实现animal接口和landanimal接口
class cat implements animal, landanimal{
    //实现接口中的方法
    public function run() {
        echo "小猫在奔跑<br>";
    }
    public function shout() {
        echo "喵喵<br>"；
    }
}
$dog=nwe dog();
$dog->run();
$dog->shout();
$dog->liveonland();
$cat=new cat();
$cat->run();
$cat->shout();
$cat->liveonland();

// 实例化dog类
$dog = new dog();
// 调用dog类的run方法
$dog->run();
echo "\n";
// 调用dog类的shout方法
$dog->shout();
echo "\n";
// 实例化cat类
$cat = new cat();
// 调用cat类的run方法
$cat->run();
echo "\n";
// 调用cat类的shout方法
$cat->shout();
echo "\n";
