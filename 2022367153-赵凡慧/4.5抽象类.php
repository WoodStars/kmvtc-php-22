<?php
 //如果一个类里面有抽象方法，那么这个类必须是抽象类
abstract class animal
{
   //在抽象类中声明抽象方法
abstract function shout();
}
//如果一个类里面有抽象方法，那么这个类必须实现抽象方法
class animal333
{
    // 抽象方法
     function shout(){
        echo "动物叫声";
     };

}
//定义dog类继承animal类
class dog extends animal{
    function shout(): void
    {
        echo "汪汪汪";
    }
}
//定义cat类继承animal类
class cat extends animal{
    function shout(): void
    {
        echo "喵喵喵";
    }
}
// 实例化animal
// $animal = new animal();
// 抽象类不能被实例化
//实例化dog类
$dog = new dog();
//调用dog类的shout方法
$dog->shout();
echo "\n";
//实例化cat类
$cat = new cat();
//调用cat类的shout方法
$cat->shout();
echo "\n";
