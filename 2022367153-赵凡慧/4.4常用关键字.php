<?php
//4.4.1 static 静态的
// 新建一个[动物]类
class animal {

    // ====================成员属性========================
    private $name;
    private $color;
    private $age;
    //   static关键字
    private static $sex = "雄性"; //私有的静态属性
    //静态成员方法
    // public function getInfo() {
    //     echo "动物的名称:". $this->name  . "<br>";
    //     echo "动物的颜色:". $this->color . "<br>";
    //     echo "动物的年龄:". $this->age   . "<br>";
    // }

    // 构造函数 
    public function __construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    }

    private function getInfo()  {
        echo "动物是雄性的";   
    }
    
}

echo animal::$sex;//错误，静态属性私有，外部不能访问
echo animal::$color;//只有static属性可以通过类来访问，因为static 定义的属性是在类加载的时候压入内存的，非static的，在每一个实例被初始化的时候再亚茹内存，有多个实例，就有多个属性
animal::getInfo();//调用静态方法 



//4.4.2 final 常量 由于定义 class 和 function /// define 成员属性
// final 定义的类不能被继承（inherit）
<?php
class animal {
    final public function getInfo() {
    }
}
class bird extends animal {
    public function getInfo() {
    }
}
$crow=new bird();
$crow->getImfo();
// can not override(重写、覆盖) [final] method —— animal::getInfo() on line 12 



//4.4.3 self 指向【类-class】的本身，不指向任何实例化的对象

class animalXx {
    private static int $firstCry = 0 ;//私有的静态变量
    private $lastCry;
    //构造方法
    function __construct() {
        $this->lastCry=++self:: $firstCry;
    }
    function printLastCry() {
        var_dump( $this->lastCry);

    }

}
$bird=new animal(); //实例化对象
$bird->printLastCry();//输出：int（1）


//4.4.4 const 
class MyClass {
    //定义一个常量
    const constant='我是一个常量!';
    function showConstant() {
        echo self::constant."<br>";//类中访问常量
    }
}
echo MyClass::constant."<br>";//使用类名来访问常量
$class=new MyClass();
$class->showConstant();




//4.4.5 --toString()方法
class TestClass {
    public $foo;
    public function __construct( $foo) {
        $this->foo= $foo;


    }//定义一个__toString()方法，返回一个成员属性 $foo
    public function __toString() {
        return $this->foo;
    }
}
$class=new TestClass('Hello');
//直接输出对象
echo $class;  //输出结果为Hello
