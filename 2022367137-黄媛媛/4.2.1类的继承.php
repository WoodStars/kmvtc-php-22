<?php
class animal {
    public $name;
    public $color;
    public $age;
    public function __construct( $name, $color, $age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    public function getInfo() {
        echo "动物的名称:". $this->name . "<br>";
        echo "动物的颜色:". $this->color . "<br>";
        echo "动物的年龄:". $this->age . "<br>";
    }
}

class bird extends animal {
    public $wing;
        //bird类的自有属性 $wing
    public function fly() { 
        //bird类自有的方法
        echo'I can fly!!!';
    }
}
    $crow=new bird("乌鸦","黑色",3);
    $crow->getInfo();
    $crow->fly();
?>