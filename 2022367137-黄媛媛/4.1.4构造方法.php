<?php
class fruit {
    public $name;
    public $color;
    public $taste;
    public function __construct( $name, $color, $taste) {
        $this->name= $name;
    }
        $this->color= $color;
        $this->taste= $taste;
    public function getInfo() {
        echo "水果的品种:". $this->name. "<br>";
        echo "水果的颜色:". $this->color.  "<br>";
        echo "水果的口味:". $this->taste. "<br>";
    }
}
$apple=new fruit("苹果","红色","甜");
//通过new关键字实例化出一个对象，名称为apple
$apple->getInfo();
$orange= new fruit('橙子','橙色','酸甜');
//通过new关键字实例化出一个对象，名称为orange
$orange->getInfo();
?>