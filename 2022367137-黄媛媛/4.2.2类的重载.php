<?php

//新建一个[手机]类
class phone {

    // ====================成员属性========================
    public $brand;
    public $price;
    public $producer;

    // ====================成员属性========================
    public function getInfo() {
        echo "手机的品牌". $this->brand . "<br>";
        echo "手机的价格". $this->price . "<br>";
        echo "手机的产地". $this->producer . "<br>";
    } 
    // 构造函数 (创建对象时为对象赋初始值)
    // parent::__construct($brand,$price,$producer)

    public function __construct($brand,$price,$producer) {
        $this->brand = $brand;
        $this->price = $price;
        $this->producer = $producer;
    }
}
    // 类的继承，新建一个[smartphone]子类继承手机类
    class smartphone extend phone{
        //成员属性：视频
        public $video;
        // 重载：继承类的方法的继承写法[parent::]
        public function getInfo(){
            // 使用[parent::]来获取父类的成员方法或者属性
            parent::getInfo();
            // echo "手机的品牌:". $this->brand  . "<br>";
            // echo "手机的价格:". $this->price . "<br>";
            // echo "手机的产地:". $this->producer   . "<br>";
            // 书写智能手机特有的属性，信息
            echo "智能手机有". $this->video . '系统<br>';
            $this->system();

        }
        // 子类[smartphone]具有的成员方法
        public function video(){
        echo $this->name . "is video call...";
        }
         // 继承类的构造函数的继承写法[parent::]
        public function __construct($brand,$price,$producer,$video) {
            parent::__construct($brand,$price,$producer)、
            // 智能手机类特有的视频属性需要单独书写
            $this->video = $video;
        }
}
?>