<?php
class animal{
    private static $firstCry=0;//私有的静态变量
    private $lastCry;
    //构造方法
    function __construct(){
        $this->$lastCry=++self::$firstCry;
    }
    function __printLastCry(){
        var__dump( $this->lastCry);
    }
}
$bird=new animal();//实例化对象
$bird->printLastCry();//输出： int（1）
?>

