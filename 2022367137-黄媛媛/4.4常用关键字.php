<?php

//4.4.1 static 静态的
// 新建一个[手机]类
class telephone {
    private $name;
    private $color;
    private $price;
    private static $producer="中国";//私有的静态属性
    public function __construct( $name, $color, $price) {
        $this->name= $name;
        $this->color= $color;
        $this->price= $price;
    }
    public function getInfo() {
        echo "手机的名称:" . $this->name . "<br>";
        echo "手机的颜色:" . $this->color . "<br>";
        echo "手机的价格:" . $this->price . "<br>";
    }
    public static function getInfo() {
        echo "手机的产地。"; 
    }
}
echo telephone::$producer;  //静态成员不能在外部访问
telephone::getInfo();// 使用静态方法
?>

<?php
class vegetable {
    final public function getInfo() {
    }
} 
class melon extends vegetable {
    public function getInfo(){
    }
}
$pumpkin=new melon();
$pumpkin->getInfo();
?>



