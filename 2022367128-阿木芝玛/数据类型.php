<?php 
$a = false;
$username = "Mikes";
echo "the username is  ", $username, "<br>";

$a = 10;
echo "a的值是$a<br>";
echo "a的值是\$a<br>";
if ($username == "Mike") {
    echo "Hi, Mike <br>";
} else {
    echo "you are not Mike<br>";

}


if ($a == true) {
    echo "a is true";
} else {
    echo "a is false";
}

echo "<br>";
$n1 = 123;
echo "n1=", $n1, "<br>";
$n2 = 0;
echo "n2=", $n2, "<br>";
$n3 = -123;
echo "n3=", $n3, "<br>";
$n4 = 0123;
echo "n4=", $n4, "<br>";
$n5 = 0xFF;
echo "n5=", $n5, "<br>";
$n6 = 0b11111111;
echo "n6=", $n6, "<br>";

$pi = 3.1415926;
echo "pi=", $pi, "<br>";
$width = 3.3e4;
echo "width=", $width, "<br>";
$var = 3e-2;
echo "var=", $var, "<br>";

echo 3 ** 4;

$arr1 = array(1, 2, 3, 4, 5, 6, 7, 8);
$arr2 = array("animals" => "dog", "color" => "red");

echo "<br>", $arr1[0];
echo "<br>", $arr1[7];
echo "<br>", $arr2["color"];


class Animal
{
    
    var $name;
    var $age;
    var $weight;
    var $sex;
    function __construct($name)
    { 
        $this->name = $name; 
    }
    
    public function run()
    {
        echo "haha,", $this->name, " is running<br>";
    }
    public function eat()
    {
        echo "haha,i am eating";
    }
}
$dog = new Animal("dog");
$cat = new Animal("cat");
$dog->run();
$cat->run();


class hero
{
    
    public $hp = 100;
    
    public $name;
    
    public $attack;

    public $shield = 0.2;

    function __construct($name, $attack)
    {
        $this->name = $name;
        $this->attack = $attack;
    }

    
    function attack($hero)
    {
        echo $this->name, "攻击了", $hero->name,"<br>";
        $hero->hp -= $this->attack;

        $hero->hp = $hero->hp - $this->attack*(1-$hero->shield);
        echo $hero->name,"的剩余血量：", $hero->hp,"<br>";
    }
}

$hero1 = new hero("瑟提",6);
$hero2 = new hero("鲁班",10);

$hero1->attack($hero2);
$hero2->attack($hero1);
$hero2->attack($hero1);
$hero2->attack($hero1);
$hero2->attack($hero1);
?>