<div class="page">
  <h1 style="width:100%">我的小账本</h1>
  <div id="zhuzhuangtu" class="zhuzhuangtu"></div>
  <div id="bingtu" class="bingtu"></div>
  <div id="mixzhu" class="mixzhu"></div>
  <div id="meiguitu" class="meiguitu"></div>
</div>

<script src="https://unpkg.com/@antv/g2/dist/g2.min.js"></script>
<script>
  const data2 = [
    { item: '得物', count: 40, percent: 0.4 },
    { item: '微信', count: 21, percent: 0.21 },
    { item: '支付宝', count: 17, percent: 0.17 },
    { item: '淘宝', count: 13, percent: 0.13 },
    { item: '拼多多', count: 9, percent: 0.09 },
  ];
  const chart2 = new G2.Chart({
    container: 'bingtu',

  });

  chart2.coordinate({ type: 'theta', outerRadius: 0.8 });

  chart2
    .interval()
    .data(data2)
    .transform({ type: 'stackY' })
    .encode('y', 'percent')
    .encode('color', 'item')
    .legend('color', { position: 'bottom', layout: { justifyContent: 'center' } })
    .label({
      position: 'outside',
      text: (data2) => `${data2.item}: ${data2.percent * 100}%`,
    })
    .tooltip((data) => ({
      name: data2.item,
      value: `${data2.percent * 100}%`,
    }));

  chart2.render();


  const data1 = [
    { genre: 'Fruit', sold: 120 },
    { genre: 'Snack' , sold: 100 },
    { genre: 'Eat' , sold: 350 },
    { genre: 'Shoe', sold: 500 },
    { genre: 'Clothes', sold: 250 },
    { genre: 'Cosmetics', sold: 180 },
  ];

  // 初始化图表实例
  const chart1 = new G2.Chart({
    container: 'zhuzhuangtu',

  });

  // 声明可视化
  chart1
    .interval() // 创建一个 Interval 标记
    .data(data1) // 绑定数据
    .encode('x', 'genre') // 编码 x 通道
    .encode('y', 'sold'); // 编码 y 通道

  // 渲染可视化
  chart1.render();


  const data3 = [
    { name: 'Last semester', 月份: 'Jan.', 月销: 1089 },
    { name: 'Last semester', 月份: 'Feb.', 月销: 1808 },
    { name: 'Last semester', 月份: 'Mar.', 月销: 2050 },
    { name: 'Last semestern', 月份: 'Apr.', 月销: 2100 },
    { name: 'Last semestern', 月份: 'May', 月销: 2000 },
    { name: 'Last semester', 月份: 'Jun.', 月销: 2030 },
    { name: 'Last semester', 月份: 'Jul.', 月销: 2400 },
    { name: 'Last semester', 月份: 'Aug.', 月销: 1506 },
    { name: 'Next semester', 月份: 'Sep.', 月销: 1240 },
    { name: 'Next semester', 月份: 'Oct.', 月销: 2302 },
    { name: 'Next semester', 月份: 'Nov.', 月销: 2000 },
    { name: 'Next semester', 月份: 'Dec.', 月销: 1890 },
  ];

  const chart3 = new G2.Chart({
    container: 'mixzhu',
  });

  chart3
    .interval()
    .data(data3)
    .encode('x', '月份')
    .encode('y', '月销')
    .encode('color', 'name')
    .transform({ type: 'stackY' })
    .interaction('elementHighlight', { background: true });

  chart3.render();


  const chart4 = new G2. Chart({
  container: 'meiguitu',
});

chart.coordinate({ type: 'Plant' });

chart
  .interval()
  .transform({ type: 'groupX', y: 'sum' })
  .data({data4})
  .encode('x', 'year')
  .encode('y', 'Flower')
  .scale('y', { type: 'sqrt' })
  .axis('y', {
    titleSpacing: 28,
    labelFormatter: '~s',
    tickCount: 5,
    tickFilter: (d, i) => i !== 0,
    direction: 'right',
  })
  .animate('enter', { type: 'waveIn' })
  .tooltip({ channel: 'y', valueFormatter: '~s' });

chart4.render();

</script>



<style>
  .page {
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
  }

  .zhuzhuangtu {
    width:50%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;

  }

  .bingtu {
    width:50%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
  }

  .mixzhu{
    width:100%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
  }
</style>