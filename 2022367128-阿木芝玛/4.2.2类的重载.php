<?php
// 新建一个[植物]类
class plants{
    // ====================成员属性========================

    public $name='';
    public $color='';
    public $season='';
    
  // 构造函数 (创建对象时为对象赋初始值)
    // parent::__construct($name, $color, $season)
    public function_construct( $name, $color, $season){
         $this->name= $name;
         $this->color= $color;
         $this->season= $season;
    }

    // ====================成员属性========================
    public function getInfo() {
        echo "植物的名称：". $this->name. "<br>";
        echo "植物的颜色：". $this->color. "<br>";
        echo "植物的季节：". $this->season. "<br>";   
     }

     // 析构函数 (销毁对象时执行)
    public function __destruct()
    {
        echo "再见:" . $this->name . "。<br>";
    }
}
/**
 * 定义一个flower类，使用extends关键字来继承animal类，作为animal类的子类
 */

// 类的继承，新建一个[flower]子类继承植物类
class flower extends plants{
    // 成员属性：叶子
    public $leaf;
    // 重载：继承类的方法的继承写法[parent::]
    public function getInfo(){
        // 使用[parent::]来获取父类的成员方法或者属性
        parent::getInfo();
        // echo "植物的名称:". $this->name  . "<br>";
        // echo "植物的颜色:". $this->color . "<br>";
        // echo "植物的季节:". $this->season   . "<br>";
        // 书写花类特有的属性，信息
        echo "花类有" . $this->leaf . '叶子<br>';
        $this->withered();
    }

    // 子类[flower]具有的成员方法
    public function withered(){
        echo $this->name . "is withered...";
    }
 
    // 继承类的构造函数的继承写法[parent::]
    public function __construct($name, $color, $season, $withered) {
        parent::__construct($name, $color, $season)、
        // 花类特有的叶子属性需要单独书写
        $this->leaf = $leaf;
    }
}

// 如何创建一个对象？
$rose=new flower("玫瑰","红色",5,"漂亮的");
$crow->getInfo();
?>
    


