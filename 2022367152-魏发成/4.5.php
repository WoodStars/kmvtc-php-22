<?php
header("Content-Type:text/html;charest=utf-8");
//使用abstract 关键字声明一个抽象类
abstract class animal{
    //抽象类中声明抽象方法
    abstract public function shout();
}
//定义 dog 类继承 animal类
class dog extends animal {
//实现抽象方法
public function shout() {
    echo "汪汪......<br>" ;
}
}
//定义cat 类继承 animal 类
class cat extends animal {
    //实现抽象方法
    public function shout() {
        echo "喵喵......<br>" ;
    }
}
$dog=new dog() ;
$dog->shout();
$cat=new cat();
$cat->shout();
?>
