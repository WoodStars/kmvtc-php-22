<div class="page">
  <h1 style="width:100%">我的小账本</h1>
  <div id="zhuzhuangtu" class="zhuzhuangtu"></div>
  <div id="bingtu" class="bingtu"></div>
  <div id="mixzhu" class="mixzhu"></div>
  <div id="zhexiantu" class="zhexiantu"></div>
</div>

<script src="https://unpkg.com/@antv/g2/dist/g2.min.js"></script>
<script>
  const data2 = [
    { item: '大白菜', count: 40, percent: 0.4 },
    { item: '豌豆颠', count: 21, percent: 0.21 },
    { item: '土豆', count: 17, percent: 0.17 },
    { item: '西红柿', count: 13, percent: 0.13 },
    { item: '薄荷', count: 9, percent: 0.09 },
  ];
  const chart2 = new G2.Chart({
    container: 'bingtu',

  });

  chart2.coordinate({ type: 'theta', outerRadius: 0.8 });

  chart2
    .interval()
    .data(data2)
    .transform({ type: 'stackY' })
    .encode('y', 'percent')
    .encode('color', 'item')
    .legend('color', { position: 'bottom', layout: { justifyContent: 'center' } })
    .label({
      position: 'outside',
      text: (data2) => `${data2.item}: ${data2.percent * 100}%`,
    })
    .tooltip((data) => ({
      name: data2.item,
      value: `${data2.percent * 100}%`,
    }));

  chart2.render();


  const data1 = [
    { genre: 'Sports', sold: 275 },
    { genre: 'Strategy', sold: 115 },
    { genre: 'Action', sold: 120 },
    { genre: 'Shooter', sold: 350 },
    { genre: 'Other', sold: 150 },
  ];

  // 初始化图表实例
  const chart1 = new G2.Chart({
    container: 'zhuzhuangtu',

  });

  // 声明可视化
  chart1
    .interval() // 创建一个 Interval 标记
    .data(data1) // 绑定数据
    .encode('x', 'genre') // 编码 x 通道
    .encode('y', 'sold'); // 编码 y 通道

  // 渲染可视化
  chart1.render();


  const data3 = [
    { name: 'London', 月份: 'Jan.', 月均降雨量: 18.9 },
    { name: 'London', 月份: 'Feb.', 月均降雨量: 28.8 },
    { name: 'London', 月份: 'Mar.', 月均降雨量: 39.3 },
    { name: 'London', 月份: 'Apr.', 月均降雨量: 81.4 },
    { name: 'London', 月份: 'May', 月均降雨量: 47 },
    { name: 'London', 月份: 'Jun.', 月均降雨量: 20.3 },
    { name: 'London', 月份: 'Jul.', 月均降雨量: 24 },
    { name: 'London', 月份: 'Aug.', 月均降雨量: 35.6 },
    { name: 'Berlin', 月份: 'Jan.', 月均降雨量: 12.4 },
    { name: 'Berlin', 月份: 'Feb.', 月均降雨量: 23.2 },
    { name: 'Berlin', 月份: 'Mar.', 月均降雨量: 34.5 },
    { name: 'Berlin', 月份: 'Apr.', 月均降雨量: 99.7 },
    { name: 'Berlin', 月份: 'May', 月均降雨量: 52.6 },
    { name: 'Berlin', 月份: 'Jun.', 月均降雨量: 35.5 },
    { name: 'Berlin', 月份: 'Jul.', 月均降雨量: 37.4 },
    { name: 'Berlin', 月份: 'Aug.', 月均降雨量: 42.4 },
  ];

  const chart3 = new G2.Chart({
    container: 'mixzhu',
  });

  chart3
    .interval()
    .data(data3)
    .encode('x', '月份')
    .encode('y', '月均降雨量')
    .encode('color', 'name')
    .transform({ type: 'stackY' })
    .interaction('elementHighlight', { background: true });

  chart3.render();
	
const chart4 = new G2.Chart({
    container: 'zhexiantu',

  });
	const data = [
  { month: 'Jan', city: 'Tokyo', temperature: 7 },
  { month: 'Jan', city: 'London', temperature: 3.9 },
  { month: 'Feb', city: 'Tokyo', temperature: 6.9 },
  { month: 'Feb', city: 'London', temperature: 4.2 },
  { month: 'Mar', city: 'Tokyo', temperature: 9.5 },
  { month: 'Mar', city: 'London', temperature: 5.7 },
  { month: 'Apr', city: 'Tokyo', temperature: 14.5 },
  { month: 'Apr', city: 'London', temperature: 8.5 },
  { month: 'May', city: 'Tokyo', temperature: 18.4 },
  { month: 'May', city: 'London', temperature: 11.9 },
  { month: 'Jun', city: 'Tokyo', temperature: 21.5 },
  { month: 'Jun', city: 'London', temperature: 15.2 },
  { month: 'Jul', city: 'Tokyo', temperature: 25.2 },
  { month: 'Jul', city: 'London', temperature: 17 },
  { month: 'Aug', city: 'Tokyo', temperature: 26.5 },
  { month: 'Aug', city: 'London', temperature: 16.6 },
  { month: 'Sep', city: 'Tokyo', temperature: 23.3 },
  { month: 'Sep', city: 'London', temperature: 14.2 },
  { month: 'Oct', city: 'Tokyo', temperature: 18.3 },
  { month: 'Oct', city: 'London', temperature: 10.3 },
  { month: 'Nov', city: 'Tokyo', temperature: 13.9 },
  { month: 'Nov', city: 'London', temperature: 6.6 },
  { month: 'Dec', city: 'Tokyo', temperature: 9.6 },
  { month: 'Dec', city: 'London', temperature: 4.8 },
];

const chart = new G2.Chart({
  container: 'zhexiantu',
  
});

chart
  .data(data)
  .encode('x', 'month')
  .encode('y', 'temperature')
  .encode('color', 'city')
  .scale('x', {
    range: [0, 1],
  })
  .scale('y', {
    nice: true,
  })
  .axis('y', { labelFormatter: (d) => d + '°C' });

chart.line().encode('shape', 'smooth');

chart.point().encode('shape', 'point').tooltip(false);

chart.render();

	

</script>

<style>
  .page {
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
  }

  .zhuzhuangtu {
    width:50%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;

  }

  .bingtu {
    width:50%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
  }

  .zhexiantu{
    width:50%;
    display: flex;
    flex-direction: row;
    justify-content: right;
    align-items: right;
  }
	
</style>