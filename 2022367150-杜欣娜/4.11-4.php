<?php
//包含生成给定长度字符串的自定义函数functions.php
include 'functions.php';
//开启或继续会话，保存图形验证码到会话中供其他页面调用
if(! isset($_SESSION)){
    session_start();
}
//创建65px*20px 大小的图像
$width = 65;
$height = 20;
$image = imagecreate($width,$height);
//为一幅图像分配颜色:imagecolorallocate
$bg_color = imagecolorallocate( $image,0x33,0x66,0xff);
//取得随机字符串
$text = random_text(5);
//定义字体、位置
$font = 5;
$x = imagesx( $image) / 2 - strlen( $text) * imagefontwidth( $font) / 2;
$y = imagesy( $image) / 2 - imagefontheight( $font) / 2;
//输出字符到图形上
$fg_color = imagecolorallocate( $image,0xff,0xff,0xff);
imagestring( $image, $font, $x, $y, $text, $fg_color);
//保存验证码到会话，用于比较验证
$_SESSION['captcha'] = $text;
//输出图像
header('Content-type:image/png');//定义header，声明图片文件
imagepng($image);
imagedestroy( $image);
?>