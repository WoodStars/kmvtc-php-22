<?php
class animal {
    public $name :
    public $color :
    public $age :
    public function __ construct ($name, $color , $age ){
        $this->name = $name ;
        $this->color = $color;
        $this->age = $age;
    }
    public function getlnfo() {
        echo "动物的名称:" . $this ->name ."<br>";
        echo "动物的颜色:" . $this ->color . "<br>";
    }
}
/* *
* 定义一个bird类，使用extends关键字来继承animal类，作为animal类的子类
*/
class bird extends animal {
    public $wing ;//bird类的自有属性$wing
    public function fly() {//bird类的自有方法
        echo 'l can fly!!!';
    }
}
$crow = new bird("公鸡","红色",30);
$crow ->getlnfo ();
$crow ->fly();
?>