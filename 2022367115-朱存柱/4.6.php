<?php
header("content-type:text/html;charset=utf-8");
interface animal{
    function run();
    function shout();
}
interface landanimal{
    public function liveonland();
}
class dog implements animal, landanimal{
    public function run();
echo "小狗在奔跑<br>";
}
public function shout() {
    echo "玩玩......<br>";
}
public function liveonland() {
    echo "小姐在陆地生活<br>";
}
$dog=new dog();
$dog->run();
$dog->shout();
$dog->liveonland()
?>