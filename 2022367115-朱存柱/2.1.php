<?php
//gettype()
$a = "Hello";
echo gettype($a) . "<br>";
$b = array(1, 2, 5);
echo gettype($b) . "<br>";
//intval
echo intval(4.5) . "<br>";
//var_dump
var_dump($a);
echo "<br>";
var_dump($a, $b);
//输出结果：string(5) "Hello" array(3) {[0] ->int(1) [1] => int(2) [2]=>int(5)}
?>


<?php
echo (floor(0.60));
echo (floor(0.40));
echo (floor(5));
echo (floor(5.1));
echo (floor(-5.1));
echo (floor(-5.9));
?>

<?php
echo rand();
echo rand();
echo rand(10, 100);
?>

<?php
$pizza = "piece1 piece2 piece3 piece4 piece5 piece6";
$pieces = explode(" ", $pizza);
echo $pieces[0];
echo $pieces[1];
?>


<?php
echo md5("apple");
?>


<?php
var_dump(checkdate(12, 31, 2000));
var_dump(checkdate(2, 29, 2001));
?>

<?php
//设置时区
date_default_timezone_set("PRC");
//checkdate()
var_dump(checkdate(12, 31, 2000)) . "<br>";
var_dump(checkdate(2, 31, 2000)) . "<br>";
//mktime()
echo time() . "<br>";
echo mktime(0, 0, 0, 12, 25, 2016) . "<br>";
//今天距离2023年国庆还有多少天
echo "今天距离2023年国庆还有多少天" . ceil((mktime(0, 0, 0, 10, 1, 2030) - time()) / (24 * 60 * 60)) . "天<br>";
//date()返回格式化的当地时间
echo "现在是：" . date('Y-m-d H:i:s');