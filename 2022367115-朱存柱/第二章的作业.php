<?php
// function function_name($a , $b , $c){
//     //  function body 函数体
//     return $a + $b;
// }

// /**
//  * 函数的声明（定义）
//  * 求平方数
//  */
// function custom_func($num){
//     return "$num*$num=" . $num*$num . "<br>";
// }

// // 函数的调用
// echo custom_func(5);
// echo custom_func(6);
// echo custom_func(7);
// echo custom_func(8);


// function unknowm( $a , $b ){
//     $a = $a +5;
//     $b = $b -5;
//     $c = $a * $b;
//     $d = $c / $c;
//     return $d;
// }

// echo unknowm(10,11);



// /**
//  * 按值传参
//  */
// function fun_2( $a  ){
//     $a = $a +5;
//     return $a;
// }


// /**
//  * 按引用值传参
//  */
// function fun_3(& $a  ){
//     $a = $a +5;
//     return $a;
// }

// /**
//  * 按引用值传参
//  */
// function fun_default(& $a  ){
//     $a = $a +5;