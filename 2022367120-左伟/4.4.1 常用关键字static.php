<?php
class animal 
{
    private $name;
    private $color;
    private $age;
    //私有的静态属性
    private static $sex="雄性";
    public function__construct($name,$color,$age) 
    {
        $this->name=$name;
        $this->color=$color;
        $this->age=$age;

    }
    public  function getInfo() 
    {
        echo"动物的名称:" . $this->name . "<br>";
        echo"动物的颜色:" . $this->color . "<br>";
        echo"动物的年龄:" . $this->age . "<br>";
        // echo"动物的性别:" . self::$sex;
    }
}
$ dog=new animal("小狗","黑色",4);
$dog->getInfo();
?>