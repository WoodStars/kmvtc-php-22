<?php
class MyClass 
{
    //定义一个常量
    const constant='我是一个常量！';
    function showConstant() 
    {
        //类中访问常量
        echo self::constant."<br>";
    }
}
echo MyClass::constant."<br>";
$class=new MyClass();
$class->showConstant();
?>