<?php
header("Content-Type:text/html;charset=utf-8");
abstract class hunmas{
    public abstract function shout();
}
//定义chinses类，实现抽象类中的方法
class chinese extends hunmas{
    public function shout(){
        echo "啦啦啦";
    }
}
//定义xiaoriben类，实现抽象类中的方法
class xiaoriben extends hunmas{
    public function shout(){
        echo "救命啊啊啊啊";
    }
}
function animalshout($obj){
    if($obj instanceof hunmas){
        $obj->shout();
    } else {
        echo "Error:对象错误";
    }
}
$chinese=new chinese();
$xiaoriben=new xiaoriben();
animalshout($chinese);




















?>