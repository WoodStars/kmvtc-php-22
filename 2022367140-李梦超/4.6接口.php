<?php
//定义接口大概
interface hunams1{
    function run();
    function shout();
}
//实例
header("Content-Type:text/html;charset=utf-8");
//定义接口
interface hunams{
    //声明抽象方法
    function dongzuo();
    function zhuangtai();
}
//定义 hunams 类，实现 hunmas 接口
class chinese implements hunams{
    //实现接口中的抽象方法
    public function dongzuo(){
        echo "小小林在看抗日片<br>";
    }
    public function zhuangtai(){
        echo "悠闲地，开心的<br>";
    }
}
//定义 xiaoriben 类，实现 hunmas 接口
class xiaoriben implements hunams{
    //实现接口中的方法
    public function dongzuo(){
        echo "小鬼子在看小男孩";
    }
    public function zhuangtai(){
        echo "应该挺开心的";
    }
}
$chinese=new chinese();
$chinese->dongzuo();
$chinese->zhuangtai();
$xiaoriben=new xiaoriben();
$xiaoriben->dongzuo();
$xiaoriben->zhuangtai();























?>