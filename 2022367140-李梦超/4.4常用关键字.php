<?php


//4.4.1 static 静态的
// 新建一个[恶魔果实]类
class emoguoshi {

    // ====================成员属性========================
    private $name;
    private $weidao;
    private $nengli;
    private static $yanyanguoshi = "麻辣的屎味"; 
    // ====================成员属性========================
    // public function getInfo() {
    //     echo "果实的名称:". $this->name  . "<br>";
    //     echo "果实的味道:". $this->weidao . "<br>";
    //     echo "果实的能力:". $this->nengli   . "<br>";
    // }

    // 构造函数 
    public function __construct($name, $weidao, $nengli) {
        $this->name  = $name;
        $this->weidao = $weidao;
        $this->nengli   = $nengli;
    }

    private function getInfo()  {
        echo "恶魔果实是难吃的";   
    }
}

echo animal::$sex;// 静态成员不能在外部访问
echo animal::$weidao;// 只有static属性可以通过类来访问，因为static 定义的属性是在类加载的时候压入内存的，非static的，在每一个实例被初始化的时候再亚茹内存，有多个实例，就有多个属性
animal::getInfo();// 使用静态方法

//4.4.2 final 常量 用于定义 class 和 function /// define 成员属性
// final 定义的类不能被继承（inherit）

// 自己尝试写一个父类一个子类，在父类使用final关键词定义，看报错信息。

// can not override(重写、覆盖) [final] method —— animal::getInfo() on line 12 

//4.4.3 self 指向【类-class】------------------------------------------------------------
class jiligulu {
    private static $firstCry = 0;//私有的静态变量
    private $lastCry;
    //构造方法
    function __construct(){
        $this->lastCry = ++self::$firstCry;
    }
    function printLastCry() {
        var_dump($this->lastCry);
    }
}
$bird = new animal();//实例化对象
$bird->printLastCry();//输出:int(1)


//4.4.4 const 定义一个常量【define()函数】---------------------------------------------
class MyClass{
    //定义一个常量
    const constant="我是一个常量！";
    function showConstant() {
        echo self::constant."<br>";//类中访问常量
    }
}
echo MyClass::constant."<br>";//使用类名来访问常量
$class=new MyClass();
$class->showConstant();



//4.4.5 _toString()方法---------------------------------------------------------
class TestClass{
    public $foo;
    public function __construct($foo) {
        $this->foo = $foo;
    }
    //定义一个_toString()方法，返回一个成员属性
    public function _toString() {
        return $this->foo;
    }
}
$class=new TestClass('Hello');
//直接输出对象
echo $class;//输出结果为‘Hello’



//4.4.6 _clone方法 克隆出一个对象------------------------------------------
class animal{
    private $name;//成员属性name
    private $color;
    private $age;
    public function __construct($name, $color, $age) {
        $this->name = $name;
        $this->color = $color;
        $this->age = $age;
    }
    public function getInfo() {
        echo'名字：'. $this->name.'颜色：'. $this->color. '年龄'. $this->age.'.<br>';
    }
}
$dog=new animal('狗','1','黑');
$dog2=clone $dog;//克隆对象
$dog2->getInfo();




//特殊应用 克隆时自动调用
class animal2{
    private $name;//私有成员属性name
    private $color;
    private $age;
    public function __construct($name, $color, $age) {
        $this->name = $name;
        $this->color = $color;
        $this->age = $age;
    }
    public function getInfo() {
        echo'名字：'. $this->name.'颜色：'. $this->color. '年龄'. $this->age.'.<br>';
    }
    public function _clone() {
        $this->name = '狗';
        $this->color = '黑';
        $this->age = '2岁';
    }
}
$pig2=new animal2('猪','白','1岁');
$pig2->getInfo();
$pig3=clone $pig2;//克隆对象
$pig3->getInfo();

//4.4.7 _call方法  在调用方法不存在时会自动调用，程序继续执行
class T_T{
    function _call($function_name, $args){
        print"你所调用的函数：$function_name（参数：";
        print_r($args);
        print")不存在！<br>";
    }
}
$test=new T_T();
$test->demo("one","two","three");















