<?php
//=======================================重载============================================
class emoguoshi{//恶魔果实
    public $name;
    public $weidao;//味道
    public $nengli;//能力
    public function __construct($name,$weidao,$nengli){
        $this->name = $name;
        $this->weidao = $weidao;
        $this->nengli = $nengli;
    }
    public function getInfo(){
        echo "果实的名称:". $this->name."<br>";
        echo "果实的味道:". $this->weidao."<br>";//"."的作用--->字符串拼接
        echo "果实的能力:". $this->nengli."<br>";
    }
}
/**
 * 定义一个bird类，使用extends关键字来继承恶魔果实类，作为他的子类
 */
class bird extends emoguoshi{
    public $name;//bird类的自有属性
    public function fly(){//bird类的自有的方法
        echo"我无敌难吃！！！！";
    }
}
$yanyanguoshi = new bird("炎炎果实","麻辣的屎","自然系掌控火元素");
$yanyanguoshi->getInfo();
$yanyanguoshi->nanchi();

// 新建一个[恶魔果实]类
class emoguoshi {

    // ====================成员属性========================
    public $name;
    public $weidao;
    public $nengli;

    // ====================成员属性========================
    public function getInfo() {
        echo "物品的名称:". $this->name  . "<br>";
        echo "物品的味道:". $this->weidao . "<br>";
        echo "物品的能力:". $this->nengli   . "<br>";
    }

    // 构造函数 (创建对象时为对象赋初始值)
    // parent::__construct($name, $weidao, $nengli)
    public function __construct($name, $weidao, $nengli) {
        $this->name  = $name;
        $this->weidao = $weidao;
        $this->nengli   = $nengli;
    }

    // 析构函数 (销毁对象时执行)
    public function __destruct()
    {
        echo "再见:" . $this->name . "。<br>";
    }
}


// 类的继承，新建一个[克星]子类继承动物类
class kexing extend animal{
    // 成员属性：海水
    public $haishui;
    // 重载：继承类的方法的继承写法[parent::]
    public function getInfo(){
        // 使用[parent::]来获取父类的成员方法或者属性
        parent::getInfo();
        // echo "物品的名称:". $this->name  . "<br>";
        // echo "物品的味道:". $this->weidao . "<br>";
        // echo "物品的能力:". $this->nengli   . "<br>";
        // 书写克星特有的属性，信息
        echo "克星有" . $this->haishui . '海水<br>';
        $this->fly();
    }

    // 子类[bird]具有的成员方法
    public function fly(){
        echo $this->name . "掉进去会死的...";
    }
 
    // 继承类的构造函数的继承写法[parent::]
    public function __construct($name, $color, $nengli, $wing) {
        parent::__construct($name, $color, $nengli) ;
        // 克星特有的克制属性需要单独书写
        $this->wing = $wing;
    }
}


// 如何创建一个对象？
$pig = new animal("名字","颜色","年龄");


// 类和对象的关系
// 类的实例化结果  ->对象
// 对一类对象的抽象->类