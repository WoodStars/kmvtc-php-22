<?php

// 新建一个[恶魔果实]类
class emoguoshi {

    // ====================成员属性========================
    private $name;
    private $weidao;
    private $nengli;

    // ====================成员属性========================
    public function getInfo() {
        echo "果实的名称:". $this->name  . "<br>";
        echo "果实的味道:". $this->weidao . "<br>";
        echo "果实的能力:". $this->nengli   . "<br>";
    }

    // 构造函数 
    public function __construct($name, $weidao, $nengli) {
        $this->name  = $name;
        $this->weidao = $weidao;
        $this->nengli   = $nengli;
    }

}

$yanyanguoshi = new animal("炎炎果实","超级麻辣味的屎","掌控火");

// 需要查看浏览器回显报错，下属代码在类的外部尝试获取animal类的实体化dog对象的name属性
echo $yanyanguoshi->name;

//==============================================================================================
// 访问修饰符
// 1.public    公有的  无访问限制，不做特别说明，均默认为声明的是public，成员内部、外部均可访问
// 2.private   私有的  仅针对当前class享有访问权限，当前类的子类以及类的外部均不允许访问
// 3.protected 保护的  所属类及其子类享有访问权限，外部代码不可访问

class MyClass 
{
    public    $public    = 'Public';
    //第一个public是访问修饰符；
    //第二个public是一个叫做public的成员属性，变量
    //第三个public是$public的成员属性的值
    private   $private   = 'Private';
    protected $protected = 'Protected';


    function printHello()  {
        // 成员内部引用成员属性，无任何权限问题
        echo $this->$public . "<br>";
        echo $this->$private . "<br>";
        echo $this->$protected . "<br>";
    }
}

$myClass = new MyClass();
$myClass->printHello();
/**
 * Public
 * Private
 * Protected
 */

// 在成员外部进行成员属性的直接访问，访问修饰符起到权限判断的作用
echo $myClass->public;
echo $myClass->private;
echo $myClass->protected;

/**
 * Public
 * Fatal error : ......(不能访问 private & protected 属性)
 * Fatal error : ......(不能访问 private & protected 属性)
 */

 //子类中的访问修饰符========================
class MyClass2 extend MyClass {
    protected $protected = 'Protected2';

    function printHello()  {
        // 成员内部引用成员属性，无任何权限问题
        echo $this->$public . "<br>";
        echo $this->$private . "<br>";    // 不显示
        echo $this->$protected . "<br>";
    }
}

$myClass2 = new MyClass2();//实例化当前类//在成员外部进行成员属性的直接访问，不可访问
echo $myClass2->public;//输出public
echo $myClass2->private;
echo $myClass2->protected;

$myClass2->printHello();//输出3个成员属性
/**
 * 定义类MyClass2
 */
class MyClass2 extends MyClass2 {//继承MyClass2
    //可以访问公共属性和保护属性，但是私有属性不可访问
    public $public = "Protected2";
    function printHello() {
        echo $this->$public . "<br>";
        echo $this->$protected . "<br>";
        echo $this->$private . "<br>";
    }
}
$myClass3 = new MyClass3();
echo $myClass3->public;//输出$public
//
echo $myClass3->private;//未定义
$myClass3->printHello();//只显示公共属性和保护属性，不显示私有属性

class emoguoshi {

    // ====================成员属性========================
    private $name;
    private $weidao;
    private $nengli;

    // ====================成员属性========================
    public function getInfo() {
        echo "果实的名称:". $this->name  . "<br>";
        echo "果实的味道:". $this->weidao . "<br>";
        echo "果实的能力:". $this->nengli   . "<br>";
    }

    // 构造函数 
    public function __construct($name, $weidao, $nengli) {
        $this->name  = $name;
        $this->weidao = $weidao;
        $this->nengli   = $nengli;
    }
    //4.3.2.1 get  (取值)
    public function __get($property_name){
        // 检查属性 this-> 当前类  的 property_name 属性 是否存在
        if(isset($this->$property_name)){
            return (this->$property_name);
        }else {
            return(NULL);
        }
    }
    //4.3.2.2 set  (赋值)
    public __set($property_name,$value){
        $this->$property_name = $value;
    }

    //4.3.2.3 isset  (检查)
    private function __isset($property_name) {
        return isset($this->$property_name);
    }

    //4.3.2.4 unset  (删除)
    private function __unset($property_name) {
        return unset($this->$property_name);
    }
}

$panda = new animal();

/**
 * private[私有的]
 * 1、仅针对当前class享有访问权限
 * 2、当前类的子类不允许访问
 * 3、类的外部均不允许访问
 */

 //!! __get __set 方法写了以后就会自动对private属性取值或者赋值

 // todo 需要同学测试 将__get __set 方法注释以后运行的结果和不注释运行的结果，作比较
public function_get($panda->name="路飞";)
public function_set($panda->color="橡胶果实";)
$panda->age=12;

echo $panda->name;
echo $panda->color;
echo $panda->age;

//todo 在类的外部使用isset函数来判断实体的变量是否设定


//4.3.2.4 unset 测试用例
echo $panda->name;//"熊猫";
unset($panda->name);
echo $panda->name;// 空输出





























