<?php
//self关键字
class animal {
    private static $firstCry =0;//私有的静态变量
    private $lastCry;
    //构造方法
    function __construct() {
        $this->lastCry =++self:: $firstCry;
    }
    function printLastCry() {
        var_dump( $this->lastCry);
    }
}
$bird = new animal();  //实例化对象
$bird->printLastCry(); //输出：int(1)


//const关键字
class MyClass {
    //定义一个常量
    const constant ='我是个常量!';
    function showConstant() {
        echo self::constant."<br>"; //类中访问常量
    }
}
echo MyClass::constant."<br>"; //使用类名来访问常量
$class = new MyClass();
$class->showConstant();


//__toString方法
class TestClass {
    public $foo;
    public function __construct( $foo) {
        $this->foo = $foo;
    }
    //定义一个__toString()方法，返回一个成员属性 $foo
    public function __toString() {
        return $this->foo;
    }
}
$class = new TestClass('Hello');
//直接输出对象
echo $class; //输出结果为'Hello'


//__clone方法
class animal {
    public $name; //成员属性 name
    public $color;
    public $age;
    public function __construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    }
    public function getInfo() {
        echo'名字:'. $this->name. ,'颜色:'. $this->color. ,'年龄:'. $this->age. '.';
    }

}
$pig =new animal('猪','白色','1岁');
$pig2 = clone $pig
#pig2->getInfo();


//__call方法
class Test {
    function __call( $function_name. $args) {
        print "你所调用的函数：$function_name(参数:";
        print_r( $arge);
        print")不存在! <br>";
    }
}
$test =new Test();
$test->demo("one","two","three");

?>