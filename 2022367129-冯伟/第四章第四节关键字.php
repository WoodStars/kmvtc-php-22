<?php

//4.4.1 static 静态的
// 新建一个[鲜花]类
class flower {

    // ====================成员属性========================
    private $name;
    private $color;
    private $florescence;
    private static $fruit = "果实"; 
    // ====================成员属性========================
    // public function getInfo() {
    //     echo "鲜花的名称:". $this->name  . "<br>";
    //     echo "鲜花的颜色:". $this->color . "<br>";
    //     echo "鲜花的花期:". $this->florescence  . "<br>";
    // }

    // 构造函数 
    public function __construct($name, $color, $florescence) {
        $this->name  = $name;
        $this->color = $color;
        $this->florescence   = $florescence;
    }

    private function getInfo() {
        echo "鲜花是有果实的";   
    }
}

//echo flower::$fruit;
flower::getInfo();


class flower{
    final public function getinfo(){

    }
}
class orchid extends flower{
    public function getinfo() {

    }
}
$natallily = new orchid();
$natallily ->getinfo();

?>