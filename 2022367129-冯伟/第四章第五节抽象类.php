<?php
header("Content-Type:test/html;charset=utf-8");
//使用abstract关键字声明一个抽象类
abstract class animal {
    //在抽象类中声明抽象方法
    abstract public function shout();
}
//定义sheep类继承animal类
class sheep extends animal {
    //实现抽象方法
    public function shout () {
        echo "咩咩······<br>";
    }
}
//定义bird类继承animal类
class bird extends aniaml {
    //实现抽象方法
    public function shout() {
        echo "叽叽······<br>";
    }
}
$dog=new dog();
$dog->shout();
$cat=new cat();
$cat->shout();

?>