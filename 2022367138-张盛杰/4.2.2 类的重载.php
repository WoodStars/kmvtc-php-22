<?php

// 新建一个[武侠]类
class Martialarts {

    // ====================成员属性========================
    public $name;
    public $weapons;
    public $age;

    // ====================成员属性========================
    public function getInfo() {
        echo "武侠的名称:". $this->name  . "<br>";
        echo "武侠的武器:". $this->weapons . "<br>";
        echo "武侠的年龄:". $this->age   . "<br>";
    }

    // 构造函数 (创建对象时为对象赋初始值)
    // parent::__construct($name, $weapons, $age)
    public function __construct($name, $weapons, $age) {
        $this->name  = $name;
        $this->weapons = $weapons;
        $this->age   = $age;
    }

    // 析构函数 (销毁对象时执行)
    public function __destruct()
    {
        echo "再见:" . $this->name . "<br>";
    }
}


// 类的继承，新建一个[stunt]子类继承动物类
class stunt extends Martialarts{
    // 成员属性：秘籍
    public $cheats;
    // 重载：继承类的方法的继承写法[parent::]
    public function getInfo(){
        // 使用[parent::]来获取父类的成员方法或者属性
        parent::getInfo();
        // echo "武侠的名称:". $this->name  . "<br>";
        // echo "武侠的武器:". $this->weapons . "<br>";
        // echo "武侠的年龄:". $this->age   . "<br>";
        // 书写绝技特有的属性，信息
        echo "武侠有" . $this->cheats . '秘籍<br>';
        $this->stunt();
    }

    // 子类[bird]具有的成员方法
    public function stunt(){
        echo "is stunting!!!";
    }
 
    // 继承类的构造函数的继承写法[parent::]
    public function __construct($name, $weapons, $age, $scheats) {
        parent::__construct($name, $weapons, $age);
        // 武侠特有的秘籍属性需要单独书写
        $this->cheats=$cheats;
    }
}


// 如何创建一个对象？
$pig = new Martialarts("赵云","武器","年龄");


// 类和对象的关系
// 类的实例化结果  ->对象
// 对一类对象的抽象->类
