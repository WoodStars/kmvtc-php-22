<?php
//4.4.1static关键字
class  animal {
    private $name;
    private $color;
    private $age;
    private static $sex="雄性"; //私有的静态属性
    //静态成员方法
    public static function getInfo() {
        echo "动物是雄性的。";
    }
}
//echo animal:: $sex; //错误，静态属性私有，外部不能访问
animal::getInfo(); //调用静态方法


//4.4.1
class animal {
    private $name;
    private $color;
    private $age;
    private static $sex="雄性"; //私有的静态属性
    public function __construct($name, $color,$age) {
        $this->name=$name;
        $this->color=$color;
        $this->age=$age;
    }
    public function getInfo() {
        echo "动物的名称:" . $this->name . "<br>";
        echo "动物的颜色:" . $this->color . "<br>";
        echo "动物的年龄:" . $this->age . "<br>";
        //echo "动物的性别:" . self::$sex;
        self::getSex();
    }
    private static function getSex() {
        echo "动物的性别:" . self::$sex;
    }
}
$dog=new animal("小狗","黑色","4");
$dog->getInfo();


//4.4.2final关键字
class animal {
    final public function getInfo() {

    }
}
class bird extends animal {
    public function getInfo() {

    }
}
$crow=new bird();
$crow->getinfo();

//4.4.3self关键字
class animal {
    private static $firstcry=0; //私有的静态变量
    private $lastcry;
    //构造方法
    function __construct() {
        $this->lastcry=++self::$firstcry;
    }
    function printlastcry() {
        var_dump($this->lastcry);
    }
}

$bird=new animal(); //实例化对象
$bird->printlastcry();//输出：int(1)


// 4.4.4 const 常量
class myclass {
    //定义一个常量
    const constant = '我是一个常量!';
    function showconstant() {
        echo self::constant . "<br>"; 

    }
}
ehco mycalss::constant . "<br>";
$class=new mycalss();
$class->showconstant();


// 4.4.5 __tostring()方法
// __tostring
class testclass {
    public $foo;
    public function __construct($foo) {
        $this->foo=$foo;
    }
    public function __tostring() {
        return $this->foo;

    }
}
$class=new testclass('hello');
//直接输出对象
echo $class;


//4.4.6 __clone()方法


class animal {
    private $name;
    private $color;
    private $age;
    public function __construct($name,$color,$age) {
        $this->name=$name;
        $this->color=$color;
        $this->age=$age;
    }
    public function getInfo() {
        echo '名字:' . $this->name . '颜色:' . $this->color . '年龄:' . $this->age . '.';
    }
}
$pig=new animal('猪','白色','1岁');
$pig2=clone $pig; //克隆对象
$pig2->getinfo();

//4.4.7__call方法
class test {
    function __call($function_name,$args) {
        print "你所调用的函数:$function_name(参数:";
        print_r($args);
        print ")不存在!<br>";
    }
}
$test=new test();
$tets->demo("one", "two","three");

?>