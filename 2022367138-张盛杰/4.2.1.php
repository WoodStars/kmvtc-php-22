<?php
class Martialarts {
    public $name;
    public $weapons;
    public $age;
    public function __construct($name,$weapons,$age) {

        $this->name = $name;
        $this->weapons = $weapons;
        $this->age = $age;
    }
    public function getinfo() {
        echo "武侠的名称;". $this->name. "<br>";
        echo "武侠的武器;". $this->weapons. "<br>";
        echo "武侠的年龄;". $this->age. "<br>";
    }
}
/**
 * 定义一个stunt类，使用extends关键字来继承animal类，作为animal类的子类
 */
class stunt extends Martialarts {
    public $cheats;//stunt类的自有属性$cheats
    public function stunt() {//stunt类自有的方法
        echo 'I have stunts!!!';
    }
}
$crow=new stunt("赵云" , "涯角枪" , "33");
$crow->getinfo();
$crow->stunt();


?>