<?php

// 新建一个[武侠]类
class Martialarts {

    // ====================成员属性========================
    private $name;
    private $weapons;
    private $age;

    // ====================成员属性========================
    public function getInfo() {
        echo "武侠的名称:". $this->name  . "<br>";
        echo "武侠的武器:". $this->weapons . "<br>";
        echo "武侠的年龄:". $this->age . "<br>";
    }

    // 构造函数 
    public function __construct($name, $weapons, $age) {
        $this->name  = $name;
        $this->weapons = $weapons;
        $this->age   = $age;
    }



$z = new Martialarts("张飞","丈八蛇矛","33");

// 需要同学查看浏览器回显报错，下属代码在类的外部尝试获取Martialarts类的实体化Martialarts对象的name属性
echo $Martialarts->name;
}
//==============================================================================================
// 访问修饰符
// 1.public    公有的  无访问限制，不做特别说明，均默认为声明的是public，成员内部、外部均可访问
// 2.private   私有的  仅针对当前class享有访问权限，当前类的子类以及类的外部均不允许访问
// 3.protected 保护的  所属类及其子类享有访问权限，外部代码不可访问

class MyClass 
{
    public    $public    = 'Public';
    //第一个public是访问修饰符；
    //第二个public是一个叫做public的成员属性，变量
    //第三个public是$public的成员属性的值
    private   $private   = 'Private';
    protected $protected = 'Protected';


    function printHello()  {
        // 成员内部引用成员属性，无任何权限问题
        echo $this->$public . "<br>";
        echo $this->$private . "<br>";
        echo $this->$protected . "<br>";
    }
}

$myClass = new MyClass();
$myClass->printHello();
/**
 * Public
 * Private
 * Protected
 */

// 在成员外部进行成员属性的直接访问，访问修饰符起到权限判断的作用
echo $myClass->public;
echo $myClass->private;
echo $myClass->protected;

/**
 * Public
 * Fatal error : ......(不能访问 private & protected 属性)
 * Fatal error : ......(不能访问 private & protected 属性)
 */

 //子类中的访问修饰符========================
class MyClass2 extends MyClass {
    protected $protected = 'Protected2';

    function printHello()  {
        // 成员内部引用成员属性，无任何权限问题
        echo $this->$public . "<br>";
        echo $this->$private . "<br>";    // 不显示
        echo $this->$protected . "<br>";
    }
}

$myClass2 = new MyClass2();
echo $myClass2->public;
echo $myClass2->private;
echo $myClass2->protected;

$myClass2->printHello();

class zsj {
//1.__get()方法
private $property_name;
public function __get($property_name) {
    if(isset($this->$property_name)) {
        return($this->$property_name);
    }else{
        return(NULL);
    }
}

//2.__set()方法
public function __set($property_name,$value) {
    $this->$property_name= $value;
}


//3. isset  (检查)
    public function __isset($property_name) {
        return isset($this->$property_name);
    }

//4. unset  (删除)
    public function __unset($property_name) {
        unset($this->$property_name);
    }
}



class anmial{
    private $name;//私有的成员属性
    private $color;
    private $age;
    //__get()方法用来获取私有属性
    function __get($property_name) {
        if(isset($this->$property_name)) {
            return($this->$property_name);
        }else{
            return(NULL);
        } 
    }
    //__set方法用来设置私有属性
    public function __set($property_name,$value) {
        $this->$property_name= $value;
    }
    
    
    //__isset()方法
        private function __isset($property_name) {
            return isset($this->$property_name);
        }
    }   
    //__unset()方法
        function __unset($property_name) {
            unset($this->$property_name);
        }
        $kitten=new animal();
        $kitten->name="小猫";
        echo var_dump( isset( $kitten->name)) . "<br>";
        //调用__isset方法，输出：bool（true）
        echo $kitten->name . "<br>";  //输出:小猫
        unset($kitten->name); //调用__unset()方法
        echo $kitten->name;//无输出

?>