<?php
/*abstract*/

// 如果一个类里面有抽象方法，那么这个类必须是抽象类
abstract class animal
{
    // 抽象方法
    abstract function shout();

}

// 如果一个类不是抽象类,那么这个类必须实现抽象方法
class animal333
{
    // 抽象方法
     function shout(){
        echo "动物叫声";
     };

}


class dog extends animal{
    function shout(): void
    {
        echo "汪汪汪";
    }
}

class cat extends animal{
    function shout(): void
    {
        echo "喵喵喵";
    }
}

// 实例化animal
// $animal = new animal();
// 抽象类不能被实例化

// 实例化dog类
$dog = new dog();
// 调用dog类的shout方法
$dog->shout();
echo "\n";
// 实例化cat类
$cat = new cat();
// 调用cat类的shout方法
$cat->shout();
echo "\n";