<?php


////4.4.1 static 静态的
//// 新建一个[动物]类
//class animal {
//
//    // ====================成员属性========================
//    private $name;
//    private $color;
//    private $age;
//    private static $sex = "雄性";
//    // ====================成员属性========================
//    // public function getInfo() {
//    //     echo "动物的名称:". $this->name  . "<br>";
//    //     echo "动物的颜色:". $this->color . "<br>";
//    //     echo "动物的年龄:". $this->age   . "<br>";
//    // }
//
//    // 构造函数
//    public function __construct($name, $color, $age) {
//        $this->name  = $name;
//        $this->color = $color;
//        $this->age   = $age;
//    }
//
//    private function getInfo()  {
//        echo "动物是雄性的";
//    }
//}
//
//echo animal::$sex;// 静态成员不能在外部访问
//echo animal::$color;// 只有static属性可以通过类来访问，因为static 定义的属性是在类加载的时候压入内存的，非static的，在每一个实例被初始化的时候再亚茹内存，有多个实例，就有多个属性
//animal::getInfo();// 使用静态方法

//4.4.2 final 常量 用于定义 class 和 function /// define 成员属性
// final 定义的类不能被继承（inherit）

// 自己尝试写一个父类一个子类，在父类使用final关键词定义，看报错信息。

// can not override(重写、覆盖) [final] method —— animal::getInfo() on line 12

//4.4.3 self 指向【类-class】的本身，不指向任何实例化的对象

class animalXxl
{
    private static int $firstCry = 0;// 私有的静态变量
    private int $lastCry;

    // construct function
    function __construct()
    {
        $this->lastCry = self::$firstCry+2;
//        $this->lastCry = ++self::$firstCry;
//        $this->lastCry = ++$this->lastCry;
        $this->lastCry = $this->lastCry + 1;
    }

//    ++0
//    = 0+1
    // void 不带返回值
    function printLastCry(): void
    {
        var_dump($this->lastCry);
    }

}

$animalXxl1 = new animalXxl();
$animalXxl1->printLastCry(); //int(1)


//4.4.4 const 常量
// const 用于定义常量，常量是不可改变的值
// 常量的值在定义后不能被修改
// 常量的值可以是标量数据类型
// 常量的值不需要 $ 符号
// 常量在定义后可以直接使用，不需要 $ 符号
// 常量可以通过类名::常量名 的方式访问
// 常量可以通过 define() 函数定义


// 使用define()函数定义常量
const PI = 3.14;
echo PI . "<br>";
//PI = 3.16; // 报错，常量不能被修改

class Cla2{
    const constant = '我是一个常量';

    function showConstant(): void
    {
        echo self::constant . "<br>";//
    }
}

echo Cla2::constant . "<br>";// 我是一个常量
$cla2 = new Cla2();
$cla2->showConstant();// 我是一个常量


//4.4.5 __toString() 方法
// __toString() 方法用于一个类被当成字符串时应怎样回应
// __toString() 方法必须返回一个字符串，否则会发出一个 E_RECOVERABLE_ERROR 级别的致命错误 FATAl ERROR
// __toString() 方法只能被声明为 public
// __toString() 方法在直接输出对象时自动调用

class testToString
{
    public mixed $foo;

    /**
     * @return mixed
     */
    public function getFoo()
    {
        return $this->foo;
    }

    public function setFoo($foo): void
    {
        $this->foo = $foo;
    }

    // 构造函数,初始化,实例化对象的时候调用,不需要手动调用,自动调用,只调用一次
    public function __construct($foo)
    {
        $this->foo = $foo;
    }
    public function __toString()
    {
        return $this->foo;
    }
}

$testToString = new testToString("我是一个对象");
echo $testToString;// 我是一个对象

//