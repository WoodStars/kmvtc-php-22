<?php

abstract class animal
{
    // 抽象方法
    abstract function shout();

}

class human
{
    function shout()
    {
        echo "人类叫声";
    }
}

class dog extends animal
{
    function shout(): void
    {
        echo "汪汪汪";
    }
}

class cat extends animal
{
    function shout(): void
    {
        echo "喵喵喵";
    }
}

function animalshout($obj)
{
    if ($obj instanceof animal) {
        $obj->shout();
    } else {
        echo "不是动物类";
    }
}

animalshout(new human());
echo "\n";
$dog = new dog();
animalshout($dog);