<?php

//① 连接数据库 使用命令[mysqli_connect] 返回一个连接对象$conn  (connection) 这个是和数据库持久的连接，通过该连接conn操作数据库
//mysqli_connect('数据库地址', '用户名', '密码', '数据库名', '端口', '套接字')
//参数：数据库地址（localhost 本机地址 127.0.0.1），用户名(数据库的用户名)，密码（数据库的密码），数据库/架构schema名（database name ），端口(port mysql默认3306)，套接字
$conn = @mysqli_connect('localhost', 'bob',
    '123456', 'woodStars', '3306')
or die('Could not connect to MySQL: ' . mysqli_connect_error());

//② 选择数据库，如果mysqli_connect未指定数据库或者需要切换数据库的时候，使用当前命令
$select = mysqli_select_db($conn, 'woodStars');
if ($select) {
    echo "连接成功";
} else {
    echo "连接失败";
}

//③ mysqli_query 执行sql语句
//mysqli_query('连接对象$conn', 'sql语句【$select_sql】')
//$result = mysqli_query($conn, $insert_sql);

// 查询数据
$select_sql = "select * from tb_admin";
$result = mysqli_query($conn, $select_sql);// result 是函数返回的数据指针

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    echo $row['user'] . ' ' . $row['password'] . ' ' . $row['create_time'] . ' ' . $row['email'] . '<br>';
}

//④ 关闭数据库连接
mysqli_close($conn);




//数据库名称为 library

// 创建一张表 图书主数据（） 包含 书名、书号、作者、出版社、出版时间、价格、ISBN、页数、分类、库存、简介
// 设置主键为 书号
// 创建一张表 图书借阅记录（） 包含 书号、借阅人、借阅时间、归还时间、借阅状态
// 设置主键为 自增主键
// 创建一张表 用户信息（） 包含 用户名、密码、邮箱、电话、地址、注册时间、用户类型
// 设置主键为 用户名

// 新增对应数据 每张表不少于5条

