<?php
// 新建一个[水果]类
class fruit  {

    // ====================成员属性========================
               public $name;
               public $color;
               public $price;

    // 构造函数 (创建对象时为对象赋初始值)
           public function __construct($name, $color, $price) {
                  $this->name  = $name;
                  $this->color = $color;
                  $this-> price  = $price;
    }

 // ====================成员方法========================
    public function getInfo() {
        echo "水果的名称:". $this->name  . "<br>";
        echo "水果的颜色:". $this->color . "<br>";
        echo "水果的价格:". $this->price . "<br>";
    }
}

/* *
*定义一个类, 使用extends 关键字来继承fruit类, 作为fruit类的子类
*/
class apple extends fruit {
          public $wd;    //苹果有属性味道
         public function sweetness() {  //苹果的味道甜
             echo;
      }
}
$crow=new apple ("苹果" ,"红色" , 3);
$crow->getInfo();
$crow->sweetness();
?>