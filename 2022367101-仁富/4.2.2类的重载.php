<?php
// 新建一个[动物]类
class animal {

    public $name;
    public $color;
    public $age;
    public function getInfo() {
        echo "动物的名称:". $this->name  . "<br>";
        echo "动物的颜色:". $this->color . "<br>";
        echo "动物的年龄:". $this->age   . "<br>";
    }
    // 构造函数 (创建对象时为对象赋初始值)
     public function_construct( $name, $color, $age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
}
    //定义一个insects(昆虫)类，使用extends关键字来继承animal类，作为animal类的子类
    class butterfly extends insect{
        //成员属性: 翅膀
    public $wing;    //insects类自有的属性 $wing (翅膀)
    public function getInfo() {
        //使用[parent::]来获取父类的成员方法或属性
        parent::getInfo();
        echo "昆虫类有" . $this->wing .'翅膀<br>';
        $this->fly();
    }

    public function fly() {
        //昆虫类自有的方法
        echo  "我会飞翔!!!";
    }
    
    //继承类的构造函数的继承写法[parent::]
    public function_construct( $name, $color, $age, $swing) {
        parent::_construct( $name, $color, $age);
        $this->wing= $wing;
    }
}
$bee=new insects("蝴蝶","五彩斑斓的",3,"漂亮的");
$bee->getInfo();
?>

