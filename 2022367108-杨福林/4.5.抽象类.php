<?php
header( "Content-Type:text/html;charset=utf-8");
abstract class animal{
    abstract public function shout();
}
class dog extends animal {
    public function shout() {
        echo "汪汪……<br>";
    }
}
class cat extends animal {
    public function shout() {
        echo "喵喵……<br>";
    }
}
$dog=new dog();
$dog->shout();
$cat=new cat();
$cat->shout();
?>