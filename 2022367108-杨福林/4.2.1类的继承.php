<?php
// 新建一个[动物]类
class animal {
    public $name;
    public $color;
    public $age;
    public function __construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    }
public function getInfo() {
        echo "动物的名称:". $this->name  . "<br>";
        echo "动物的颜色:". $this->color . "<br>";
        echo "动物的年龄:". $this->age   . "<br>";
    }
}
  class bird extends animal {
       public $wing;
       public function fly() {
            echo 'I can fly!!!';
       }
  }
$cat = new bird ("猫","白色",4);
$cat->getInfo();
$cat->fly();
   



