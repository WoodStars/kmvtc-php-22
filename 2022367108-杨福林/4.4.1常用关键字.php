<?php
header("Conten-Type:text/html;charset=utf=8");
class plant {
    private $name;
    private $color;
    private $age;
    private static $sex= "雌性"; 
    public function __construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    }
    public function getInfo() {
        echo "植物的名称:". $this->name  . "<br>";
        echo "植物的颜色:". $this->color . "<br>";
        echo "植物的年龄:". $this->age   . "<br>";
        self::getSex();
   }
  private static function getSex() {
        echo "植物的性别:" . self::$sex; 
   }
}
$lily=new plant("百合","白色",4);
$lily->getInfo();
?>