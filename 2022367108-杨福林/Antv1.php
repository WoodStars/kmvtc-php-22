<div class="page">
  <h1 style="width:100%">我的账本</h1>
  <div id="zhuzhuangtu" class="zhuzhuangtu"></div>
  <div id="bingtu" class="bingtu"></div>
  <div id="mixzhu" class="mixzhu"></div>

</div>

<script src="https://unpkg.com/@antv/g2/dist/g2.min.js"></script>
<script>
  const data2 = [
    { item: '拼团', count: 40, percent: 0.40},
    { item: '支付宝', count: 21, percent: 0.21},
    { item: '淘宝', count: 17, percent: 0.17 },
    { item: '实体店', count: 13, percent: 0.13 },
    { item: '拼多多', count: 9, percent: 0.09 },
  ];
  const chart2 = new G2.Chart({
    container: 'bingtu',

  });

  chart2.coordinate({ type: 'theta', outerRadius: 0.8 });

  chart2
    .interval()
    .data(data2)
    .transform({ type: 'stackY' })
    .encode('y', 'percent')
    .encode('color', 'item')
    .legend('color', { position: 'bottom', layout: { justifyContent: 'center' } })
    .label({
      position: 'outside',
      text: (data2) => `${data2.item}: ${data2.percent * 100}%`,
    })
    .tooltip((data) => ({
      name: data2.item,
      value: `${data2.percent * 100}%`,
    }));

  chart2.render();


  const data1 = [
    { genre: 'periphery', sold: 1072.60 },
    { genre: 'novel', sold: 1457 },
    { genre: 'clothing', sold: 1364 },
    { genre: 'cosmetics', sold: 675 },
    { genre: 'shoes', sold: 786},
  ];

  // 初始化图表实例
  const chart1 = new G2.Chart({
    container: 'zhuzhuangtu',

  });

  // 声明可视化
  chart1
    .interval() // 创建一个 Interval 标记
    .data(data1) // 绑定数据
    .encode('x', 'genre') // 编码 x 通道
    .encode('y', 'sold'); // 编码 y 通道

  // 渲染可视化
  chart1.render();


  const data3 = [
    { name: 'Last semester', 月份: 'Jan.', 月销: 212.60 },
    { name: 'Last semester', 月份: 'Feb.', 月销: 1402.51 },
    { name: 'Last semester', 月份: 'Mar.', 月销: 2155.46 },
    { name: 'Last semester', 月份: 'Apr.', 月销: 2055.20 },
    { name: 'Last semester', 月份: 'May', 月销: 7309.26 },
    { name: 'Last semester', 月份: 'Jun.', 月销: 2068.66 },
    { name: 'Last semester', 月份: 'Jul.', 月销: 2784.22 },
    { name: 'Last semester', 月份: 'Aug.', 月销: 10000 },
    { name: 'Next semester', 月份: 'sep.', 月销: 3148.91 },
    { name: 'Next semester', 月份: 'Oct.', 月销: 2913.06 },
    { name: 'Next semester', 月份: 'Nov', 月销: 2731.57 },
    { name: 'Next semester', 月份: 'Dec.', 月销: 2174.27 },
  ];

  const chart3 = new G2.Chart({
    container: 'mixzhu',
  });

  chart3
    .interval()
    .data(data3)
    .encode('x', '月份')
    .encode('y', '月销')
    .encode('color', 'name')
    .transform({ type: 'stackY' })
    .interaction('elementHighlight', { background: true });

  chart3.render();


</script>

<style>
  .page {
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
  }

  .zhuzhuangtu {
    width:50%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;

  }

  .bingtu {
    width:50%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
  }

  .mixzhu{
    width:100%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
  }

  const data = [
  { plant: 'rose', sales: 150 },
  { plant: 'tulip', sales: 125 },
  { plant: 'lily', sales: 86 },
  { plant: 'pink', sales: 145 },
  { plant: 'iris', sales: 48 },
  { plant: 'yucca', sales: 94 },
  { plant: 'orchid', sales: 56 },
  { plant: 'peony', sales: 38 },
];

const chart = new Chart({
  container: 'container',
  autoFit: true,
});

chart
  .interval()
  .coordinate({ transform: [{ type: 'transpose' }] })
  .data(data)
  .encode('x', 'plant')
  .encode('y', 'sales');

chart.render();





</style>