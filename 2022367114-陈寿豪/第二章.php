<?php 
//2.1.1调用函数                                                           
 function function_name($a , $b , $c){
    function body 函数体
        return $a + $b;
}

// /**
//  * 函数的声明（定义）
//  * 求平方数
//  */
function custom_func($num){
    return "$num*$num=" . $num*$num . "<br>";
}

// // 函数的调用
echo custom_func(5);
echo custom_func(6);
echo custom_func(7);
echo custom_func(8);


function unknowm( $a , $b ){
    $a = $a +5;
    $b = $b -5;
    $c = $a * $b;
    $d = $c / $c;
    return $d;
}

echo unknowm(10,11);



// /**
//  * 按值传参
//  */
function fun_2( $a  ){
     $a = $a +5;
     return $a;
}


// /**
//  * 按引用值传参
//  */
function fun_3(& $a  ){
    $a = $a +5;
    return $a;
}

// /**
//  * 按引用值传参
//  */
function fun_default(& $a  ){
    $a = $a +5;
    return $a;
}


$x = 18;
echo fun_2($x);

echo $x;

/**
 * 按默认值传参
 * $a 基本工资
 * $b 绩效
 * $default1 提成
 * $default2 扣减比例
 */
function fun_default($a, &$b, $default1 = 1.1, $default2 = 0.1)
{
    todo
    $sal = $a + $b * $default1 - ($a + $b * $default1) * $default2;
    return $sal;
}

$jixiao = 2000;
echo fun_default(5000,$jixiao);
echo "<br>";
fun_default(5000,$jixiao,1.1,0.1);
echo fun_default(5000,$jixiao,1.5);
echo "<br>";
echo fun_default(5000,$jixiao,1.6,1);
echo "<br>";

//2.1.4变量函数
header("content-type:text/html;charset=utf-8");
function com(){
    echo"我来了.<br>";
}   
function go($name = "jack"){
    echo $name . "走了.<br>";
}
function back($string){
    echo $string . "又回来了.<br>";
}
$func ='come';
$func();
$func ='go';
$func('Tom');
$func ='back';
$func('lily');

//2.1.5函数的引用
function &test(){
    static $b = 0;
    $b = $b + 1;
    echo $b . "<br>";
    return $b;
}

$a = test();
echo $a . "<br>";
$a = 5;
echo test() ."<br>";
echo $a . "<br>";

//2.2..1变量函数库
$a ="Hello";
echo gettype( $a)."<br>";
$b=array(1,2,5);
echo gettype( $b)."<br>";
echo intval(4.5)."<br>";
var_dump( $a);
echo"<br>";
var_dump( $a,$b);


//2.2.2php数学函数库
echo (floor(0.60));
echo (floor(0.40));
echo (floor(5));
echo (floor(5.1));
echo (floor(-5.1));
echo (floor(-5.9));


//rand()函数
echo rand();
echo "<br>";
echo rand();
echo "<br>";
echo rand(10,100);
echo "<br>";

//explode()函数
$pizza = "piece1 piece2 piece3 piece4 piece5 piece6";

$pieces = explode(" ",$pizza);
echo $pieces[0];
echo "<br>";
echo $pieces[1];
echo "<br>";

//md5()函数
echo md5("apple");


//checkdate函数
var_dump(checkdate(12,31,2000));
var_dump(checkdate(2,29,2001));



//mktime()函数
date_default_timezone_set("PRC");
var_dump(checkdate(12,31,2000))."<br>";
var_dump(checkdate(2,31,2000))."<br>";
echo time()."<br>";
echo mktime(0,0,0,12,25,2016)."<br>";
echo "今天距2030年国庆节还有".ceil((mktime(0,0,0,10,1,2030)-time())/(24*60*60))."天<br>";
echo "现在是:".date('Y-m-d H:i:s');
echo "<br>";
echo "--------<br>";


//任务实施
//生成随机字符串
//返回给定长度的随机字符串
function random_text($count, $rm_similar = false) {
    //创建字符数组
    $chars = array_flip(array_merge(range(0,9), range('A','Z')));
    //删除容易引起混淆的相似单词
    if($rm_similar) {
        unset($chars[0], $chars[1], $chars[2], $chars['I'], $chars['O'], $chars['Z']);

    }
    //生成随机字符文本
    for($i = 0, $text = ''; $i < $count; $i++) {
        $text .= array_rand($chars);
    }
    return $text;

}

//函数测试
include 'functions.php';
if( ! isset( $_SESSION)) {
     session_start();


}

//包含生成给定长度字符串的自定义函数functions.php
include'functions.php';
//开启或继续会话，保存图形验证码
if(! isset( $_SESSION)) {
    session_start();
}
//创建65px*20px大小的图像
$width = 65;
$height = 20;
$image = imagecreate( $width, $height);
//为一幅图像分配颜色：imagecolorallocate
$bg_color = imagecolorallocate( $image, 0x33, 0x66, 0xff);
//取得随机字符串
$text = random_text(5);
//定义字体，位置
$font = 5;
$x = imagesx( $image) / 2 - strlen( $text) * imagefontwidth( $font) / 2;
$y = imagesx( $image) / 2 - imagefontheight( $font) / 2;
//输出字符到图形上
$fg_color = imagecolorallocate( $image, 0xff, 0xff, 0xff);
imagestring( $image, $font, $x, $y, $test, $fg_color);
//保存验证码到会话，用于比较验证
$_SESSION['captcha'] = $test;
//输出图像
header('Content-type:image/png'); //定义header,声明图片文件
imagepng( $image);
imagedestroy( $image);
//php GD库实例
header("Conten-type:image/png");                  //设置生成图像格式
$im = imagecreate(120, 30);                        //新建画布
$bg = imagecolorallocate($im, 0, 0, 255);           //背景
$sg = imagecolorallocate($im, 255, 255, 255);       //前景
imagefill($im, 120, 30, $bg);                       //填充背景
imagestring($im, 7, 8, 5, "image create", $sg);      //填充字符串
imagepng($im);                                   //输出图形
imagedestroy($im);                               //销毁资源变量



header("Content-type:image/png");
$im = imagecreate(80, 20);
$bg = imagecolorallocate($im, 255, 255, 0);
$sg = imagecolorallocate($im, 0, 0, 0);
$ag = imagecolorallocate($im, 231, 104, 50);
imagefill($im, 120, 30, $bg);
$str = 'qwertyuipasdfghjklzxcvbnm123456789QWERTYUIPASDFGHJKLZXCVBNM';
$len = strlen($str) - 1;
for ($i = 0; $i < 4; $i++) {
        $strl = $str[mt_rand(0, $len)];           //取随机字符
        imagechar($im, 7, 16 * $i + 7, 2, $strl, $sg);
}
for ($i = 0; $i < 100; $i++) {                             //填充杂色点
        imagesetpixel($im, rand() % 80, rand() % 20, $ag);
}
imagepng($im);
imagedestroy($im);

?>