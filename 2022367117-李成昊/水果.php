<?php
class plants {
    public $name;
    public $color;
    public $age;
    public function __construct( $name, $color, $age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
}
public function getInfo() {
    echo "植物的名称:" . $this->name . "<br>";
    echo "植物的颜色:" . $this->color . "<br>";
    echo "植物的年龄:" . $this->age . "<br>";
}
public function __destruct() {
    echo "再见:" . $this->name."<br>";
  }
}
$rose=new plants('玫瑰','粉色',8);
$rose->getInfo();
$cactus=new plants('仙人掌','绿色',100);
$cactus->getInfo();
$meaty=new plants('多肉','淡绿',18);
$meaty->getInfo();
$tulip=new plants('郁金香','淡粉色',30);
$tulip->getInfo();
?>
