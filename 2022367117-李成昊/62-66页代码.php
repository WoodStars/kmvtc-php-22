<?php

//gettype()

$a="hello";

echo gettype( $a)."<br>"; //输出结果：string

$b=array(1,2,5);

echo gettype( $b)."<br>"; //输出结果：array

//intval

echo intval(4,5)."<br>"; //输出结果：4

//var_dump

var_dump( $a); //输出结果：string（5） "Hello"

echo "<br>";

var_dump( $a,$b)."<br>";
//输出结果：string(5) "Hello" array(3) {[0]=>int(1) [1]=>int(2) [2]=>int(5)}
?>

<?php

echo(floor(0.60));

echo(floor(0.40));

echo(floor(5));

echo(floor(5.1));

echo(floor(-5.1));

echo(floor(-5.9))."<br>";

?>

<?php

echo rand();

echo rand();

echo rand(10,100)."<br>";

?>

<?php

$pizza ="piece1 piece2 piece3 piece4 piece5 piece6";

$pieces = explode(" ", $pizza);

echo $pieces[0];

echo $pieces[1]."<br>";

?>

<?php

echo md5("apple")."<br>";

?>

<?php

var_dump(checkdate(12,31,2000));

var_dump(checkdate(2,29,2001))."<br>";

?>

<?php

date_default_timezone_set("PRC");

var_dump(checkdate(12,31,2000))."<br>";

var_dump(checkdate(2,31,2000))."<br>";

echo time()."<br>";

echo mktime(0,0,0,12,25,2016)."<br>";

echo "今天距2030年国庆节还有".ceil((mktime(0,0,0,10,1,2030)-time())/(24*60*60))."天<br>";

echo "现在是:".date('Y-m-d H:i:s')."<br>";

?>