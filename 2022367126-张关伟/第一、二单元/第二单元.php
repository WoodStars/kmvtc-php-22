<?php
//gettype()
$a="Hello";
echo gettype($a). "<br>"; //输出结果：string
$b=array(1,2,5);
echo gettype($b). "<br>"; //输出结果：array
//intval
echo intval(4.5). "<br>"; //输出结果：4
//var_dump
var_dump($a);  //输出结果：string(5) "Hello"

echo "<br>";
var_dump($a,$b);
//输出结果：string(5) "Hello" array(3) { [0]=> int(1) [1]=> int(2) [2]=> int(5) }


echo "==================================================================","<br>";

//floor()函数
echo (floor(0.60))."<br>";
echo (floor(0.40))."<br>";
echo (floor(5))."<br>";
echo (floor(5.1))."<br>";
echo (floor(-5.1))."<br>";
echo (floor(-5.9))."<br>";


echo "==================================================================","<br>";
//rand()函数
echo rand()."<br>";
echo rand()."<br>";
echo rand(10,100)."<br>";


echo "==================================================================","<br>";
//explode()函数
$pizza = "piece1 piece2 piece3 piece4 piece5 piece6 ";
$pieces = explode (" ",$pizza);
echo $pieces[0]."<br>";
echo $pieces[1]."<br>";


echo "==================================================================","<br>";
//md5（）函数
echo md5("apple");
echo "<br>";

echo "==================================================================","<br>";
//checkdate（）函数
var_dump(checkdate(12,31,2000));
echo "<br>";
var_dump(checkdate(2,29,2001));
echo "<br>";


echo "==================================================================","<br>";
//mktime（）函数
date_default_timezone_set("PRC");
var_dump(checkdate(12,31,2000));
var_dump(checkdate(2,31,2000));

echo time()."<br>";
echo mktime(0,0,0,12,25,2016)."<br>";
echo "今天距2030年国庆还有".ceil((mktime(0,0,0,10,1,2030) -time())/(24*60*60))."天<br>";
echo "现在是：".date('Y-m-d H:i:s');

echo "==================================================================","<br>";

//生成随机字符串
function random_text( $count, $rm_similar = false) { 

   $chars = array_flip( array_merge( range(0,9),range('A','Z')));
   if ( $rm_similar ) {
       unset( $chars[0],$chars[1],$chars[2],$chars['I'],$chars['O'],$chars['Z']);
   }
   for ( $i = 0, $text =''; $i < $count; $i++ ) {
       $text =array_rand( $chars);
   }
   return $text;
}

echo "==================================================================","<br>";

include'functions.php';
echo random_text(5,true);

echo "==================================================================","<br>";

//生成随机图形验证码
include'functions.php';
if ( ! isset( $_SESSION) ) {
    session_start();
}
$width = 65;
$height = 20;
$image = imagecreate( $width, $height );

$bg_color = imagecolorallocate( $image,0x33,0x66,0xff);

$text = random_text(5);

$font = 5;
$x = imagesx( $image ) / 2 - strlen( $text ) * imagefontwidth( $font ) /2;
$y = imagesy( $image ) / 2- imagefontheight( $font ) /2;

$fg_color = imagecolorallocate( $image,0xff,0xff,0xff);
imagestring($image,$font,$x,$y,$text,$fg_color);
$_SESSION['captcha'] = $text;
header('Content-type:image/png');
imagepng( $image );
imagedestroy( $image );


echo "==================================================================","<br>";
//PHP GD库实例
header("Cont-type:image/png");
$im = imagecreate(120,30);
$bg = imagecolorallocate( $im,0,0,255);
$sg = imagecolorallocate( $im,255,255,255);
imagefill( $im,120,30, $sg );
imagestring($im,7,8,5,"image create",$sg);
imagepng( $im );
imagedestroy( $im );

echo "==================================================================","<br>";
//B.image_create2.php
header("Content-type:image/png");
$im = imagecreate(80,20);
$bg = imagecolorallocate( $im,255,255,0);
$sg = imagecolorallocate( $im,0,0,0);
$ag = imagecolorallocate( $im,231,104,50);
imagefill( $im,120,30, $bg );
$str = 'qwertyuiopasdfghjklzxcvbnm123456789QWERTYUIOPASDFGHJKLZXCVBNM';
$len = strlen( $str )-1;
for( $i = 0; $i < $len; $i++ ) { 
     $str1 =  $str[mt_rand (0,$len)] ;
     imagechar( $im, 7,16 * $i + 7,2, $str,$sg  );
    }
for( $i = 0; $i < 100; $i++ ) {
     imagesetpixel( $im, rand()%80,rand()%20,$ag );
     }
imagepng( $im );
imagedestroy( $im );

echo "==================================================================","<br>";
//（2）PHP JpGraph实例
require_once('jpgraph/jpgraph.php');
require_once('jpgraph/jpgraph_pie.php');
require_once('jpgraph/jpgraph_pie3d.php');
//示例数据
$data = array(40,60,21,33);
//创建图像
$graph = new PieGraph(350,250);
$theme_class = new VividTheme;
$graph->SetTheme( $theme_class );
$title = "一个简单的3D饼图";
//编码转换
$title = iconv("UTF-8","GB2312//IGNORE", $title );
//设置字体，标题
$graph->title->SetTheme(FF_SIMSUN,FS_BOLD);
$graph->title->Set( $title );
//输出3D饼图
$p1 = new PiePlot3D($data);
$graph->Add($p1);
$p1->ShowBorader();
$p1->SetColor("black");
$p1->ExplodeSlice(1);
$graph->Stroke();




















?>