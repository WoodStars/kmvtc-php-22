<?


$a = false;
$username = "Mikes";

echo "the username is  ", $username, "<br>";


echo "=====================================================";


$a = 10;
echo "a的值是$a<br>";
echo "a的值是\$a<br>";

if ($username == "Mike") {
    echo "Hi, Mike <br>";
    
} else {
    echo "you are not Mike<br>";

}


if ($a == true) {
    echo "a is true";
} else {
    echo "a is false";
}

echo "<br>";

echo "=====================================================";

$n1 = 123;
echo "n1=", $n1, "<br>";
$n2 = 0;
echo "n2=", $n2, "<br>";
$n3 = -123;
echo "n3=", $n3, "<br>";
$n4 = 0123;
echo "n4=", $n4, "<br>";
$n5 = 0xFF;
echo "n5=", $n5, "<br>";
$n6 = 0b11111111;
echo "n6=", $n6, "<br>";

$pi = 3.1415926;
echo "pi=", $pi, "<br>";
$width = 3.3e4;
echo "width=", $width, "<br>";
$var = 3e-2;
echo "var=", $var, "<br>";

echo 3 ** 4;

$arr1 = array(1, 2, 3, 4, 5, 6, 7, 8);
$arr2 = array("animals" => "dog", "color" => "red");

echo "<br>", $arr1[0];
echo "<br>", $arr1[7];
echo "<br>", $arr2["color"];

echo "=====================================================";
class Animal
{
    
    var $name;
    var $age;
    var $weight;
    var $sex;
    function __construct($name)
    { 
        $this->name = $name; 
    }
   
    public function run()
    {
        echo "haha,", $this->name, " is running<br>";
    }
    public function eat()
    {
        echo "haha,i am eating";
    }
}
$dog = new Animal("dog");
$cat = new Animal("cat");
$dog->run();
$cat->run();



echo "=====================================================";
// 定义一个和平精英雄
class PUBG
{
    //血量
    public $hp =200;
    //姓名
    public $name;
    //攻击力
    public $attack;

    public $shield = 1;

    function __construct($name, $attack)
    {
        $this->name = $name;
        $this->attack = $attack;
    }

    // 攻击
    function attack($PUBG)
    {
        echo $this->name, "攻击了", $PUBG->name,"<br>";
        $PUBG->hp -= $this->attack;

        $PUBG->hp = $PUBG->hp - $this->attack*(1-$PUBG->shield);
        echo $PUBG->name,"的剩余血量：", $PUBG->hp,"<br>";
    }
}

$PUBG1 = new PUBG("是你腻害",10);
$PUBG2 = new PUBG("莹",12);

$PUBG1->attack($PUBG2);
$PUBG2->attack($PUBG1);
$PUBG2->attack($PUBG1);
$PUBG2->attack($PUBG1);
$PUBG2->attack($PUBG1);
?>