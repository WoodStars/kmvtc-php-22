<?php
class MyClass{
    //定义一个常量
    const constant = "我是一个常量";
    function showConstant() {
        echo self::constant."<br>";    //类中访问常量
    }
}
echo MyClass::class."<br>";          //使用类名来访问常量
$clss=new MyClass();
$clss->showConstant();

?>