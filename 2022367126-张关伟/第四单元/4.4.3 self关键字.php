<?php
class animal{
    private static $firstCry=0;     //私有的静态变量
    private $lastCry;
    //构造方法
    function __construct(){
        $this->lastCry = ++self::$firstCry;
    }
    function printtLast(){
        var_dump($this->lastCry);
    }
}
$bird = new Animal();              //实例化对象
$bird->printtLast();               //输出：int(1)

?>