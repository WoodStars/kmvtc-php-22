<?php
//类的重载
class Electronics {
    public $name;          //名称
    public $model;         //型号
    public $brand;         //品牌
    public $Released;      //发布时间
    public function __construct($name, $model, $brand, $Released) {
        $this->name = $name;  
        $this->model = $model;
        $this->brand = $brand;
        $this->Released = $Released;
    }
    public function getinfo() {
        echo "电子产品类型". $this->type."<br>";
        echo "电子产品名称". $this->name ."<br>";
        echo "电子产品型哈". $this->model ."<br>";
        echo "电子产品品牌". $this->brand ."<br>";
    }
}
/**
 * 定义一个computer类，使用extends关键字来继承Electronics类，作为Electronics类的子类
 */
class computer extends Electronics {
    public $development;
    public function getinfo () {
        parent::info();
        echo "电脑可以用来". $this->development ."开发程序<br>";
        $this->automated_operation();
    }
    public function automated_operation () {
        //电脑可以自动化运行
        echo"我可以实行自动化操作";
    }
    public function __construct($name, $model, $brand, $Released,$development){
        parent::__construct($name, $model, $brand, $Released);
        $this->development = $development;
    }
}
$crow = new computer("笔记电脑","华为Matebook","D16","huawei");
$crow->getinfo();






?>