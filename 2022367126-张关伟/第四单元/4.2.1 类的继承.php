<?php
//类的继承
class plant {
    public $name;
    public $color;
    public $floresccence;
    public $flower_language;

    public function __construct($name, $color,$floresccence,$flower_language) {
        $this->name = $name;        
        $this->color = $color;
        $this->floresccence = $floresccence;
        $this->flower_language = $flower_language;   
    }
    public function getinfo () {
        echo "花名：". $this->name ."<br>" ; 
        echo "花的颜色：". $this->color ."<br>";
        echo "花期：". $this->floresccence ."<br>";
        echo "花语：". $this->flower_language ."<br>";
    }
}
/**
 * 定义一个flower类，使用extends关键字来继承plant类，作为flower类的子类
 */
class flower extends plant {
    public $petal;         //flower类的自有属性$petal（花瓣）
    public function beautiful()  {   //flower自有特点
        echo "I am beautiful!!!";
    }     
    
}
$crow = new flower("玫瑰","红色","15天","热情、热爱着您、热恋");   //定义属性，并实例化
$crow->getInfo();                                               //内部调用方法
$crow->beautiful();

$crow = new flower("满天星","蓝色","10天","象征青春活力与美好友情");
$crow->getInfo();
$crow->beautiful();


?>