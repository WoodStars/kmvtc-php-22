# 创建数据库 library（图书馆）
create database library;

# 使用数据库 library
use library;

# // 创建一张表 图书主数据（） 包含 书名、书号、作者、出版社、出版时间、价格、ISBN、页数、分类、库存、简介
# // 设置主键为 书号
create table book(
                     book_name varchar(50) not null,
                     book_id int primary key auto_increment,
                     author varchar(50) not null,
                     publisher varchar(50) not null,
                     publish_time date not null,
                     price decimal(10,2) not null,
                     ISBN varchar(50) not null,
                     page int not null,
                     category varchar(50) not null,
                     stock int not null,
                     introduction text
);

insert into book (book_name, author, publisher, publish_time, price, ISBN, page, category, stock, introduction)
values (
           'Java编程思想', 'Bruce Eckel', '机械工业出版社', '2007-01-01', 108.00, '9787111213826', 700, '计算机', 100, '本书是一本全面介绍Java编程的畅销书，全书共分为24章，涵盖了Java编程的方方面面。'
       );
# // 创建一张表 图书借阅记录（） 包含 书号、借阅人、借阅时间、归还时间、借阅状态
# // 设置主键为 自增主键
CREATE TABLE BookBorrowRecords (
    id INT AUTO_INCREMENT PRIMARY KEY,
    book_id VARCHAR(50) NOT NULL,
    borrower VARCHAR(100) NOT NULL,
    borrow_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    return_time TIMESTAMP NULL,
    borrow_status ENUM('Borrowed', 'Returned') NOT NULL DEFAULT 'Borrowed'
);


# // 创建一张表 用户信息（） 包含 用户名、密码、邮箱、电话、地址、注册时间、用户类型
# // 设置主键为 用户名

CREATE TABLE UserInfo (
    username VARCHAR(50) NOT NULL PRIMARY KEY,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    phone VARCHAR(20) NOT NULL UNIQUE,
    address VARCHAR(255) NOT NULL,
    register_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_type ENUM('admin', 'user', 'guest') NOT NULL DEFAULT 'user'
);