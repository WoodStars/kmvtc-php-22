<?php
class  plant {
    public $name;
    public $color;
    public $season;

    public function __construct( $name,$color,$season ){
    $this->name=$name;
    $this->season=$season;
}
    public function getInfo(){
    echo"植物名称:" . $this->name. "<br>";
    echo"植物颜色:" . $this->color. "<br>";
    echo"植物季节:" . $this->season. "<br>";
}
}
    $flower=new plant("花","红色","一年四季");
    //通过new关键字实例化出一个对象,名称为flower
    $flower->getInfo();
    $grass = new plant ("草","绿色","一年四季");
    $grass->getInfo();
    $tree = new plant("树","绿色","一年四季");
    $tree->getInfo();
?>