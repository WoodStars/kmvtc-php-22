<?php
header("Content-Type:text/html;charset=utf-8");
//使用abstract关键字声明一个抽象类
abstract class animal {
    abstract public function shout();
}
    //抽象方法
    abstract function shout();
class cat extends animal{
    {
        echo "喵喵喵"
    }
}


//实例化dog类
$dog = new dog();
//调用dog类的shout方法
$dog->shout();
echo "\n";
//实例化cat类
$cat = new cat();
//调用cat类的shout方法

class cat extends animal{
    //实现抽象方法
    public function shout() {
        echo "喵喵......<br>";
    }
}
$dog=new dog();
$dog->run();
$dog->shout();
$cat=new cat();
$cat->shout();
$cat->run();
?>

<?php
header("Content-Type:text/html;charset=utf-8");
interface animal{
    function run();
    function shout();
}
//定义landanimal接口
interface landanimal{
    public function liveonland();
}
//定义dog类，实现animal接口和landanimal接口
class dog implements animal, landanimal{
    //实现接口中的抽象方法
    public function run() {
        echo "小狗在奔跑<br>";
    } 
    public function shout() {
        echo "汪汪......<br>";
    }
    public function liveonland() {
        echo "小狗在陆地上生活<br>";
    }
}
$dog=new dog();
$dog->run();
$dog->shout();
$dog->liveonland();
?>

<?php
header("Content-Type:text/html;charset=utf-8");
abstract class animal{
    public abstract function shout();
}
//定义dog类，实现抽象中的方法
class dog extends animal{
    public function shout() {
        echo "汪汪汪......<br>";
    }
}
function animalshout( $obj) {
    if( $obj instanceof animal){
        $obj->shou();
    } else{
        echo "Error:对象错误！";
    }
}
$cat=new cat();
$dog=new dog();
animalshout( $cat);
animalshout( $dog);
?>

