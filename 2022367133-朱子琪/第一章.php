//3月19日
<!DOCTYPE html>
<html>
<head>
    <title>PHP实现简单计算器</title>
    <meta charset="UTF-8">
</head>
<body>
    <table align="center" border="1" width="500">
        <caption><h1>caption我的计算器</h1></caption>
        <form action="">
            <tr>
                <td>
                    <input type="text" size="5" name="num1" value="<?php echo $_GET["num1"] ?? ''; ?>">
                </td>
                <td>
                    <select name="ysf">
                        <option value="+" <?php echo ($_GET["ysf"] ?? '') === "+" ? "selected" : ""; ?>>+</option>
                        <option value="-" <?php echo ($_GET["ysf"] ?? '') === "-" ? "selected" : ""; ?>>-</option>
                        <option value="x" <?php echo ($_GET["ysf"] ?? '') === "x" ? "selected" : ""; ?>>x</option>
                        <option value="/" <?php echo ($_GET["ysf"] ?? '') === "/" ? "selected" : ""; ?>>/</option>
                        <option value="%" <?php echo ($_GET["ysf"] ?? '') === "%" ? "selected" : ""; ?>>%</option>
                    </select>
                </td>
                <td>
                    <input type="text" size="5" name="num2" value="<?php echo $_GET["num2"] ?? ''; ?>">
                </td>
                <td><input type="submit" name="sub" value="计算"></td>
            </tr>
            <?php if (isset($_GET["sub"])): ?>
            <tr><td colspan="4">
            <?php
                $num1 = $_GET["num1"] ?? 0;
                // echo "用户输入的第1个值是",$num1;
                $num2 = $_GET["num2"] ?? 0;
                // echo "用户输入的第2个值是",$num2;
                $message = "";
                if (is_numeric($_GET["num1"]) && is_numeric($_GET["num2"])) {
                    $num1 = (float)$num1;
                    $num2 = (float)$num2;

                    $result = 0;
                    switch ($_GET["ysf"]) {
                        case '+': $result = $num1 + $num2; break;
                        case '-': $result = $num1 - $num2; break;
                        case 'x': $result = $num1 * $num2; break;
                        case '/': 
                            if ($num2 != 0) {
                                $result = $num1 / $num2;
                            } else {
                                $message = "除数不能为0。";
                                echo $message;
                            }
                            break;
                        case '%': 
                            if ($num2 != 0) {
                                $result = $num1 % $num2;
                            } else {
                                $message = "除数不能为0。";
                                echo $message;
                            }
                            break;
                    }
                    echo  "结果是：" . $result;
                } else {
                    $message = "请输入有效的数字。";
                    echo $message;
                }
            ?>
            </td></tr>
            <?php endif; ?>
        </form>
    </table>
</body>
</html>


//3月21日
<!DOCTYPE html>
<html>

<head>
    <title>鸡兔同笼</title>
    <meta charset="UTF-8">
</head>

<body>
    <table align="center" border="1" width="500">
        <caption>
            <h1>计算器</h1>
        </caption>
        <form action="">
            <tr>
                <td>
                    <div>头</div>
                </td>

                <td>
                    <div>脚</div>
                </td>
                <td>
                    <div>按钮</div>
                </td>
            </tr>
            <tr>
                <td>
                    <input placeholder="头数" type="text" size="5" name="num1" value="<?php echo $_GET["num1"] ?? ''; ?>">
                </td>
                <td>
                    <input type="text" size="5" name="num2" value="<?php echo $_GET["num2"] ?? ''; ?>">
                </td>
                <td><input type="submit" name="sub" value="计算"></td>
            </tr>
            <?php if (isset ($_GET["sub"])): ?>
                <tr>
                    <td colspan="4">
                        <?php
                        $num1 = $_GET["num1"] ?? null;
                        // echo "已知头共有", $num1, "只,";
                        $num2 = $_GET["num2"] ?? null;
                        // echo "已知脚共有", $num2, "只,";
                    
                        if (is_numeric($num1) && is_numeric($num2)) {
                            // $num1 = (float) $num1;
                            // $num2 =  $num2;
                            $rabbit_num = ($num2 - 2 * $num1) / 2;

                            $chick_num = $num1 - $rabbit_num;

                            if ($chick_num < 0 || $rabbit_num < 0) {
                                echo "此题无解";

                            }
                            //todo 判断计算出来的值是否为整数，如若不是，则依旧无解
                            else if (!is_int($chick_num) && !is_int($rabbit_num)) {
                                echo $chick_num,"--",$rabbit_num,"不是整数";

                            } else {
                                echo "答案：鸡有" . $chick_num, "，兔有" . $rabbit_num;
                            }

                        } else {
                            echo $message;
                        }
                        ?>
                    </td>
                </tr>
            <?php endif; ?>
        </form>
    </table>
</body>

</html>

