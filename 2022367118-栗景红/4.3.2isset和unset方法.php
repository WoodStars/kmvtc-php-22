<?php
class animal {
    private $name; //私有成员属性
    private $color;
    private $age;
    //__ get()方法用来获取私有属性
    function__ get( $property_name){
        if(isset( $this ->$property_name)){
            return ($this ->$property_name);
        } else{
            return(NULL);
        }
    }
    //__ set()方法用来设置私有属性
    function __ set( $property_name, $value){
        $this ->$property_name = $value;
    }
    //__ isset()方法
    function __ isset( $property_name){
        return isset( $this ->$property_name);
    }
    //__ unset()方法
    function __ unset( $property_name){
        unset( $this ->$property_name);
    }
}
$pig = new animal();
$pig ->name = "大猪";
echo var_dump(isset( $pig ->name)) . "<br>";
//调用__ isset()方法，输出：bool（true)
echo $pig ->name . "<br>";  //输出：大猪
unset( $pig ->name); //调用__ unset()方法
echo $pig ->name;//无输出
?>