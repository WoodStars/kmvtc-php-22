<?php
header("Content-Type:text/html;charset=utf-8");
class animal {
    private $name; //私有的成员属性
    private $color;
    private $age;
    //_get()方法用来获取私有属性的值
    public function __ get( $property_name){
        if(insset( $this ->$property_name)){
            return $this ->$property_name;
        } else{
            return(NULL);
        }
    }
    //_set()方法用来设置私有属性的值
    public function __ set( $property_name, $value){
        $this ->$property_name = $value;
    }
}
$tiger = new animal();
$tiger ->name = "老虎"; //自动调用__ set()方法
$tiger ->color = "花白色";
$tiger ->age = 10;
echo $tiger ->name."<br>"; //自动调用__ get()方法
echo $tiger ->color."<br>";
echo $tiger ->age."<br>";
?>