<?php
/* *
* 定义类 MyClass
*/
class MyClass {
    public $public='Public';  //定义公共属性 $public
    protected $protected='Protected';  //定义保护属性 $protected
    private $private='Private';        //定义私有属性 $private
    function printHello() { //输出3个成员属性
        echo $this->public;
        echo $this->protected;
        echo $this->private;
    }
}
$obj=new MyClass(); //实例化当前类
echo $obj->public; //输出 $public
//echo $obj->protected; //Fatal error: Cannot access protected propery MyClass::$protected
//echo $obj->private; //Fatal error: Cannot access private propery MyClass::$private
$obj->printHello();
/ * *
* 定义类 MyClass2
*/

echo $myClass->public;
echo $myClass->private;
echo $myClass->protected;
class MyClass2 extend MyClass {
    protected $protected = 'Protected2';
    function printHello()  {
        echo $this->$public;
        echo $this->$private;    
        echo $this->$protected;
    }
}
$obj2 = new MyClass2();
echo $obj2->public;
echo $myClass2->private;
//echo $myClass2->protected;\
$obj2->printHello();
?>





