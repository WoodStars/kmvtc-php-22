<?php
class fruit {
    public $name ;
    public $color ;
    public $taste ;
    public function __construct( $name, $color, $taste) {
        $this->name=$name;
        $this->color=$color;
        $this->taste=$taste;
    }
    public function getInfo() {
        echo "水果的名称:" . $this->name . "<br>";
        echo "水果的颜色:" . $this->color . "<br>";
        echo "水果的味道:" . $this->taste . "<br>";
    }
    /* *
    * 析构方法，在对象销毁时自动调用
    */
    public function __destruct() {
        echo "再见:" . $this->name. "<br>";
    }
}
$mango=new fruit('芒果','黄色','甜');
$mango->getInfo();
$apple=new fruit('苹果','红色','甜');
$apple->getInfo();
?>