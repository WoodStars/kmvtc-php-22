<?php
class_animal {
    public $name;
    public $color;
    public $age;
    public function_construct( $name, $color, $age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    public function getInfo() {
        echo "动物的名称:" . $this->name . "<br>";
        echo "动物的颜色:" . $this->color . "<br>";
        echo "动物的年龄:" . $this->age . "<br>";
    }
}
//定义一个cats类，使用extends关键字来继承animal类，作为animal类的子类
class cats extends animal{
    public $claw; //cats类自有的属性 $claw
    public function getInfo() {
        parent::getInfo();
        echo "猫科动物有" . $this->claw .'爪子<br>';
        $this->run();
    }
    public function run() {
        //猫科动物自有办法
        echo "我会飞奔!!!";
    }
    public function_construct( $name, $color, $age, $cats) {
        parent::_construct( $name, $color, $age);
        $this->claw= $claw;
    }
}
$crow=new cats("狮子","棕色",4,"雄壮的");
$crow->getInfo();
?>