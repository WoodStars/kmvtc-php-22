<?php
class animal {
    //成员属性
    public $name;
    public $color;
    public $age;
    public function __construct( $name, $color, $age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    public function getinfo() {
        echo "动物的名称:" . $this->name . "<br>";
        echo "动物的颜色:" . $this->color . "<br>";
        echo "动物的年龄:" . $this->age . "<br>";
    }
}
/* *
*定义一个bird类，使用extends关键字来继承animal类，作为animal类的子类
*/
class bird extends animal {
    public $wing;//bird类的自有属性 $wing
    public function fly() {//bird类自有的方法
        echo'I can fly!!! ';
        //输出
    }
}
$crow=new bird("鹦鹉","彩色",3);//定义鹦鹉的属性
$crow->getInfo();//从内部调用
$crow->fly();//调用鸟类的方法
?>