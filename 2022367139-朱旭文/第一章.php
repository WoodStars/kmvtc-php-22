Apache主目录【C:/Apache24】

### tree命令

输出当前路径下的目录树

> C:.
> ├─bin
> ├─cgi-bin
> ├─conf
> │  ├─extra
> │  └─original
> │      └─extra
> ├─error
> │  └─include
> ├─htdocs
> ├─icons
> │  └─small
> ├─include
> ├─lib
> ├─logs
> ├─manual
> └─modules

修改配置文件：

位置在： ./conf/httpd.conf

1. 定义Apache的源路径

> Define SRVROOT "c:/Apache24"

2. 设置服务的启动端口

> Listen 6603
>
> ServerName localhost:6603

3. 设置网页文档路径

> DocumentRoot "${SRVROOT}/htdocs"
>
> c:/Apache24/htdocs

4. 配置默认的索引规则，优先显示index.php，若不存在，则向后检索文件，直至最后一个

> <IfModule dir_module>
>
>   DirectoryIndex index.php  index.html
>
> </IfModule>

5. 加载php模块（支持apache服务器解析php）

> LoadModule php_module "C:/php8/php8apache2_4.dll"
>
> AddType application/x-httpd-php .php .html
>
> PHPIniDir "C:/php8"

6. 新增类型

> AddType application/x-httpd-php .php .html

7. php初始化路径

> PHPIniDir "C:/php8"

php8主目录【C:/php8】

> ├─dev
> ├─ext
> ├─extras
> │  └─ssl
> └─lib
>  └─enchant



## 启动Apache服务器

C:\Apache24\bin\ApacheMonitor.exe



![](C:\Users\Think\Desktop\360截图20240319202522890.jpg)

保证到这一步Apache2_4左侧是绿色标记，即为服务启动成功。



在浏览器中输入地址 **localhost:4949**，显示的是目录**DocumentRoot "${SRVROOT}/htdocs"**下的index.html文件

![](C:\Users\Think\Desktop\360截图20240319202620808.jpg)

修改Apache配置文件，并重启服务器，加载 index.php文件。

> <IfModule dir_module>
>
> DirectoryIndex  index.php index.html
>
> </IfModule>

![](C:\Users\Think\Pictures\Screenshots\屏幕截图(1).png)