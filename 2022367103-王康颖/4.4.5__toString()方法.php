<?php
class TestClass{
    public $foo;
    public function __construct( $foo ) {
        $this->foo= $foo;
    }
    //定义一个__toString()方法，返回一个成员属性 $foo
    public function __toString(){
        return $this->foo;
    }
}
$class=new TestClass('Hello');
//直接输出对象
echo $class;    //输出结果为'Hello'
?>