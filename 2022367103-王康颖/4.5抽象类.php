<?php


//使用abstract关键字声明一个抽象类
abstract class superhero3 {
    //在抽象类中声明抽象方法
    abstract public function shout();
}

//定义superman类继承superhero类
class superman extends superhero3 {
    //实现抽象方法
    public function shout() {
        echo "豪油根<br>";
    }
}

//定义spiderman类继承superhero类
class spiderman extends superhero3 {
    //实现抽象方法
    public function shout() {
        echo "欧拉欧拉<br>";
    }
}
$superman=new spiderman();
$superman->shout();
$spiderman=new spiderman();
$spiderman->shout();

?>