<?php



//  定义和调用函数
function custom_func($num)
{
        return " $num * $num=" . $num * $num . "<br>";
}
echo custom_func("10");         //输出结果：10 * 10+100
echo custom_func("12.5");       //输出结果：12.5 * 12.5=156.25





//默认函数
function fun_sum($a, $b = 20)
{
        $a = $a + 5;
        return $a + $b;
}
$x = 10;
echo fun_sum($x);    //输出结果：35
echo $x;               //输出结果：10
"<br>";

// 函数的返回值
function my_fun($a = 1)
{
        echo $a;
        return;         //结束函数的运行，下面的程序将不会被执行

}
my_fun();       //输出结果：1

//
"<br>";

function squre($num)
{
        return $num * $num;     //返回一个数的平方
}
echo squre(4);          //输出16


// 变量函数
header("Content-Type:text/html;charset=utf-8");
function come()  //定义come（）函数
{
        echo "come函数: i am coming!<br>";
}

function go($name = "jack")    //定义go（）函数
{
        echo "go函数: $name = 走了";
}

function back($string)    //定义back（）函数
{
        echo $string . "又回来了<br>";
}
$func = 'come';  //声明一个变量，将变量赋值为“come”
$func();         //使用变量函数来调用函数come()，输出我“我来了”
$func = 'go';    //重新为变量赋值
$func("Tom");    //使用变量函数来调用函数go()，输出“TOM走了”
$func("back");   //重新为变量赋值
$func("Lily");   //使用变量函数来调用函数back()，输出“Lily又回来了”

// 函数的引用

function &test()
{
        static $b = 0;   //声明一个静态变量
        $b = $b + 1;
        echo $b . "<br>";
        return $b;
}
$a = test(); //输出结果1
$a = 5;
$a = test(); //输出结果2
$a = &test(); //输出结果3
$a = 5;
$a = test();  //输出结果6


// PHP变量函数库

//gettype()
$a = "hello";
echo gettype($a) . "<br>";    //输出结果：string
$b = array(1, 2, 5);
echo gettype($b) . "<br>";    //输出结果：array
// intval
echo intval(4, 5) . "<br>";   //输出结果：4
//var_dump
var_dump($a);                 //输出结果：string(5) "Hello"
echo "<br>";
var_dump($a, $b);             //输出结果：string(5) "Hello" array(3) { [0] => int(1) [1] => int(2) [2] => int(5) }


// floor（）函数
echo (floor(0.60));    //输出结果：0
echo "<br>";
echo (floor(0.40));    //输出结果：0
echo "<br>";
echo (floor(5));       //输出结果：5
echo "<br>";
echo (floor(5.1));     //输出结果：5
echo "<br>";
echo (floor(-5.1));    //输出结果：-6
echo "<br>";
echo (floor(-5.9));    //输出结果：-6
echo "<br>";


//rand()函数
echo rand();
echo "<br>";
echo rand();
echo "<br>";
echo rand(10, 100);
echo "<br>";

//explode()函数
$pizza = "piece1 piece2 piece3 piece4 piece5 piece6";
$pieces = explode(" ", $pizza);
echo $pieces[0];   //输出 piece1
echo "<br>";
echo $pieces[1];   //输出 piece2
echo "<br>";


//md5()函数
echo md5("apple");    //输出结果：1f3870be274f6c49b3e31a0c6728957f


// checkdate()函数
var_dump(checkdate(12, 31, 2000));    // bool(true)
var_dump(checkdate(2, 29, 2001));     // bool(false)
echo "<br>";


// mktime()函数
//设置时区
date_default_timezone_set("PRC");
//checkdate()
var_dump(checkdate(12, 31, 2000)) . "<br>";    //输出:bool(ture)
var_dump(checkdate(2, 31, 2000)) . "<br>";     //输出:bool(false)
//mktime()
echo time() . "<br>";                          //返回当前时间戳
echo mktime(0, 0, 0, 12, 25, 2016) . "<br>";
//今天距2030年国庆还有多少天
echo "今天距2030年国庆还有" . ceil((mktime(0, 0, 0, 10, 1, 2030) - time()) / (24 * 60 * 60)) . "天<br>";
//date()返回格式化的当地时间    
echo "现在是:" . date('Y-m-d H:i:s');
echo "<br>";
echo "----------<br>";



//生成随机字符串
//返回给定长度的随机字符串
function random_text($count, $rm_similar = false)
{
        //创建字符数组
        $chars = array_flip(array_merge(range(0, 9), range('A', 'Z')));
        //删除容易引起混淆的相似单词
        if ($rm_similar) {
                unset($chars[0], $chars[1], $chars[2], $chars['I'], $chars['O'], $chars['Z']);
        }
        //生成随机字符文本
        for ($i = 0, $text = ''; $i < $count; $i++) {
                $text .= array_rand($chars);
        }
        return $text;

}


echo "<br>";
echo "----------<br>";

//包含生成给定长度字符串的自定义函数functions.php
include 'functions.php';
//开启或继续会话，保存图形验证码到会话中供其他页面调用
if (!isset($_SESSION)) {
        session_start();
}
//创建65px*20px 大小的图像
$width = 65;
$height = 20;
$image = imagecreate($width, $height);
//为一幅图像分配颜色：imagecolorallocate
$bg_color = imagecolorallocate($image, 0x33, 0x66, 0xff);
//取得随机字串符
$_text = random_text(5);
//定义字体，位置
$font = 5;
$x = imagesx($image) / 2 - strlen($text) * imagefontwidth($font) / 2;
$y = imagesy($image) / 2 - imagefontheight($font) / 2;
//输出字符到图形上
$fg_color = imagecolorallocate($image, 0xff, 0xff, 0xff);
imagestring($imgae, $font, $x, $y, $text, $fg_color);
//保存验证码到会话中，用于比较验证
$_SESSION['captcha'] = $text;
//输出图像
header('Content-type:image/png'); //定义header,声明图片文件
imagepng($image);
imagedestroy($image);



header("Conten-type:image/png");                  //设置生成图像格式
$im = imagecreate(120, 30);                        //新建画布
$bg = imagecolorallocate($im, 0, 0, 255);           //背景
$sg = imagecolorallocate($im, 255, 255, 255);       //前景
imagefill($im, 120, 30, $bg);                       //填充背景
imagestring($im, 7, 8, 5, "image create", $sg);      //填充字符串
imagepng($im);                                   //输出图形
imagedestroy($im);                               //销毁资源变量



header("Content-type:image/png");
$im = imagecreate(80, 20);
$bg = imagecolorallocate($im, 255, 255, 0);
$sg = imagecolorallocate($im, 0, 0, 0);
$ag = imagecolorallocate($im, 231, 104, 50);
imagefill($im, 120, 30, $bg);
$str = 'qwertyuipasdfghjklzxcvbnm123456789QWERTYUIPASDFGHJKLZXCVBNM';
$len = strlen($str) - 1;
for ($i = 0; $i < 4; $i++) {
        $strl = $str[mt_rand(0, $len)];           //取随机字符
        imagechar($im, 7, 16 * $i + 7, 2, $strl, $sg);
}
for ($i = 0; $i < 100; $i++) {                             //填充杂色点
        imagesetpixel($im, rand() % 80, rand() % 20, $ag);
}
imagepng($im);
imagedestroy($im);

?>