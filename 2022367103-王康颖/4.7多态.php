<?php

abstract class superhero2{
    public abstract function shout();
}

//定义dog类，实现抽象类中的方法
class superman extends superhero2{
    public function shout(){
    echo"豪油根<br>";
    }
}

class spiderman extends superhero2{
    public function shout(){
        echo "欧拉欧拉<br>";
    }
}
function superheroshout($obj){
    if( $obj instanceof superhero2){
        $obj->shout();
    } else{
        echo "Error:对象错误!";
    }
}
$spiderman = new spiderman();
$superman = new superman();
superheroshout($spiderman);
superheroshout($superman);
superheroshout($crow);
?>