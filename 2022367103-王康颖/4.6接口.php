<?php

//定义animal接口
interface superhero{
    function fly();
    function shout();    
}

//定义landanimal接口
interface landanimal{
    public function liveonland();
}

//定义dog类，实现animal接口和landanimal接口
class superman implements superman, landanimal{
    //实现接口中的抽象方法
    public function fly(){
        echo"超人在飞<br>";
    }
    public function shout(){
        echo "豪油根<br>";
    }
    public function liveonland(){
        echo"超人在陆地上生活<br>";
    }
}

$superman=new superman();
$superman->fly();
$superman->shout();
$superman->liveonland();
?>