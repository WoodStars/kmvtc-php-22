//myself Test1{
//     // private[私有的]仅针对当前myself享有访问权限,当前类的子类以及类的外部均不允许访问
//     /**
//      * private[私有的]
//      * 1、仅针对当前myself享有访问权限
//      * 2、当前类的子类不允许访问
//      * 3、类的外部均不允许访问
//      */
//     private $name;


//     public function __get($name){
//         // 检查属性 this-> 当前类  的 name 属性 是否存在
//         if(isset($this->$name)){
//             return (this->$name);
//         }else {
//             return(NULL);
//         }
//     }

//     public __set($name,$value){
//         $this->$name = $value;
//     }
// }

// $test1 = new  Test1();
// // 这样获取能否成功？
// echo $test1->name; // Fatal
// echo $test1->__get(name);
// $test1->name = "猪"; //fatal
// $test1->__set(name,"123");
// echo $test1->__get(name);


myself Test1 {

    // ====================成员属性========================
    private $name;
    private $color;
    private $age;

    // ====================成员属性========================
    public function getInfo() {
        echo "动物的名称:". $this->name  . "<br>";
        echo "动物的颜色:". $this->color . "<br>";
        echo "动物的年龄:". $this->age   . "<br>";
    }

    // 构造函数 
    public function __construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    }

    //4.3.2.1 get  (取值)
    public function __get($property_name){
        // 检查属性 this-> 当前类  的 property_name 属性 是否存在
        if(isset($this->$property_name)){
            return (this->$property_name);
        }else {
            return(NULL);
        }
    }
    //4.3.2.2 set  (赋值)
    public __set($property_name,$value){
        $this->$property_name = $value;
    }

<?php
header("content-type:text/html;charset=utf-8");
class animal {
    private $name; //私有的成员属性
    private $color;
    private $age;
    //_get( )方法用来获取私有属性
    public function _get( $property_name,$value) {
        if (isset( $this->$property_name)) {
            return $this-> $property_name;
        } else {
            return(NULL);
        }
    }
    //_set（）方法用来设置私有属性
    public function __set( $property_name, $value) {
        $this-> $property_name= $value;
    }
}
$tiger=new animal();
$tiger->name="老虎"; //自动调用_set()方法
$tiger->color="花花绿绿";
$tiger->age=2;
echo $tiger->name."<br>";
echo $tiger->color."<br>";
echo $tiger->age."<br>";
?>


    //4.3.2.3 isset  (检查)
    private function __isset($property_name) {
        return isset($this->$property_name);
    }

    //4.3.2.4 unset  (删除)
    private function __unset($property_name) {
        return unset($this->$property_name);
    }
}

$panda = new animal();

/**
 * private[私有的]
 * 1、仅针对当前class享有访问权限
 * 2、当前类的子类不允许访问
 * 3、类的外部均不允许访问
 */

 //!! __get __set 方法写了以后就会自动对private属性取值或者赋值

 // todo 需要同学测试 将__get __set 方法注释以后运行的结果和不注释运行的结果，作比较
$panda->name="狗熊";
$panda->color="棕色";
$panda->age=10;

echo $panda->name;
echo $panda->color;
echo $panda->age;

//todo 在类的外部使用isset函数来判断实体的变量是否设定


//4.3.2.4 unset 测试用例
echo $panda->name;//"狗熊";
unset($panda->name);
echo $panda->name;// 无输出
?> 