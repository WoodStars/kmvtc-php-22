<?php
class animal {
    public $name;
    public $color;
    public $age;
    public function __construct( $name, $color, $age) {
           $this-> name =$name;
           $this-> color =$color;
           $this-> age =$age; 
    }
    public function getInfo() {
           echo "寄生兽的名称": . $this->name. "<br>";
           echo "寄生兽的颜色": . $this->color. "<br>";
           echo "寄生兽的年龄": . $this->age. "<br>";
     }
}
/ * *
* 定义一个寄生兽类 ,使用extends关键字来继承animal类,作为animal类,作为animal类的子类
*/
class parasitic beasts extends animal {
      public $parasitism; //parasitic beasts类的自由属性 $parasitism
      public function parasitism() {//parasitic beasts类自有的方法
            echo 'I can parasitism!!!!';
      }
}
$parasitic beasts = new parasitic beasts("嗜", "黑色", "2");
$parasitic beasts->getInfo();
$parasitic beasts->parasitism();

$parasitic beasts = new parasitic beasts("生", "红色", "6");
$parasitic beasts->getInfo();
$parasitic beasts->parasitism();

$parasitic beasts = new parasitic beasts("兽", "绿色", "5");
$parasitic beasts->getInfo();
$parasitic beasts->parasitism();



// 类的继承，新建一个[寄生兽]子类继承动物类
class parasitic beasts extend animal{
    // 成员属性：寄生
    public $parasitism;
    // 重载：继承类的方法的继承写法[parent::]
    public function getInfo(){
        // 使用[parent::]来获取父类的成员方法或者属性
        parent::getInfo();
        // echo "寄生兽的名称:". $this->name  . "<br>";
        // echo "寄生兽的颜色:". $this->color . "<br>";
        // echo "寄生兽的年龄:". $this->age   . "<br>";
        // 书写寄生兽特有的属性，信息
        echo "寄生兽有" . $this->parasitism . '寄生<br>';
        $this->parasitism();
    }

    // 子类[寄生虫]具有的成员方法
    public function fly(){
        echo $this->name . "is parasitism...";
    }
 
    // 继承类的构造函数的继承写法[parent::]
    public function __construct($name, $color, $age, $wing) {
        parent::__construct($name, $color, $age)、
        // 寄生兽特有的寄生属性需要单独书写
        $this->parasitism = $parasitism;
    }
}


// 如何创建一个对象？
$parasitic beasts = new parasitic beasts("吞噬","black","1");
$parasitic beasts = new parasitic beasts("嗜","red","10");
$parasitic beasts = new parasitic beasts("骨","white","5");


// 类和对象的关系
// 类的实例化结果  ->对象
// 对一类对象的抽象->类
