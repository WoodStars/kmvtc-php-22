<?php

// 新建一个[恐龙]类
class animal {

    // ====================成员属性========================
    private $name;
    private $color;
    private $age;

    // ====================成员属性========================
    public function getInfo() {
        echo "恐龙的名称:". $this->name  . "<br>";
        echo "恐龙的颜色:". $this->color . "<br>";
        echo "恐龙的年龄:". $this->age   . "<br>";
    }

    // 构造函数 
    public function __construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    }

}

$dinosaur = new dinosaur("霸王龙","黑色","20");
// $dinosaur->getInfo();
echo $dinosaur->name;
$dinosaur = new dinosaur("翼龙","白色","5");
// $dinosaur->getInfo();
echo $dinosaur->name;
$dinosaur = new dinosaur("古龙","蓝色","6");
// $dinosaur->getInfo();
echo $dinosaur->name;、
$dinosaur = new dinosaur("剑尾龙","红色","4");
// $dinosaur->getInfo();
echo $dinosaur->name;
?>`

// 查看浏览器回显报错,下属代码在类的外部尝试获取animal类的实体化dinosaur对象的name属性
echo $dinosaur->name;

//==============================================================================================
// 访问修饰符
// 1.public    公有的  无访问限制,不做特别说明,均默认为声明的是public,成员内部、外部均可访问
// 2.private   私有的  仅针对当前class享有访问权限,当前类的子类以及类的外部均不允许访问
// 3.protected 保护的  所属类及其子类享有访问权限，外部代码不可访问

class MyClass 
{
    public    $public    = 'Public';
    //第一个public是访问修饰符;
    //第二个public是一个叫做public的成员属性,变量
    //第三个public是$public的成员属性的值
    private   $private   = 'Private';
    protected $protected = 'Protected';


    function printHello()  {
        // 成员内部引用成员属性，无任何权限问题
        echo $this->$public . "<br>";
        echo $this->$private . "<br>";
        echo $this->$protected . "<br>";
    }
}

$myClass = new MyClass();
$myClass->printHello();
/**
 * Public
 * Private
 * Protected
 */

// 在成员外部进行成员属性的直接访问，访问修饰符起到权限判断的作用
echo $myClass->public;
echo $myClass->private;
echo $myClass->protected;

/**
 * Public
 * Fatal error : ......(不能访问 private & protected 属性)
 * Fatal error : ......(不能访问 private & protected 属性)
 */

 //子类中的访问修饰符========================
class MyClass2 extend MyClass {
    protected $protected = 'Protected2';

    function printHello()  {
        // 成员内部引用成员属性，无任何权限问题
        echo $this->$public . "<br>";
        echo $this->$private . "<br>";    // 不显示
        echo $this->$protected . "<br>";
    }
}

$myClass2 = new MyClass2();
echo $myClass2->public;
echo $myClass2->private;
echo $myClass2->protected;

$myClass2->printHello();
