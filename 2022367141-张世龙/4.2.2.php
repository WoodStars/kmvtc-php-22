<?php
class phone {
    public $name;
    public $color;
    public $model;
    public function__construct( $name, $color, $model) {
        $this->name=$name;
        $this->color=$color;
        $this->model=$model;
    }
    public function getInfo() {
        echo "手机的名称：" . $this->name . "<br>";
        echo "手机的颜色：" . $this->color . "<br>";
        echo "手机的型号：" . $this->model . "<br>";
    }
} 
/**
 * 定义一个HUAWEI类，使用extends关键字来继承phone类，作为phone类的子类
 */
class HUAWEI extends phone {
    public $wing;//HUAWEI类自有的属性 $wing
    //重载类的方法的继承写法
    public function getInfo() {
        //parent:: 来获取父类的成员属性或方法
        parent::getInfo();
        echo "华为有" . $this->wing . '超清摄像头<br>';//书写华为的特有属性
        $this->camera() ;
    }
    public function camera() {
        echo "我有超清摄像头!!!";
    }
    public function __construct($name, $color, $model,$swing) {
        parent::__construct($name, $color, $model);
        //鸟类特有的属性需要单独命名
        $this->wing = $swing;
    }
}
$crow= new HUAWEI("P60","黑色","mate",3);
$crow->getInfo();
?>