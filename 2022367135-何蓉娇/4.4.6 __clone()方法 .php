<?php
class animal {
    private $name;    //私有成员属性
    private $color;
    private $age;
    public function __construct( $name, $color, $age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    public function getInfo() {
        echo'名字：'. $this->name . '颜色：' . $this->color . '年龄：' . $this->age .'.<br>';
    }
    public function __clone() {
        $this->name="萨摩耶";
        $this->color="白色";
        $this->age="2岁";
    }
}
$pig=new animal('猪','粉色','1岁');
$pig->getIfo();
$pig2=clone $pig;   //克隆对象
$pig2->getIfo();
?>