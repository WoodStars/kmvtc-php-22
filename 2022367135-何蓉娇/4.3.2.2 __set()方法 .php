<?php
header("Content-Type:text/html;charset=utf-8");
class animal {
    private $name;    //私有的成员属性
    private $color;
    private $age;
    //__get() 方法用来获取私有属性的值
    public function __get( $property_name) {
        if (isset( $this->$property_name)) {
            return $this->$property_name;
        } else {
            return(NULL);
        }
    }
    //__set()方法用来设置私有属性的值
    public function __set( $property_name, $value) {
        $this->$property_name= $value;
    }
}
    $dog=new animal();
    $dog->name="小狗";    //自动调用__set()方法
    $dog->color="白色";
    $dog->age=4;
    echo $dog->name."<br>"; //自动调用__set()方法
    echo $dog->color."<br>"; 
    echo $dog->age."<br>"; 
?>