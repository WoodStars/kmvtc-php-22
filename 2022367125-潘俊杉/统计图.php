<div class="page">
    <h1 style="width:100%">我的小账本</h1>
    <div id="zhuzhuangtu" class="zhuzhuangtu"></div>
    <div id="bingtu" class="bingtu"></div>
    <div id="mixzhu" class="mixzhu"></div>
    <div id="containe" class="containe"></div>
  </div>
  
  <script src="https://unpkg.com/@antv/g2/dist/g2.min.js"></script>
  <script>
    const data2 = [
      { item: '事例一', count: 40, percent: 0.4 },
      { item: '事例二', count: 21, percent: 0.21 },
      { item: '事例三', count: 17, percent: 0.17 },
      { item: '事例四', count: 13, percent: 0.13 },
      { item: '事例五', count: 9, percent: 0.09 },
    ];
    const chart2 = new G2.Chart({
      container: 'bingtu',
  
    });
  
    chart2.coordinate({ type: 'theta', outerRadius: 0.8 });
  
    chart2
      .interval()
      .data(data2)
      .transform({ type: 'stackY' })
      .encode('y', 'percent')
      .encode('color', 'item')
      .legend('color', { position: 'bottom', layout: { justifyContent: 'center' } })
      .label({
        position: 'outside',
        text: (data2) => `${data2.item}: ${data2.percent * 100}%`,
      })
      .tooltip((data) => ({
        name: data2.item,
        value: `${data2.percent * 100}%`,
      }));
  
    chart2.render();
  
  
    const data1 = [
      { 我的开销: '吃饭', sold: 1200 },
      { 我的开销: '抽烟', sold: 500 },
      { 我的开销: '零食', sold: 120 },
      { 我的开销: '游戏充值', sold: 350 },
      { 我的开销: '攒钱', sold: 100 },
    ];
  
    // 初始化图表实例
    const chart1 = new G2.Chart({
      container: 'zhuzhuangtu',
  
    });
  
    // 声明可视化
    chart1
      .interval() // 创建一个 Interval 标记
      .data(data1) // 绑定数据
      .encode('x', '我的开销') // 编码 x 通道
      .encode('y', 'sold'); // 编码 y 通道
  
    // 渲染可视化
    chart1.render();
  
  
    const data3 = [
      { name: 'AK-47一发入魂', 月份: '一月', 价格: 18.9 },
      { name: 'AK-47一发入魂', 月份: '二月', 价格: 18.9 },
      { name: 'AK-47一发入魂', 月份: '三月', 价格: 18.9 },
      { name: 'AK-47一发入魂', 月份: '四月', 价格: 18.9 },
      { name: 'AK-47一发入魂', 月份: '五月', 价格: 18.9 },
      { name: 'AK-47一发入魂', 月份: '六月', 价格: 18.9 },
      { name: 'AK-47一发入魂', 月份: '七月', 价格: 18.9 },
      { name: 'AK-47一发入魂', 月份: '八月', 价格: 18.9 },
      { name: 'AK-47一发入魂', 月份: '九月', 价格: 18.9 },
      { name: 'AK-47一发入魂', 月份: '十月', 价格: 18.9 },
      { name: 'AK-47一发入魂', 月份: '十一月', 价格: 18.9 },
      { name: 'AK-47一发入魂', 月份: '十二月', 价格: 18.9 },
      { name: 'AK-47二西莫夫', 月份: '一月', 价格: 12.4 },
      { name: 'AK-47二西莫夫', 月份: '二月', 价格: 12.4 },
      { name: 'AK-47二西莫夫', 月份: '三月', 价格: 12.4 },
      { name: 'AK-47二西莫夫', 月份: '四月', 价格: 12.4 },
      { name: 'AK-47二西莫夫', 月份: '五月', 价格: 12.4 },
      { name: 'AK-47二西莫夫', 月份: '六月', 价格: 12.4 },
      { name: 'AK-47二西莫夫', 月份: '七月', 价格: 12.4 },
      { name: 'AK-47二西莫夫', 月份: '八月', 价格: 12.4 },
      { name: 'AK-47二西莫夫', 月份: '九月', 价格: 12.4 },
      { name: 'AK-47二西莫夫', 月份: '十月', 价格: 12.4 },
      { name: 'AK-47二西莫夫', 月份: '十一月', 价格: 12.4 },
      { name: 'AK-47二西莫夫', 月份: '十二月', 价格: 12.4 },
  
    ];
  
    const chart3 = new G2.Chart({
      container: 'mixzhu',
    });
  
    chart3
      .interval()
      .data(data3)
      .encode('x', '月份')
      .encode('y', '价格')
      .encode('color', 'name')
      .transform({ type: 'stackY' })
      .interaction('elementHighlight', { background: true });
  
    chart3.render();
  
    const data = [
    { year: '1991', value: 3 },
    { year: '1992', value: 4 },
    { year: '1993', value: 3.5 },
    { year: '1994', value: 5 },
    { year: '1995', value: 4.9 },
    { year: '1996', value: 6 },
    { year: '1997', value: 7 },
    { year: '1998', value: 9 },
    { year: '1999', value: 13 },
  ];
  
  const chart = new G2.Chart({
    container: 'container',
    autoFit: true,
  });
  
  chart
    .data(data)
    .encode('x', 'year')
    .encode('y', 'value')
    .scale('x', {
      range: [0, 1],
    })
    .scale('y', {
      domainMin: 0,
      nice: true,
    });
  
  chart.line().label({
    text: 'value',
    style: {
      dx: -10,
      dy: -12,
    },
  });
  
  chart.point().style('fill', 'white').tooltip(false);
  
  chart.render();
  
  chart.render();
  
  
  </script>
  
  <style>
    .page {
      height: 100%;
      width: 100%;
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
    }
  
    .zhuzhuangtu {
      width:50%;
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center;
  
    }
  
    .bingtu {
      width:50%;
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center;
    }
  
    .mixzhu{
      width: 50%;
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center;
    }
  
  
    .containe{
      width: 50%;
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center;
    }
  </style>