<?php
class fruit {
    //成员属性
    public $name="";
    public $color="";
    public $price="";
    //构造函数（创建对象时为对象赋初始值）
    public function __construct( $name, $color, $price) {
        $this->name=$name;
        $this->color=$color;
        $this->price=$price;
    }
    //成员属性
    public function getinfo() {
        echo"水果的名称:" . $this->name . "<br>";
        echo"水果的颜色:" . $this->color . "<br>";
        echo"水果的价格:" . $this->price . "<br>";
    }
}
/**
 * 定义一个stone fruit类，使用extends关键字来继承fruit类，作为fruit的子类
 */
// 类的继承，新建一个【kernel】子类继承水果类
class kernel extends fruit {
    //成员属性：核仁
    public $kernel;
    //重载：继承类的方法的继承写法【parent::】
    public function germinate() {
        // 使用【parent::]来获取成员方法或者属性
        parent::getInfo();
       // echo"水果的名称:" . $this->name . "<br>";
        //echo"水果的颜色:" . $this->color . "<br>";
        //echo"水果的价格:" . $this->price . "<br>";
        echo "核果类有"  . $this->hard . '核仁<br>';
        $this->germinate();
    }
    // 子类stone fruit具有的成员方法
    public function germinate() {
        echo"我会发芽！！！";
}
     //继承类的构造函数的继承写法[prent::]
    public function __construct( $name, $color, $price,$kernel) {
        parent::__construct( $name, $color, $price);
    // 有核水果特有的核仁属性需要单独书写  
        $this ->kernel = $kernel ;
    }
}
$crow=new stone fruit ("榴莲","黄色","60","美味的");
$crow ->getinfo();
?>