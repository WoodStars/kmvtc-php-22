<?
//gettype()
$a="Hello" ;
echo gettype( $a). "<br>";  //输出结果:string
$b=array(1,2,5) ;
echo gettype( $b). "<br>";  //输出结果:array
//intval
echo intval(4.5)."<br>";  //输出结果为:4
//var_dump
var_dump( $a) ;    //输出结果:string(5)  "Hello"
echo "<br>" ;
var_dump( $a, $b ) ;
//输出结果:string(5) "Hello" array(3) { [0] =>int(1) [1]=> int(2) [2]=> int(5) }


//floor()函数
echo(floor(0.60)). "<br>";
echo(floor(0.40)). "<br>";
echo(floor(5)). "<br>";
echo(floor(5.1)). "<br>";
echo(floor(-5.1)). "<br>";
echo(floor(-5.9)). "<br>";


//rand()函数
echo rand(). "<br>" ;
echo rand(). "<br>" ;
echo rand(10,100). "<br>" ;


//explode()函数
// array explode(string separator,string string,[int limit])
$pizza = "piecel piece3 piece4 piece5 piece6" ;
$pieces = explode( " ", $pizza ) ;
echo $pieces[0] ."<br>";
echo $pieces[1] ."<br>";


//md5()函数
echo md5("apple")."<br>";


//checkdate()函数
//bool chechdate( int month, int day, int year )
var_dump( checkdate(12,31,2000))."<br>" ;
var_dump( checkdate(2,29,2001))."<br>" ;

//mktime()函数
 //设置时区
 date_default_timezone_set("PRC");
 //checkdate()
 var_dump(checkdate(12,31,2000))."<br>";
 var_dump(checkdate(2,31,2000))."<br>";
 //mktime()
 echo time()."<br>" ;
 echo mktime(0,0,0,12,25,2016)."<br>" ;
 //今天距2030年国庆节还有多少天
 echo "今天距2030年国庆节还有" . ceil((mktime(0,0,0,10,1,2030)-time())/(24*60*60))."天<br>" ;
 //data()返回格式化的当地时间
 echo "现在是:".date('Y-m-d H:i:s') ;
 

?>