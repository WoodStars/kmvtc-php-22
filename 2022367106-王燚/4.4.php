<?php
class animal{
    // ====================成员属性========================
    private $name;
    private $color;
    private $age;
    private static $sex = "雄性"; 
    // 构造函数 
    public function __construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    }
    private function getInfo()  {
        echo "动物的名称:". $this->name  . "<br>";
        echo "动物的颜色:". $this->color . "<br>";
        echo "动物的年龄:". $this->age   . "<br>";
       // echo "动物是雄性的";   .self::$sex;
       self::getSex();
    }
}

//echo animal::$sex;// 静态成员不能在外部访问
//echo animal::$color;// 只有static属性可以通过类来访问，因为static 定义的属性是在类加载的时候压入内存的，非static的，在每一个实例被初始化的时候再亚茹内存，有多个实例，就有多个属性
//animal::getInfo();// 使用静态方法

//4.4.3self关键字（类本身，不指向任何已经实例化的对象）
class animal{
    private static $firstCry=0;//私有的静态变量
    private $lastCry;
    //构造函数
function __construct(){
    $this->lastCry =++self::$firstCry;   //++0= 0+1
}
    function printLastCry(): void
    {
        var_dump($this->lastCry);
    }
}
$animalXxl1 = new animalXxl();  //实例化对象
$animalXxl1->printLastCry(); //输出：int（1）

//4.4.4 const常量
class MyClass{
    //定义一个常量
    const constant = '我是一个常量';
    function showConstant(): void
    {
        echo self::constant . "<br>";//类中访问常量
    }
}
echo Cla2::constant . "<br>";// 使用类名访问常量
$cla2 = new Cla2();
$cla2->showConstant();// 我是一个常量

//4.4.5 __toString()方法
class testToString
{
    public mixed $foo;
    public function getFoo()
    {
        return $this->foo;
    }
    // 构造函数,初始化,实例化对象的时候调用,不需要手动调用,自动调用,只调用一次
    public function __construct($foo)
    {
        $this->foo = $foo;
    }
    public function __toString()
    {
        return $this->foo;
    }
}
$testToString = new testToString("我是一个对象");
//直接输出对象
echo $testToString;// 我是一个对象

4.4.6//__clone 方法
//require "./animal.php";
// ./ 代表当前目录
// ../ 代表上一级目录
// / 代表根目录

class animal {
    private $name ;
    private $color;
    private $age;
    public functionn__construct($name,$colo,$age) {
        $this->name=$name;
        $this->color=$color;
        $this->age=$age;
    }
    pubilc function getInfo() {
        echo'名字:'.$this->name . ,'颜色：'. $this->color . ,'年龄：'. $this->age.'.<br>';
    }
    public functionn__clone(){
        $this->name="狗";
        $this->color="黑";
        $this->age="2岁";
    }
}
    $pig = new animal("猪", "粉色", 2);
    $pig2 = clone $pig;
    //$pig2->getInfo();
    $pig2->setName("小猪");//克隆对象
    $pig2->setAge(18);
    
    
    // 4.4.7__call() 
    class callTest{
        //$name 参数是要调用的方法名称
        //$arguments 参数是一个数组，包含了要传递给方法的参数
        public function __call($function_name, $arguments) {
            print "你所调用的函数：'$function_name' 不存在！\n";
            print "你所传递的参数是：\n";
            print_r($arguments);
            }
        }
    $test=new Test();
    $test->demo ("one","two","three");        
        
   
    



