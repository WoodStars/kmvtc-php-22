<?php
class fruit {
    //成员属性
    public $name="";
    public $color="";
    public $price="";
    //构造函数（创建对象时为对象赋初始值）
    public function __construct( $name, $color, $price) {
        $this->name=$name;
        $this->color=$color;
        $this->price=$price;
    }
    //成员属性
    public function getinfo() {
        echo"水果的名称:" . $this->name . "<br>";
        echo"水果的颜色:" . $this->color . "<br>";
        echo"水果的价格:" . $this->price . "<br>";
    }
    //析构函数 （销毁对象时执行）
    public function __destruct() {
        echo"好吃:" . $this->name . "<br>";
        echo"美味:" . $this->name . "<br>";
    }
}
//通过关键字new实例化出一个对象
$apple = new fruit("苹果","红色","15");
$apple->getinfo();
$orange = new fruit("橙子","黄色","9");
$orange->getinfo();
?>
