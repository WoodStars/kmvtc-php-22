<?php
class fruit {
    //成员属性
    public $name="";
    public $color="";
    public $price="";
    //构造函数（创建对象时为对象赋初始值）
    public function __construct( $name, $color, $price) {
        $this->name=$name;
        $this->color=$color;
        $this->price=$price;
    }
    //成员属性
    public function getinfo() {
        echo"水果的名称:" . $this->name . "<br>";
        echo"水果的颜色:" . $this->color . "<br>";
        echo"水果的价格:" . $this->price . "<br>";
    }
}
/**
 * 定义一个stone fruit类，使用extends关键字来继承fruit类，作为fruit的子类
 */
// 类的继承，新建一个【stone fruit】子类继承水果类
class stone fruitextends fruit {
    //成员属性：核仁
    public $kernel;
    //子类具有的成员方法
    public function germinate(){
        echo 'I can germinate!!!';
    }
}
$crow=new kernel ("榴莲","黄色","60");
$crow ->getinfo();
$crow ->germinate();//为对象属性赋值
?>
