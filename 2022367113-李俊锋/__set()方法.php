<?php
header("Content-Type:text/html;charset=utf-8");
class animal {
    private $name;  //私有的成员属性
    private $color;
    private $age;
    //__get（）方法用来获取私有属性
    public function __get( $property_name) {
        if (isset( $this-> $property_name)){
        return ( $this-> $property_name);
    } else {
        return(NULL);
    }
}
//__set()方法用来设置私有属性
public function __set( $property_name, $value) {
    $this->$property_name=$value;
    }
}
$panda=new animal();
$panda->name="小熊猫";//自动调用__set()方法
$panda->color="黑白色";
$panda->age=50;
echo $panda->name."<br>";//自动调用__get()方法
echo $panda->color."<br>";
echo $panda->age."<br>";
?>
