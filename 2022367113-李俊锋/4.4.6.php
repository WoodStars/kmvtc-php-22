<?php
class animal {
    private $name ; //私有属性
    private $color;
    private $age;
    public function __construct( $name, $color, $age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    public function getInfo() {
        echo'名字:'. $this->name. '颜色:'. $this->color. '年龄:'. $this->age. '.<br>';
    }
    public function __clone() {
        $this->name="狗";
        $this->color="白";
        $this->age="11岁";
      }
    }
    $pig=new animal('猪','黑','5岁');
    $pig->getInfo();
    $pig2=clone $pig; //克隆对象
    $pig2->getInfo();
    ?>