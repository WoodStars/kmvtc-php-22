<?php
class plants {
    public $name;//成员属性
    public $color;
    public $age;
    public function __construct( $name, $color, $age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
}
public function getInfo() {
    echo "植物的名称:" . $this->name . "<br>";
    echo "植物的颜色:" . $this->color . "<br>";
    echo "植物的年龄:" . $this->age . "<br>";
}
/**
 * 析构方法，在对象销毁时自动调用
 */
public function __destruct() {
    echo "再见:" . $this->name."<br>";
  }
}
$rose=new plants('玫瑰','粉色',8);//通过 new 关键字实例化出一个对象，名称为 rose
$rose->getInfo();
$cactus=new plants('仙人掌','绿色',100);//通过 new 关键字实例化出一个对象，名称为 cactus
$cactus->getInfo();
$meaty=new plants('多肉','淡绿',18);
$meaty->getInfo();
$tulip=new plants('郁金香','淡粉色',30);
$tulip->getInfo();
?>