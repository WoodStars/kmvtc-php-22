<?php
class MyClass {
    //定义一个变量
    const constant='我是个常量!';
    function showConstant() {
        echo self::constant."<br>"; //类中访问常量
    }
}
echo MyClass::constant."<br>"; //使用类名来访问常量
$class=new MyClass();
$class->showConstant(); //我是一个常量
?>