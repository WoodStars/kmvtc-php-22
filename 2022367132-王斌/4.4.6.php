<?php
class animal{
    private $name;//私有成员属性
    private $color;
    private $age;
    public function __construct( $name, $color, $age){
        $this->name=$name;
        $this->color=$color;
        $this->age=$age;
    }
    public function getInfo(){
        echo '名字:'. $this->name . ,'颜色:' . $this->color . ,'年龄:'. $this->age . '.<br>';
    }
    public function __clone(){
        $this->name="杨敏润";
        $this->color="五彩斑斓";
        $this->age="20岁";
    }
}
$pig=new animal('杨润敏'，'黑','3岁');
$pig->getInfo();
$pig2=clone $pig;//克隆对象
$pig2->getInfo();
?>