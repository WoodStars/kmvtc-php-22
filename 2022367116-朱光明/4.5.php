<?php
abstract class animal
{
    abstract function shout();

}
{
    // 抽象方法
     function shout(){
        echo "动物叫声";
     };

}
class dog extends animal{
    function shout(): void
    {
        echo "汪汪汪";
    }
}

class cat extends animal{
    function shout(): void
    {
        echo "喵喵喵";
    }
}

// 实例化dog类
$dog = new dog();
// 调用dog类的shout方法
$dog->shout();
echo "\n";
// 实例化cat类
$cat = new cat();
// 调用cat类的shout方法
$cat->shout();
echo "\n";