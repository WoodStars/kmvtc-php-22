<?php
class animal {
   public $name;
   public $color;
   public $age;
   public function __construct( $name, $color, $age) {
         $this->name= $name;
         $this->color= $color;
         $this->age= $age;
   }
   public function getIonfo() {
       echo "动物的名称:" . $this->name . "<br>";
       echo "动物的颜色:" . $this->color . "<br>";
       echo "动物的年龄:" . $this->age . "<br>";
   }
}
//用extends关键字来继承animal类，作为animal类的子类
class bird extends animal {
    public $wing ;
    public function fly() {
        echo 'I can fly!!!';
    }
}
$crow=new bird("乌鸦","黑色","3");
$crow->getIonfo();
$crow->fly();
?>