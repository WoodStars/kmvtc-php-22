<?php
class game
{
    public $name;
    public $type;
    public $age;
    public function __construct($name, $type, $age)
    {
        $this->name = $name;
        $this->type = $type;
        $this->age = $age;
    }
    public function getInfo()
    {
        echo "游戏的名称:" . $this->name . "<br>";
        echo "游戏的类型:" . $this->type . "<br>";
        echo "游戏的年龄:" . $this->age . "<br>";
    }
    public function __destruct()
    {
        echo "再见:" . $this->name . "<br>";
    }
}
$rainbowsix = new game('彩虹六号', '射击', "9");
$rainbowsix->getInfo();
$Valorant = new game('瓦罗兰特', '射击', '3');
$Valorant->getInfo();
?>  