<?php


//4.4.1 static 静态的
// 新建一个[动物]类
class animal {

    // ====================成员属性========================
    private $name;
    private $color;
    private $age;
    private static $sex = "雄性";  、//私有的静态属性
    // ====================成员属性========================
    // public function getInfo() {
    //     echo "动物的名称:". $this->name  . "<br>";
    //     echo "动物的颜色:". $this->color . "<br>";
    //     echo "动物的年龄:". $this->age   . "<br>";
    // }

    // 构造函数 
    public function __construct($name, $color, $age) {
        $this->name  = $name;
        $this->color = $color;
        $this->age   = $age;
    }
    // 静态成员方法
    private function getInfo()  {
        echo "动物是雄性的";   
    }
}

echo animal::$sex;  //错误，静态属性私有，外部不能访问
echo animal::$sex;  //只有static属性可以通过类来访问，因为static
animal::getInfo();  //调用静态方法


4.4.2 final //关键字

class animal{
    final public function getInfo(){

    }
}
class bird extends animal(){
    public function getInfo(){

    }
}
$crow =new bird();
$crow->getINfo();

4.4.3 self 关键字
class animal{
    privste static $firsCry=0;//私有的静态变量
    private $lastCry;
    //构造方法
    funxtion__construct(){
        $this->lastCry=++self::$fistCry;
    }
    function printLastCry(){
        var_dump($this->lastCry);
    
    }
}
$bird=new aninak();//实例化对象
$biird=printLastCry();//输出：int(1)


4.4.4 const(关键字)  常量
// const 用于定义常量，常量是不可改变的值 
//使用define()函数定义常量
class MyClass{
    //定义一个常量
    const constant ='我是个常量!';
    function showConstant(){
        echo self::constant."<br>"//类中访问常量
    }
}
echo MyClass::constant."<br>";//使用类名来访问常量
$class=new MyClass();
$class->showConstant();

4.4.5 const 关键字
class TestClass{
    public $foo;
    public function __constant($foo){
        $this->foo=$foo;//定义一个—toString()方法，返回一个成员属性$foo
        public function __toString(){
            retun $this->foo;
        }
    }
}
$class =new TestClass('Hello');
//直接输出对象
echo $class;  //输出结果为'Hello'


4.4.5 __toString()方法

__clone()方法
//当对象被克隆时，__clone()方法会被调用。

class animal{
    private $name;//成员属性name
    private $color;
    private $age;
    public function __construct($name;$color,$age){
        $this->name;
        $this->$color;
        $this->$age;

    }
    public function getinfo(){
        echo'名字 :'. $this->name.,'颜色:'. $this->color.,'年龄:'. $this->age.'.';
    }
}
$pig=new animal('猪','白色','1岁');
$pig2=vlone $pig;   //克隆对象
$pig2->getinfo();


4.4.7__call()方法
class callTest{
    //$name参数是要用调用的方法名称
    //$arguments参数是一个数组，包含了要传递给方法的参数
    public function __call($function_name,$argumets){
        print "你所调用的函数；'$function_name'不存在！\n";
        print "你所传递的参数是："
    }
    }
    class Test{
        function __call($function_name,$ages){
            print"你所调用的函数:$function_name(参数:";
            print_r($ages);
            print")不存在!<br>"
        }
    }

$test=new Test();
$test->demo("one","two","three");



 4.4.8 //自动加载类
class MyClass{

}
class Myclass2{

}
spl_autoloada_register(function($class_name){
    require_once $class_name . '.class.php';
});
$obj =new MyClass();
print_r($onj);
$obj2=new MyClass();
print_r($obj2);




?>
