<?php
abstract class animaol
{
abstract function shout();//使用抽象abstract关键字声明一个抽象类

}
//定义dog类继承animal类
class dog extends animal{
    //实现抽象方法
    public function shout(){
        echo "汪汪......<br>";
    }
}
//定义cat类继承animal类
class cat extends animal{
    //实现抽象方法
    public function shout(){
        echo "喵喵.....<br>";

    }
}
dog=new dog();//通过new关键字实例化出一个对象，名称为dog
$dog->shout();//调用dog类的shout方法
$cat=new cat();
$cat->shout();//调过cat类的shout方法
?>
