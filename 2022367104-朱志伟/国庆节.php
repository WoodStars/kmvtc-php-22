<?php
//设置时区
date_default_timezone_set("PRC");
//checkdate
var_dump(checkdate(12,31,2000)) . "<br>";
var_dump(checkdate(2,29,2001)) . "<br>";
//mktime
echo time ()."<br>";
echo mktime(0,0,0,12,25,2016)."<br>";
//今天距2030国庆节还有多少天
echo "今天距2030国庆节还有" . ceil((mktime(0,0,0,10,1,2030) - time())/(24*60*60)) . "天 <br>";
//date()返回格式化的当地时间
echo "现在是:" .date ('Y-m-d H:i:s');
?>