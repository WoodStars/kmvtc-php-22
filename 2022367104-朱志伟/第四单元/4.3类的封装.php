<?php
//类的封装
header("Content-Type:text/html;charset=utf-8");
class flower {
    private $name;       //私有属性name
    private $color;      //私有属性color
    private $age;        //私有属性age
    public function __construct($name, $color, $age) {
        $this->name = $name;
        $this->color = $color;
        $this->age = $age;
}
public function getInfo() {
    echo"花的名称:". $this->name ."<br>";
    echo"花的颜色:". $this->color ."<br>";
    echo "花的年龄:". $this->age ."<br>";
}
}
$meigui = new flower("玫瑰","红色",1);
//$meigui->getInfo();
echo $meigui->name;


echo "==========================================================================="."<br>";
//设置封装
/**
 * 定义类 MyClass
 */
class MyClass {
    public $public='Public';      //定义公共属性$public
    protected $protected= 'Protected';//定义保护属性$protected
    private $private= 'Private';      //定义私有属性$private
    function printHello() {
        echo $this->public;
        echo $this->protected;
        echo $this->private;
    }
}
$obj=new MyClass();             //实例化当前类
echo $obj->public;              //输出 $public
//echo $obj->protected;//Fatal error: Cannot access protected protected property MyClass::$protected
//echo $obj->private;//Fatal error:Cannot access private property MyClass::$private
$obj->printHello();              //输出3个成员属性
/**
 * 定义类  MyClass2
 */
class MyClass2 extends MyClass {  //继承MyClass类
    //可以访问公共属性和保护属性，但是私有属性不可以访问
    protected $public= 'Protected2';
    function printHello() {
        echo $this->public;
        echo $this->protected;
        echo $this->private;
    }
}
$obj2=new MyClass2();
echo $obj2->public;             //输出$public
echo $obj2->protected; //Cannot access protected property MyClass2::$protected
echo $obj2->private;     //未定义
$obj2->printHello();



echo "==========================================================================="."<br>";

//4.3.2 _set()、_get()
header("Content-Type:text/heml;charset=utf-8");
class flower {
    private $name;//私有的成员属性
    private $color;
    private $age;
    //_get()方法用来获取私有属性的值
    public function__get($property_name) {
        if(isset($this->$property_name,$value)) {
            return $this->$property_name;
        } else {
            return(NULL);
        }
    }
    //__set()方法用来设置私有属性的值
     function__set($property_name,$value) {
        $this->$property_name=$value;
    }
}
$meigui=new flower();
$meigui->name="玫瑰";//自动调用set_()方法
$meigui->color="红色";
$meigui->age=4;
echo $meigui->name."<br>";//自动调用__get()方法
echo $meigui->color."<br>";
echo $meigui->sge."<br>";




echo "==========================================================================="."<br>";

// _isset()、_unset()方法
class flower {
    private $name;//私有的成员属性
    private $color;
    private $age;
    //__get()方法用来获取私有属性
    function __get($property_name) {
        if (isset($this->$property_name)) {
            return ($this->$property_name);
        } else {
            return(NULL);
        }
    }
    //__set()方法用来设置私有属性
    function __set($property_name,$value) {
        $this->$property_name=$value;
    }
    //__isset()方法
    function __isset($property_name) {
        return isset($this->$property_name);
    }

    //__unset()方法
    function __unset($property_name) {
        unset($this->$property_name);
    }
}
$meigui=new flower();
$meigui->name="玫瑰";
echo var_dump(isset($meigui->name)) . "<br>";
//调用__isset()方法,输出:bool(true)
echo $meigui->name . "<br>";
unset($meigui->name);//调用__unset()方法
echo $meigui->name;//无输出
?>