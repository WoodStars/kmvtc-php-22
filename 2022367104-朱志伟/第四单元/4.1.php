
<?php
class animal {
    public $name;
    public $color;
    public $age;
    public function __construct($name,$color,$age) {
        $this->name=$name;
        $this->color=$color;
        $this->age=$age;
    }
    public function getInfo() {
        echo"动物的名称:" . $this->name . "<br>";
        echo"动物的颜色:" . $this->color . "<br>";
        echo"动物的年龄:" . $this->age . "<br>";
    }
    /**
     * 
     */
    public function destruct() {
        echo "再见:" . $this->name."<br>";
    }
}
$pig=new animal('猪','白色',4);
$pig->getInfo();
$dog=new animal('狗','黑色',3);
$dog->getInfo();
?>



