<?php
interface animal    //定义animal接口
{
    function run();  //声明抽象方法
    function shout();
}
interface landanimal  //定义landanimal接口
{
    public function liveonland();
}
class dog implements animal, landanimal  //定义dog类,实现animal接口和landanimal接口
{
//实现接口中的抽象方法
    public function run()
    {
        echo "狗在跑<br>";
    }
    public function shout()
    {
        echo "汪……<br>";
    }
    public function liveonland()
    {
        echo "狗在陆地生活<br>";
    }
}
class cat implements animal    //定义cat类,实现animal接口
{
//实现接口中的抽象方法
    public function run()
    {
        echo "猫在跑<br>";
    }
    public function shout()
    {
        echo "喵……<br>";
    }
}
$dog=new dog();
$dog->run();
$dog->shout();
$dog->liveonland();
$cat=new cat();
$cat->run();
$cat->shout();
?>
