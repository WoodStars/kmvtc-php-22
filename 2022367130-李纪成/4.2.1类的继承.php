<?php
class vegetable{
    public $name
    public $color
    public $taste
    //构造函数（创建对象时为对象赋初始值)
    public function __construct($name,$color,$taste){
        $this->name=$name;
        $this->color=$color;
        $this->taste=$taste;
    }
    public function getInfo(){
        echo"菜的名称:" . $this->name . "<br>";
        echo"菜的颜色:" . $this->color . "<br>";
        echo"菜的味道:" . $this->taste . "<br>";
    }
}
//定义一个raw这个类，使用extends关键字来继承vegetable类，作为vegetable类的子类
class raw extends vegetable {
    public $raw; //raw类的自有属性$raw
    public function easy(){ //raw类的自有方法
        echo'I easy!';
    }
}
$tomato=new raw("西红柿","红色","好") //定义西红柿这个类的属性
$tomato->getInfo();
$tomato->easy();
?>
