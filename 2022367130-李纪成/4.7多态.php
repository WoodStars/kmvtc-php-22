<?php
adstract class animal
{
    public adstract function shout();
}
class dog extends animal //定义cat类,实现抽象类中的方法
{
    public function shout()
    {
        echo "汪……<br>";
    }
}
class cat extends animal //定义cat类,实现抽象类中的方法
{
    public function shout()
    {
        echo "喵……<br>";
    }
}
function animalshout($obj)
{
    if($obj instanceof animal)
    {
        $obj->shout();
    }
    else
    {
        echo "Error:对象错误！";
    }
}
$cat=new cat();
$dog=new dog();
animalshout($cat);
animalshout($dog);
animalshout($crow);
?>