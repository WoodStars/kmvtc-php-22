<?php
//4.4.1 static关键字 静态的
class animal 
{
    // ====================成员属性========================
    private $name;
    private $color;
    private $age;
    private static $sex = "雄性"; //私有的静态属性

    //静态成员方法
    private function getInfo()
    {
        echo "动物是雄性的。";  
    }
}
echo animal::$sex;//私有，静态成员不能在外部访问
echo animal::$color;//只有static属性可以通过类来访问，因为static 定义的属性是在类加载的时候压入内存的，非static的，
                      //在每一个实例被初始化的时候再亚茹内存，有多个实例，就有多个属性
animal::getInfo();//调用静态方法

public function __construct($name, $color, $age)
{
    $this->name  = $name;
    $this->color = $color;
    $this->age   = $age;
}
public function getInfo()
{
    echo "动物的名称:". $this->name  . "<br>";
    echo "动物的颜色:". $this->color . "<br>";
    echo "动物的年龄:". $this->age   . "<br>";
    self::getSex(); //echo"动物的性别:" . self::$sex;
}
private static function getSex()
{
    echo"动物的性别:" . self::$sex;
}
$dog=new animal("小狗","棕色",4);
$dog->getInfo();


//4.4.2 final关键字（定义类class和方法function，成员属性用define定义）
//使用final关键字标记的类不能被继承inherit
class animal
{
    final public function getInfo()
    {

    }
}
class bird extends anmial
{
    public function getInfo()
    {

    }
}
$crow=new bird();
$crow->getInfo();
// can not override(重写、覆盖) [final] method —— animal::getInfo() on line 12 final关键字的使用


//4.4.3 self关键字
//指向类本身，不能指向任何已实例化的对象
class animal
{
    private static $firstCry=0; //私有的静态变量
    private $lastCry;
    //构造方法(初始化实例化对象自动调用)
    function __construct()
    {
        $this->lastCry=++self::$firstCry;  //++0 = 0+1
    }
    function printLastCry()
    {
        var_dump($this->lastCry);
    }
}
$bird=new anmial();     //实例化对象
$bird->printLastCry();  //输出:int(1)

//4.4.4 const关键字
class MyClass
{
    //定义一个常量
    const constant='我是常量!';
    function showConstant()
    {
        echo self::constant."<br>"; //类中访问常量
    }
}
const MyClass::constant."<br>"; //使用类名来访问常量
$class=new MyClass();
$class->showConstant();


//4.4.5 __toString()方法
class TestClass
{
    public $foo;
    public function __constant($foo)
    {
        $this->foo=$foo;
    }
    //定义一个__toString()方法，返回一个成员属性$foo
    public function __toString()
    {
        return $this->foo;
    }
}
$class=new TestClass('Hello');
//直接输出对象
echo $class;      //输出'Hello'



//4.4.6 __clone()方法
//克隆对象 克隆后两个对象互不干扰
class animal
{
    private $name;  //成员属性name
    private $color;
    private $age;
    //构造方法(初始化实例化对象时自动调用)
    public function __construct($name,$color,$age)
    {
        $this->name=$name;
        $this->color=$color;
        $this->age=$age;
    }
    public function getInfo()
    {
        echo '名字:' . $this->name . '颜色:' . $this->color . '年龄:' . $this->age . '.';
    }
}
$pig=new animal('猪','白色','1岁');
$pig2=clone $pig;    //克隆对象
$pig2->getinfo();

class animal
{
    private $name;    //私有成员属性
    private $color;
    private $age;
    public function __construct($name,$color,$age)
    {
        $this->name=$name;
        $this->color=$color;
        $this->age=$age;
    }
    public function getInfo()
    {
        echo '名字:' . $this->name . '颜色:' . $this->color . '年龄:' . $this->age . '.<br>';
    }
    public function __clone()
    {
        $this->name="狗";
        $this->coler="黑";
        $this->age"2岁";
    }
}
$pig=new animal('猪','白色','1岁');
$pig->getinfo();
$pig2=clone $pig;     //克隆对象
$pig2->getinfo();



//4.4.7 __call()方法
//功能：该方法在调用的方法不存在时自动调用
class test
{
    function __call($function_name,$args)
    //function_name：要调用的方法名称
    {
        print "你所调用的函数:$function_name(参数:";
        print_r($args);
        print ")不存在!<br>";
    }
}
$test=new test();
$tets->demo("one", "two","three");
//数组索引是从0开始的
?>
