<?php
header( "Content-Type;text/html;charset=utf-8");
//定义接口
interface hunams{
    //声明抽象方法
    function run();
    function shout();
}
//定义dog类，实现hunams接口
class xxc implements hunams{
    //实现接口中的抽象方法
    public function run(){
        echo "小小超在奔跑<br>";
    }
    public function shout(){
        echo "救救我......<br>";
    }
}
//定义xxc类，实现hunams接口
class xmc implements huanms{
    //实现接口中的方法
    public function run(){
        echo "小梦超在抗日<br>";
    }
    public function shout() {
        echo "求求你.....<br>";
    }
}
$xxc=new xxc();
$xxc->run();
$xxc->shout();
$xmc=new xmc();
$xmc->run();
$xmc->shout();
?>