


<?php

// 新建一个[动漫]类
class wangmeishijie {

    // ====================成员属性========================
    public $name;
    public $chengyuan;
    public $jineng;

    // ====================成员属性========================
    public function getInfo() {
        echo "动漫人物的名称:". $this->name  . "<br>";
        echo "动漫人物的成员:". $this->chenyuan . "<br>";
        echo "动漫人物的技能:". $this->jineng   . "<br>";
    }

    // 构造函数 (创建对象时为对象赋初始值)
    // parent::__construct($name, $chenyuan, $jineng)
    public function __construct($name, $chengyuan, $jineng) {
        $this->name  = $石昊;
        $this->chenyuan = $主角;
        $this->jineng  = $鲲鹏;
    }

    // 析构函数 (销毁对象时执行)
    public function __destruct()
    {
        echo "再见:" . $this->name . "。<br>";
    }
}


// 类的继承，新建一个[xuanhuan]子类继承动物类
class xuanhuan extend wangmeishijie{
    // 成员属性：翅膀
    public $wing;
    // 重载：继承类的方法的继承写法[parent::]
    public function getInfo(){
        // 使用[parent::]来获取父类的成员方法或者属性
        parent::getInfo();
        // echo "动漫人物的名:". $this->name  . "<br>";
        // echo "动漫人物的成:". $this->chenyuan . "<br>";
        // echo "动漫人物的技能:". $this->jineng  . "<br>";
        // 书写动漫类特有的属性，信息
        echo "动漫类" . $this->wing . '玄幻<br>';
        $this->fly();
    }

    // 子类[xuanhuan]具有的成员方法
    public function fly(){
        echo $this->name . "is flying...";
    }
 
    // 继承类的构造函数的继承写法[parent::]
    public function __construct($name, $chengyuan, $jineng, $xuanhuan) {
        parent::__construct($name, $chengyuan, $xuanhuan)、
    
        $this->wing = $wing;
    }
}


$crow =new wangmei("石昊","猪脚","技能");
$crow->getinfo();
$crow->fly();
?>