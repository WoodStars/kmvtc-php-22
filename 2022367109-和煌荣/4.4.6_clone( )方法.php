<?php
class animal {
    private $name ;//成员属性 name
    private $color;
    private $age;
    public function __construct( $name, $color, $age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    public function getInfo() {
        echo '名字:'. $this->name . ,'颜色:'. $this->color. ,'年龄:'. $this->
        age.'.';
    }
    public function _clone() {
        $this->name="狗";
        $this->color="黑";
        $this->age="2岁";
    }
}
$pig=new animal('猪','白色','1岁');
$pig2=clone $pig; //克隆对象
$pig2->getInfo();
?>