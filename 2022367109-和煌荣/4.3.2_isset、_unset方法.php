<?php
header("Content-Type:text/html;charset=utf-8");
class animal {
   private $name;
   private $color;
   private $age;
   //_get()方法用来获取私有属性的值
   public function _get( $property_name){
    if (isset( $this->$property_name)) {
        return $this->$property_name;
    } else {
        return(NULL);
    }
   }
public function _set( $property_name, $value) {
    $this->$property_name= $value;
  }
  function _isset( $property_name) {
    return isset( $this->$property_name);
}
function _unset( $property_name) {
    unset( $this->$property_name);
 }
}
$pig=new animal=();
$pig->name="小猪";
echo var_dump(isset( $pig->name)) . "<br>";
echo $pig->name . "<br>";
unset( $pig->name);
echo $pig->name;
?>

