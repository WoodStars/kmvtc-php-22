<?php
class vegetables  {
    public $name;
    public $color;
    public $taste;
    
    // 构造函数
    public function __construct( $name, $color, $taste) {
            $this->name=$name;
            $this->color=$color;
            $this->taste=$taste;
    }

    
    public function getInfo() {
        echo "蔬菜名称：" . $this->name . "<br>";
        echo "蔬菜颜色: " . $this->color . "<br>";
        echo "蔬菜味道: " . $this->taste . "<br>"; 
    }

    // 析构函数
    public function __destruct(){
        echo "再见:" . $this->name. "<br>";

    }
}

$potato=new vegetables('土豆','黄色','多种多样');
$potato->getInfo();
$balsampear=new vegetables('苦瓜','绿色','苦');
$balsampear->getInfo();
$eggplant=new vegetables('茄子','紫色','辣');
$eggplant->getInfo();
$eggplant->getInfo();
 //类的继承
 class animal {
    public $name;
    public $color;
    public $climate;
    public function_construct($name, $color, $age) {
        $this->name= $name;
        $this->color=$color;
        $this->age=$climate;
        }
  public function getInfo(){
       echo "植物名称:" . $this->name. "<br>";
       echo "植物颜色:" . $this->color ."<br>";
       echo "植物气候:" . $this->climate."<br>";
  }
}

/**
*定义一个bird类，使用extends关键字来继承animal类，作为animal类的子类
*/
class grass extends animal {
   public $wing;
   public function fly() {
      echo 'I can fly!!!';
      }
  }
$crow=new grass ("小草" , "绿色" , "温带");
$crow->getInfo();
$crow->fly();

//4.3.2.1 get  (取值)
public function __get($property_name){
    // 检查属性 this-> 当前类  的 property_name 属性 是否存在
    if(isset($this->$property_name)){
        return (this->$property_name);
    }else {
        return(NULL);
    }
}
//4.3.2.2 set  (赋值)
public __set($property_name,$value){
    $this->$property_name = $value;
}

//4.3.2.3 isset  (检查)
public function __isset($property_name) {
    return isset($this->$property_name);
}

//4.3.2.4 unset  (删除)
public function __unset($property_name) {
    return unset($this->$property_name);
}
/**
 * private[私有的]
 * 1、仅针对当前class享有访问权限
 * 2、当前类的子类不允许访问
 * 3、类的外部均不允许访问
 */

 //!! __get __set 方法写了以后就会自动对private属性取值或者赋值

 // todo 需要同学测试 将__get __set 方法注释以后运行的结果和不注释运行的结果，作比较

$parrot = new animal();
$parrot->name="鹦鹉";
$parrot->color="绿色";
$parrot->age=16;

echo $parrot->name;
echo $parrot->color;
echo $parrot->age;

echo $parrot->name;
unset($parrot->name);
echo $parrot->name;

class animal {
      private $name;
      private $color;
      private $age;
      private static $sex="雄性";
      public static function getInfo() {
        echo "动物是雄性。";
      }

}

//echo animal::$sex;
animal::getInfo();
