<?php
//gettype()
$a="Hello";
echo gettype( $a)."<br>";  //输出结果：string
$b=array(1,2,5);
echo gettype( $b)."<br>";  //输出结果：array
//intval
echo intval(4.5)."<br>";   //输出结果：4
//var_dump
var_dump( $a);             //输出结果：string(5) "Hello"
echo "<br>";
var_dump( $a,$b);
//输出结果：string(5) "Hello" array(3) { [0] => int(1) [1] => int(2) [2] => int(2) [2] => int(5) }
?>


<hr>
<?php
echo( floor(0.60)) ;
echo( floor(0.40)) ;
echo( floor(5)) ;
echo( floor(5.1)) ;
echo( floor(-5.1)) ;
echo( floor(-5.9)) 
?>


<hr>
<?php
echo rand();
echo rand();
echo rand(10,100);
?>

<hr>
<?php
var_dump(checkdate(12,31,2000)) ;   //bool(true)
var_dump(checkdate(2,29,2001)) ;    //bool(false)
?>


<hr>
<?php
$pizza  = "piece1 piece2 piece3 piece4 piece5 piece6";
$pieces = explode(" ", $pizza);
echo $pieces[0];   //输出 piece1
echo $pieces[1];   //输出 piece2
?>

<hr>
<?php
echo md5("apple");
?>


<hr>
<?php
date_default_timezone_set("PRC") ;
//checkdate()
var_dump(checkdate(12,31,2000)). "<br>" ;   //输出:bool(true)
var_dump(checkdate(2,31,2000)). "<br>" ;    //输出:bool(false)
//mktime()
echo time(). "<br>" ;
echo mktime(0,0,0,12,25,2016). "<br>" ;
//今天距2030年国庆节还有多少天
echo "今天距2023年国庆节还有".ceil((mktime(0,0,0,10,1,2023)-time())/(24*60*60)). "天<br>" ;
//date()返回格式化的当地时间
echo "现在是:".date('Y-m-d H:i:s') ;
?>

<hr>
<?php
$array1 = array(1,2,3,4) ;                      //定义不带键名的数组
$array2 = array("color" => "red", "name" => "Mike", "number" => "01") ;
                                                //定义带键名的数组
$arrar3 = array(1 => 2, 2 => 4,5 => 6,8,10) ;   //定义省略某些键名的数组
?>


<hr>
<?php
$array1 = array("a" => 5,"b" => 10,20) ;
print_r( $array1) ;
?>

<hr>
<?php
$color=array("red","green","blue") ;
echo $color[0]."<br>";    //red
$color[3]="black" ;       
echo $color[3]."<br>";    //black
$age=array("Peter" =>"40","Ben" =>"38","Joe" =>"4") ;
echo "Peter的年龄为：". $age["Peter"]."<br>" ;    //Peter的年龄为：40
?>

<hr>
<?php
$family=array(
    "Father" =>array("name" =>"Peter","age" =>"40"),
    "Mother" =>array("name" =>"Ben","age" =>"38"),
    "Son" =>array("name" =>"Joe","age" =>"4")
);
echo "父亲的姓名：". $family["Father"]["name"] ;       //父亲的姓名：Peter
echo "孩子的年龄：". $family["Son"]["age"] ;           //孩子的年龄：4
?>

<hr>
<?php
$array=array(1,2,3,5 =>7,8,9) ;
echo count( $array) ;
echo sizeof( $array) ;
?>


<hr>
<?php
$n = 15 ;
$str = "hello" ;
$array = array(1,2,3) ;
$newarray = compact("n" , "str" , "array") ;
print_r( $newarray) ;
?>

<hr>
<?php
$array=array("key1" =>1,"key2" =>2,"key3" =>3) ;
extract( $array) ;
echo " $key1 $key2 $key3" ;
?>

<hr>
<?php
$a=array('green','red','yellow') ;
$b=array('avocado','apple','banana') ;
$c=array_combine( $a, $b) ;
print_r( $c) ;
?>

<hr>
<?php
$array1 = range(1,5) ;
$array2 = range(2,10,2) ;
$arrar3 = range("a" , "e") ;
print_r( $array1) ;
print_r( $array2) ;
?>

<hr>
<?php
$color = array("red","green","blue") ;
$age = array("Peter" => 40,"Ben" => 38,"Joe" => 4) ;
var_dump(array_key_exists("1", $color)) ;
var_dump(in_array("38", $age)) ;
?>

<hr>
<?php
$color = array("red","green","blue") ;
$age = array("Peter" => 40,"Ben" => 38,"Joe" => 4) ;
var_dump(array_search("green", $color)) ;
var_dump(array_search("38", $age)) ;
?>

<hr>
<?php
$color = array("red","green","blue") ;
echo key( $color) ."<br>" ;
next( $color) ;
echo key( $color) ."<br>" ;
?>

<hr>
<?php
$color = array("red","green","blue") ;
list( $red, $green, $blue) = $color ;
echo $red ;
?>

<hr>
<?php
$array1=array_fill(2,3,"red") ;
$keys=array("a",3,"b") ;
$array2=array_fill_keys( $keys,"good") ;
print_r( $array1) ;
print_r( $array2) ;
?>


<?php
$result = print "ok";//输出：ok
echo $result . "<br>";//输出：1
echo "I","Love","PHP" . "<br>";//输出：ILOVEPHP
?>

<hr>
<?php
printf("%b",5);
printf("%c",65);
echo "<br>";
printf("%b,%c,%.2f",5,65,3.1415);
echo"<br>";
printf("%d,%o,%e,%x",12,20,123.45,345);
?>

<hr>
<?php
header("Content-Type:text/html;charset=utf8");
$str="I Love PHP";
echo strlen( $str);     //输出：10
echo strlen("中国");    //输出：6
?>

<hr>
<?php
$str="I Love PHP";
echo strtolower( $str) ."<br>";   //输出：i love php
echo strtoupper( $str) ."<br>";   //输出：I LOVE PHP
?>


