<?php
class animal {
    private $name;
    private $color;
    private $age;
    //_get()方法用来获取私有属性的值
    public function __get( $property_name) {
        if (isset( $this-> $property_name)) {
            return ( $this-> $property_name);
        } else {
            return(NULL);
        }
    }
    //_set()方法用来设置私有属性
    function __set( $property_name, $value) {
        $this-> $property_name= $value;
    }
    //_isset()方法
    function __isset( $property_name) {
        return isset( $this-> $property_name);
    }

    //_unset()方法
    function __unset( $property_name) {
        unset( $this-> $property_name);
    }
}
$sheep=new animal();
$sheep->name="小羊";
echo var_dump(isset( $sheep->name)) . "<br>";
echo $sheep->name . "<br>";
unset( $sheep->name);
echo $sheep->name;
?>