<?php
class plant {
    private $name;  
    private $color;
    private $age;
    public function __construct( $name, $color, $age) {
        $this->name=$name;
        $this->color=$color;
        $this->age=$age;
    }
    public function getInfo() {
        echo '名字:' . $this->name . ,'颜色:' . $this->color . ,'年龄:' . $this->age . '.<br>';
    }
    public function __clone(){
        $this->name="玫瑰";
        $this->color="白色";
        $this->age="4岁";
    }
}
$willow=new plant('柳树','绿色','5岁');
$willow->getInfo();
$willow2=clone $willow;  //克隆对象
$willow2->getInfo();
?>