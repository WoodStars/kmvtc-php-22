<?php
header("Content-Type:text/html;charset=utf-8");
class animal {
    private $name;//私有成员属性
    private $color;
    private $taste;
    //_get（）方法用来获取私有属性的值
    public function _($propety_name) {
        if (isset($this->$propety_name)) {
            return $this->$propety_name;
        } else {
            return(NULL);
        }
    }
    //_se()方法用来设置私有属性的值
    public function _($propety_name, $value) {
        $this->$propety_name=$value;
    }
}
$tea = new drink();
$tea->name="奶茶";//自动调用_set()方法
$tea->color="咖色";
$tea->taste="甜";
        echo $tea->name  . "<br>";//自动调用_set()方法
        echo $tea->color . "<br>";
        echo $tea->taste  . "<br>";
        ?>
