 <?
 class animal {
    private $name;
    private $color;
    private $age;
    private static $sex = "雄性"; //私有的静态属性
    //静态成员方法
    private function getInfo()  {
        echo "动物是雄性的。";
    }}
    echo animal::$sex;//错误，静态属性私有，外部不能访问
    animal::getInfo();  //调用静态方法
?>    


<?php
header("Content-Type:text/html;charset=utf-8");
class animal {
    private $name;//私有成员属性
    private $color;
    private $age;
    private static $sex = "雄性"; //私有的静态属性
    public function _contruct($name,$color,$age) {
        $this->name=$name;
        $this->color=$color;
        $this->age=$age;
    }
    private function getInfo()  {
        echo "动物的名称:". $this->name  . "<br>";
       echo "动物的颜色:". $this->color . "<br>";
      echo "动物的年龄:". $this->age   . "<br>";
      echo animal::$sex;
    }
    private static function getSex(){
        echo"动物的性别：" . self :: $sex;
    }}
    $dog=new animal("小狗","黑色","4");
    $dog->geInfo();
    ?>

