<?php
class animal{
    private $name;
    private $color;
    private $age;
    function __get($property_name) {
        if (isset( $this->$property_name)) {
            return ( $this-> $property_name);
        }else{
            return(NULL);
        }
    }
    //__set(方法用来设置私有属性)
    function __set( $property_name, $value) {
        $this-> $property_name= $value;
    }
    //isset()方法
    function __isset( $property_name) {
        return isset( $this-> $property_name);
    }
    //__unset(方法)
    function __unset( $property_name) {
        unset( $this-> $property_name);
    }
}
$elephant=new animal();
$elephant->name="小象";
echo var_dump(isset( $pig->name)) . "<br>"
//调用__isset()方法，输出：bool(true)
echo $elephant->name . "<br>" ;    //输出:小象
unset( $elephant->name);  //调用__unset()方法
echo $elephant->name;   //无输出
?>