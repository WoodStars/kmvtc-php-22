<?php
class animal{
    public $name;
    public $color;
    public $age;
    //构造
    public function __construct($name,$color,$age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    public function getInfo() {
        echo "动物名称：" . $this->name ."<br>";
        echo"颜色:" . $this->color . "<br>";
        echo"年龄:" . $this->age. "<br>";
    }
    //析构方法
    public function __destruct_destruct() {
        echo "再见:" . $this->name."<br>";
    }

}
$dog= new animal('狗','白色',3);
$dog->getInfo();
$elephant= new animal('大象','灰白色',10);
$elephant->getInfo();
$peacock= new animal('孔雀','彩色',3);
$peacock->getInfo();
$lion= new animal('狮子','白色',5);
$lion->getInfo();
?>