<?php
//返回给定长度的随机字符串
function random_text( $count, $rm_similar = false) {
    //创建字符数组
    $chars = array_flip(array_merge(range(0,9) , range('A','B')));
    //删除容易引起混淆的相似单词
    if( $rm_similar) {
        unset( $chars[0], $chars[1], $chars[2], $chars[I], $chars[O], $chars[Z]);
    }
    //生成随机字符文本
    for ( $i = 0, $text = ''; $i < $count; $i++) {
        $text.=array_rand( $chars) ;
    }
    return $text;
}
//调用函数
include 'functions.php';
//开启或继续会话，保存图形验证码到会话中供其他页面调用
if (! isset( $_SESSION)) {
    session_start(); 
}
//创建65pxX20px 大小的图像
$width = 65;
$height = 20;
$image = imagecreate( $width, $height) ;
//为一幅图像分配颜色
$bg_color = imagecolorallocate( $image, 0x33, 0x66, 0xff);
//取得随机字符串
$text = random_text(5);
//定义字体、位置
$font = 5;
$x = imagesx( $image) /2 - strlen( $text) * imagefontwidth( $font) /2;
$y = imagesx( $image) /2 - imagefontheight( $font) / 2;
//输出字符到图形上
$fg_color = imagecolorallocate( $image, 0xff, 0xff, 0xff);
imagestring( $image, $font, $x, $y, $text, $fg_color);
//保存验证码到会话，用于比较验证
$_SESSION['captcha'] = $text;
//输出图像
header('Content-type:image/png');
imagepng( $image);
imagedestroy( $image);