<?php
header("Content-Type:text/html;charser=utf-8") ;
class animal{
    private $name;
    private $color;
    private $age;
    private static $sex="雄性" ;//私有的静态属性
    public function __construct( $name, $color, $age){
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    public function getinfo() {
        echo"名称:" . $this->name . "<br>" ;
        echo"颜色:" . $this->color . "<br>" ;
        echo"年龄:" . $this->age . "<br>" ;
        //echo"动物的性别:",self::$sex;
    }
    private static function getSex() {
        echo "动物的性别:" . self::$sex;
    }
    
}
$elephant=new animal("小象","灰色",4) ;
$elephant->getinfo() ;




//4.4.2final 关键字
class animal{
    final public function getinfo(){

    }
}
class elepant extends animal{
    public function getinfo(){ //尝试对父类的覆盖

    }
}
$crow=new elephant();
$crow->getinfo();



//4.4.3self 关键字
class animal {
    private static $firstCry=0;
    private $lastCry;
    //构造方法
    function __construct() {
        $this->lastCry = ++self:: $firstCry;
    }
    function printLastCry() {
        var_dump( $this->lastCry);
    }
}
$bird= new animal(); // 实例化对象
$bird->printLastCry(); //输出 ：int(1)




//4.4.4 const 关键字
 //const 用于定义常量，常量是不可改变的值
 //常量的值在定义后不能被修改，不需要$ 符号（定义后可以直接使用)
 //常量可以通过类名：：常量名的方式访问
class Myclass {
    //定义一个常量
    const constant='我是一个常量! ';
    function showConstant() {
        echo self::constant."<br>"; //类中访问常量
    }

}
echo MyClass::constant."<br>"; //使用类名来访问常量
$class=new MyClass();
$class->showConstant();



//4.4.5 _toString()方法
 //_toString() 方法用于一个类被当做字符串时应怎样回应
 // _toString() 只能被声明为public，在直接输出对象时自动调用
class TestClass {
    public $foo;
    public function __construct( $foo) {
        $this->foo= $foo;
    }
    //定义一个__toString()方法，返回一个成员属性 $foo
    public function __toString() {
        return $this->foo;
    }
}
$class=new TestClass('Hello');
//直接输出对象
echo $class; //输出结果为'Hello'



// _clone()方法
 // 当对象被克隆时，_clone()方法被调用，可以通过这个方法来复制对象的属性，但不能复制方法
 // _clone方法在直接克隆对象时自动调用，间接克隆对象时不会被调用
 // _clone() 方法不能传递参数，可以有返回值，但返回值会被忽略，且没有意义
 // _clone()方法不能被声明为static，只能被声明为public
class animal {
    private $name ; //私有成员属性
    private $color;
    private $age;
    public function __construct( $name, $color, $age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    public function getInfo() {
        echo'名字:' . $this->name .,'颜色:' . $this->color . ,'年龄:' . $this->age . '.<br>';
    }
    public function __clone() {
        $this->name="狗";
        $this->color="黑";
        $this->age="2岁";
    }

}
$pig=new animal('猪','白','1岁');
$pig->getinfo();
$pig2=clone $pig;  //克隆对象
$pig2->getinfo();


//4.4.7 _call()方法

class Test {
    function _call( $function_name, $arguments) {
        print "你所调用的函数: $function_name(参数:";
        print_r( $args);
        print")不存在! <br>";
    }
}
$test=new Test();
$test->demo("1","2","3");


//4.4.8自动加载类

class Myclass1{

}
class Myclass2{

}
spl_autoload_register(function ( $class_name) {
    require_once $class_name . '.class.php';

});
$obj =new Myclass1();
print_r( $obj);
$obj2=new Myclass2();
print_r( $obj2);

?>