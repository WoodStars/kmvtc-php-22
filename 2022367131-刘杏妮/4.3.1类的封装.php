<?php
//新建一个类
class film {
    private $name;//私有属性name
    private $types;//私有属性 types
    private $time;//私有属性 time
    //析构
    public function __construct( $name, $types, $time) {
        $this->name= $name;
        $this->types= $types;
        $this->time= $time;
    }
    public function getInfo() {
        echo "名称:" . $this->name . "<br>";
        echo "类型:" . $this->types . "<br>";
        echo "时间:" . $this->time . "<br>";
    }
    $dumbo= new film("小飞象","动画",2019);
    echo $dumbo->name;
}



/* *
 *定义类 myclass 
 */
class Myclass{
    public $public='public'; // 访问修饰符；
    protected $protected='protec';// 一个叫做public的成员属性
    private $private='private';// 是$public的成员属性的值
    function printHello() {
        echo $this->public;
        echo $this->protected;
        echo $this->private;
    }
}
$obj=new Myclass();
echo $obj->public;
//echo $obj->protected; //Fatal error: Cannot access protected property MYclass::$protected
//echo $obj->protected; //Fatal error: Cannot access protected property MYclass::$private
$obj->printHello();
/* * 
* 定义MyClass2
*/
class MyClass2 extends MyClass {
    protected $protected='protected2';
    function printHello() {
        echo $this->public;
        echo $this->protected;
        echo $this->private;
    }
}
$obj2=new Myclass();
echo $obj->public;
echo $obj2->private;
$obj2->printHello();
?>