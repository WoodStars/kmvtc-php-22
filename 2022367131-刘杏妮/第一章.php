<?php
$a = true; //布尔型
$username ="Mike" ;
if ( $username =="Mike") {
    echo"Hello,Mike! <br>";
}
//使用布尔值进行逻辑控制
if ( $a == true ) {
    echo"a为真<br>" ;
}
echo'\'\\<br>';//输出:'\
$a = 10;
echo'The a value is $a<br>';
echo "The a value is $a<br>";
//整型
$n1 = 123;
$n2 = 0;
$n3 = -123;
$n4 = 0123;
$n5 = 0x1b;
$n6 = 0b1010;

//浮点型
$pi = 3.1415926;
$width=3.3e4;
$var=3e-5
//复合型
$arrl = array(1,2,3,4,5,6,7,8,9);
$arr2 = array("animal" => "dog","color" => "red" );
echo $arrl[2];
echo $arr2["color"];
//对象
class Animal {
    public $name;
    public $age;
    public $weight;
    public $sex;
    public function run() {
        echo "Haha.I can run!";
    }
    public function eat() {
        echo"I can eat!";
    }

}
$dog = new Animal;
$dog->run() ;


//定义一个英雄类
class hero
{
    //血量
    public $hp = 100;
    //姓名
    public $name;
    //攻击力
    public $attack;

    public $shield = 0.2;

    function __construct($name, $attack)
    {
        $this->name = $name;
        $this->attack = $attack;
    }

    // 攻击
    function attack($hero)
    {
        echo $this->name, "攻击了", $hero->name,"<br>";
        $hero->hp -= $this->attack;

        $hero->hp = $hero->hp - $this->attack*(1-$hero->shield);
        echo $hero->name,"的剩余血量：", $hero->hp,"<br>";
    }
}

$hero1 = new hero("瑟提",6);
$hero2 = new hero("鲁班",10);

$hero1->attack($hero2);
$hero2->attack($hero1);
$hero2->attack($hero1);
$hero2->attack($hero1);
$hero2->attack($hero1);


?>