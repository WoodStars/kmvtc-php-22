<?php
header("Content-Type:text/html;charset=utf-8");
class animal {
    private $name; //私有的成员属性
    private $color;
    private $age;
    //_get()方法用来获取私有属性的值
    public function_get($property_name) {
        if (isset( $this->$property_name)) {
            return $this-> $property_name;
        } else {
            return(NULL);
        }
    }
    //_set()方法用来获取私有属性的值
    public function __set( $property_name, $value) {
        $this-> $property_name= $value;
    }
}
$pig=new animal();
$pig->name="猪";
$pig->color="粉色";
$pig->age=3;
echo $pig->name."<br>";
echo $pig->color."<br>";
echo $pig->age."<br>";
?>
