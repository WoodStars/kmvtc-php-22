<?php
class animal {
    public $name;
    public $color;
    public $age;
    public function_construct( $name, $color, $age) {
        $this->name= $name;
        $this->color= $color;
        $this->age= $age;
    }
    public function getInfo() {
        echo "动物的名称:" . $this->name . "<br>";
        echo "动物的颜色:" . $this->color . "<br>";
        echo "动物的年龄:" . $this->age . "<br>";
    }
}
//定义一个insects(昆虫)类，使用extends关键字来继承animal类，作为animal类的子类
class cats extends animal{
    public $wing; //insects类自有的属性 $wing (翅膀)
    public function fly() { //insects类自有的方法
        echo "i can fly!!!";
    }
   
}
$bee=new insects("蜜蜂","黄色",4);
$bee->getInfo();
$bee->fly();
?>
