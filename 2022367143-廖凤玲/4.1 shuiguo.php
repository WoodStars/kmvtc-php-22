<?php
class fruit {
    public $name;   //成员属性name
    public $color;   //成员属性color
    public $price;    //成员属性price
    public function __construct( $name, $color, $price) {  
       //输出 “我是构造方法”
        $this->name= $name;
        $this->color= $color;
        $this->price= $price;
}
public function getInfo() {
    echo "水果的名称:" . $this->name . "<br>";
    echo "水果的颜色:" . $this->color . "<br>";
    echo "水果的价格:" . $this->price . "<br>";
}
public function __destruct() {
    echo "好吃:" . $this->name."<br>";
    
  }
}
$apple = new fruit("苹果","红色","15"); //通过new关键字实例化出一个对象，名称为apple
$apple->getinfo();
$pineapple = new fruit("菠萝","黄色","9");
$pineapple->getinfo();
$watermelon = new fruit("西瓜","绿色","10");
$watermelon->getinfo();
?>