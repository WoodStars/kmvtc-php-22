<?php
class fruit {
    //成员属性
    public $name="";
    public $color="";
    public $price="";
    //构造函数（创建对象时为对象赋初始值）
    public function __construct( $name, $color, $price) {
        $this->name=$name;
        $this->color=$color;
        $this->price=$price;
    }
    //成员属性
    public function getinfo() {
        echo"水果的名称:" . $this->name . "<br>";
        echo"水果的颜色:" . $this->color . "<br>";
        echo"水果的价格:" . $this->price . "<br>";
    }
    //析构函数 （销毁对象时执行）
    public function __destruct() {
        echo"好吃:" . $this->name . "<br>";
        echo"美味:" . $this->name . "<br>";
    }
}
$apple = new fruit("苹果","颜色","价格");

// 需要同学查看浏览器回显报错，下属代码在类的外部尝试获取animal类的实体化dog对象的name属性
echo $apple->name;

//==============================================================================================
// 访问修饰符
// 1.public    公有的  无访问限制，不做特别说明，均默认为声明的是public，成员内部、外部均可访问
// 2.private   私有的  仅针对当前class享有访问权限，当前类的子类以及类的外部均不允许访问
// 3.protected 保护的  所属类及其子类享有访问权限，外部代码不可访问
class MyClass 
{
    public $public = 'Public';
    //第一个public是访问修饰符；
    //第二个public是一个叫做public的成员属性，变量
    //第三个public是$public的成员属性的值
    private   $private   = 'Private';
    protected $protected = 'Protected';


    function printHello()  {
        // 成员内部引用成员属性，无任何权限问题
        echo $this->$public . "<br>";
        echo $this->$private . "<br>";
        echo $this->$protected . "<br>";

    }
}

$myClass = new MyClass();
$myClass->printHello();
/**
 * Public
 * Private
 * Protected
 */

// 在成员外部进行成员属性的直接访问，访问修饰符起到权限判断的作用
echo $myClass->public;
echo $myClass->private;
echo $myClass->protected;

/**
 * Public
 * Fatal error : ......(不能访问 private & protected 属性)
 */

 //子类中的访问修饰符========================
 class MyClass2 extend MyClass {
    protected $protected = 'Protected2';

    function printHello()  {
        // 成员内部引用成员属性，无任何权限问题
        echo $this->$public . "<br>";
        echo $this->$private . "<br>";    // 不显示
        echo $this->$protected . "<br>";

    }
}

$myClass2 = new MyClass2();
echo $myClass2->public;
echo $myClass2->private;
echo $myClass2->protected;

$myClass2->printHello();


<?php
header("Content-Type:text/html;charset=utf-8");
class animal {
    private $name; //私有的成员属性
    private $color;
    private $age;
    //_get()方法用来获取私有属性的值
    public function_get($property_name) {
        if (isset( $this->$property_name)) {
            return $this-> $property_name;
        } else {
            return(NULL);
        }
    }
    //_set()方法用来获取私有属性的值
    public function __set( $property_name, $value) {
        $this-> $property_name= $value;
    }
}
$pig=new animal();
$pig->name="猪";
$pig->color="粉色";
$pig->age=3;
echo $pig->name."<br>";
echo $pig->color."<br>";
echo $pig->age."<br>";
?>

