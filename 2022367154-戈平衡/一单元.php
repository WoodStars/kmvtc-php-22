<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="UTF-8">
    <title>九九乘法表</title>
</head>

<body>
    <table border="1">
        <?php
        // 外层循环控制行数，即乘数
        for ($i = 1; $i <= 9; $i++) {
            echo "<tr>"; // 开始新的一行
            // 内层循环控制列数，即被乘数
            for ($j = 1; $j <= $i; $j++) {
                // 计算当前单元格的乘法结果
                $result = $i * $j;
                // 输出乘法结果，并在其后加上相应的中文注释
                echo "<td>$i × $j = $result</td>";
            }
            echo "</tr>"; // 结束当前行
        }
        ?>
    </table>
</body>

</html>
<!DOCTYPE html>
<html>

<head>
    <title>PHP实现简单计算器</title>
    <meta charset="UTF-8">
</head>

<body>
    <table align="center" border="1" width="500">
        <caption>
            <h1>caption我的计算器</h1>
        </caption>
        <form action="">
            <tr>
                <td>
                    <input type="text" size="5" name="num1" value="<?php echo $_GET["num1"] ?? ''; ?>">
                </td>
                <td>
                    <select name="ysf">
                        <option value="+" <?php echo ($_GET["ysf"] ?? '') === "+" ? "selected" : ""; ?>>+</option>
                        <option value="-" <?php echo ($_GET["ysf"] ?? '') === "-" ? "selected" : ""; ?>>-</option>
                        <option value="x" <?php echo ($_GET["ysf"] ?? '') === "x" ? "selected" : ""; ?>>x</option>
                        <option value="/" <?php echo ($_GET["ysf"] ?? '') === "/" ? "selected" : ""; ?>>/</option>
                        <option value="%" <?php echo ($_GET["ysf"] ?? '') === "%" ? "selected" : ""; ?>>%</option>
                    </select>
                </td>
                <td>
                    <input type="text" size="5" name="num2" value="<?php echo $_GET["num2"] ?? ''; ?>">
                </td>
                <td><input type="submit" name="sub" value="计算"></td>
            </tr>
            <?php if (isset($_GET["sub"])): ?>
                <tr>
                    <td colspan="4">
                        <?php
                        $num1 = $_GET["num1"] ?? 0;
                        // echo "用户输入的第1个值是",$num1;
                        $num2 = $_GET["num2"] ?? 0;
                        // echo "用户输入的第2个值是",$num2;
                        $message = "";
                        if (is_numeric($_GET["num1"]) && is_numeric($_GET["num2"])) {
                            $num1 = (float) $num1;
                            $num2 = (float) $num2;

                            $result = 0;
                            switch ($_GET["ysf"]) {
                                case '+':
                                    $result = $num1 + $num2;
                                    break;
                                case '-':
                                    $result = $num1 - $num2;
                                    break;
                                case 'x':
                                    $result = $num1 * $num2;
                                    break;
                                case '/':
                                    if ($num2 != 0) {
                                        $result = $num1 / $num2;
                                    } else {
                                        $message = "除数不能为0。";
                                        echo $message;
                                    }
                                    break;
                                case '%':
                                    if ($num2 != 0) {
                                        $result = $num1 % $num2;
                                    } else {
                                        $message = "除数不能为0。";
                                        echo $message;
                                    }
                                    break;
                            }
                            echo "结果是：" . $result;
                        } else {
                            $message = "请输入有效的数字。";
                            echo $message;
                        }
                        ?>
                    </td>
                </tr>
            <?php endif; ?>
        </form>
    </table>
</body>

</html>

<?

// 【布尔型】定义一个变量a，赋值为false
$a = false;
// 【字符串】定义一个变量username，赋值为“Mike”
$username = "Mikes";
// echo命令打印，显示
echo "the username is  ", $username, "<br>";
// 打印你好，今天是星期四
// echo "<h1>你\a好\n\r今天是\\疯狂星期四,v我\$50</h1><br>";

$a = 10;
echo "a的值是$a<br>";
echo "a的值是\$a<br>";
// ==是作比较
// = 赋值
if ($username == "Mike") {
    echo "Hi, Mike <br>";
    # code...
} else {
    echo "you are not Mike<br>";

}


if ($a == true) {
    echo "a is true";
} else {
    echo "a is false";
}

echo "<br>";
//整型
$n1 = 123;
echo "n1=", $n1, "<br>";
$n2 = 0;
echo "n2=", $n2, "<br>";
$n3 = -123;
echo "n3=", $n3, "<br>";
$n4 = 0123;
echo "n4=", $n4, "<br>";
$n5 = 0xFF;
echo "n5=", $n5, "<br>";
$n6 = 0b11111111;
echo "n6=", $n6, "<br>";
//浮点型
$pi = 3.1415926;
echo "pi=", $pi, "<br>";
$width = 3.3e4;
echo "width=", $width, "<br>";
$var = 3e-2;
echo "var=", $var, "<br>";

echo 3 ** 4;
// 数组
$arr1 = array(1, 2, 3, 4, 5, 6, 7, 8);
$arr2 = array("animals" => "dog", "color" => "red");
// 格式化代码 alt+shift+F
echo "<br>", $arr1[0];//1
echo "<br>", $arr1[7];//1
echo "<br>", $arr2["color"];//1

//=====================================================
// 对象
class Animal
{
    //基本属性
    var $name;
    var $age;
    var $weight;
    var $sex;
    function __construct($name)
    { // 构造函数
        $this->name = $name; // 将传入的参数赋值给属性 name
    }
    //基本方法
    public function run()
    {
        echo "haha,", $this->name, " is running<br>";
    }
    public function eat()
    {
        echo "haha,i am eating";
    }
}
$dog = new Animal("dog");
$cat = new Animal("cat");
$dog->run();
$cat->run();

// 定义一个英雄类
class hero
{
    //血量
    public $hp = 100;
    //姓名
    public $name;
    //攻击力
    public $attack;

    public $shield = 0.2;

    function __construct($name, $attack)
    {
        $this->name = $name;
        $this->attack = $attack;
    }

    // 攻击
    function attack($hero)
    {
        echo $this->name, "攻击了", $hero->name, "<br>";
        $hero->hp -= $this->attack;

        $hero->hp = $hero->hp - $this->attack * (1 - $hero->shield);
        echo $hero->name, "的剩余血量：", $hero->hp, "<br>";
    }
}

$hero1 = new hero("瑟提", 6);
$hero2 = new hero("鲁班", 10);

$hero1->attack($hero2);
$hero2->attack($hero1);
$hero2->attack($hero1);
$hero2->attack($hero1);
$hero2->attack($hero1);
?>

<?php
$a = true; //布尔型
$username = "Mike";
//使用了字符串进行逻辑控制
if ($username == "Mike") {
    echo "Hello,Mike! <br>";
}
//使用布尔值进行逻辑控制
if ($a == true) {
    echo "a 为真 <br>";
}
?>

<?php
echo (floor(0.60));
echo (floor(0.40));
echo (floor(5));
echo (floor(5.1));
echo (floor(-5.1));
echo (floor(-5.9))
    ?>

<?php
var_dump(checkdate(12, 31, 2000));
var_dump(checkdate(2, 29, 2001));
?>



<?php
$pizza = "piece1 piece2 piece3 piece4 piece5 piece6";
$pieces = explode(" ", $pizza);
echo $pieces[0];
echo $pieces[1];
?>


<?php
echo md5("apple");
?>



<?php
date_default_timezone_set("PRC");
var_dump(checkdate(12, 31, 2000)) . "<br>";
var_dump(checkdate(2, 31, 2000)) . "<br>";
echo time() . "<br>";
echo mktime(0, 0, 0, 12, 25, 2016) . "<br>";
echo "今天距2023年国庆节还有" . ceil((mktime(0, 0, 0, 10, 1, 2023) - time()) / (24 * 60 * 60)) . "天<br>";
echo "现在是:" . date('Y-m-d H:i:s');
?>


<?php
/*
//返回给定长度的随机字符串
function random_text( $count, $rm_similar = false) {
    //创建字符组
    $chars = array_flip(array_merge(range(0,9), range('A','Z')));
    //删除容易引起混淆的相似单词
    if ( $rm_similar) {
        unset( $chars[0], $chars[1], $chars[2],$chars['I'], $chars['O'], $chars['Z']);
    }
    //生成随机字符文本
    for ( $i = 0, $text = ""; $i < $count; $i++) {
        $text. =array_rand( $chars) ;
    }
    return $text;
}


include 'two.php';
echo random_text(5,true);
*/
?>

<?php
$a = true; //布尔型
$username = "Mike";
//使用了字符串进行逻辑控制
if ($username == "Mike") {
    echo "Hello,Mike! <br>";
}
//使用布尔值进行逻辑控制
if ($a == true) {
    echo "a 为真 <br>";
}
?>