<?php
date_default_timezone_set("PRC");
var_dump(checkdate(12, 31, 2000)) . "<br>";
var_dump(checkdate(2, 31, 2000)) . "<br>";
echo time() . "<br>";
echo mktime(0, 0, 0, 12, 25, 2016) . "<br>";
echo "今天距2023年国庆节还有" . ceil((mktime(0, 0, 0, 10, 1, 2023) - time()) / (24 * 60 * 60)) . "天<br>";
echo "现在是:" . date('Y-m-d H:i:s');
?>

<?php
echo (floor(0.60));
echo (floor(0.40));
echo (floor(5));
echo (floor(5.1));
echo (floor(-5.1));
echo (floor(-5.9))
    ?>

<?php
var_dump(checkdate(12, 31, 2000));
var_dump(checkdate(2, 29, 2001));
?>



<?php
$pizza = "piece1 piece2 piece3 piece4 piece5 piece6";
$pieces = explode(" ", $pizza);
echo $pieces[0];
echo $pieces[1];
?>

<!DOCTYPE html>
<html>

<head>
    <title>鸡兔同笼计算器</title>
</head>

<body>
    <h2>鸡兔同笼计算器</h2>

    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <p>总共有多少个头？<input type="number" name="heads" required></p>
        <p>总共有多少只脚？<input type="number" name="feet" required></p>
        <input type="submit" name="calculate" value="计算">
    </form>

    <?php
    if (isset($_POST['calculate'])) {
        $heads = $_POST['heads'];
        $feet = $_POST['feet'];

        // 计算鸡和兔子的数量
        $rabbits = ($feet - 2 * $heads) / 2;
        $chickens = $heads - $rabbits;

        // 检查解是否合法
        if ($rabbits >= 0 && $chickens >= 0 && $chickens == intval($chickens) && $rabbits == intval($rabbits)) {
            echo "<h3>鸡的数量：" . $chickens . "</h3>";
            echo "<h3>兔子的数量：" . $rabbits . "</h3>";
        } else {
            echo "<h3>无法找到合法的鸡和兔子数量。</h3>";
        }
    }
    ?>
</body>

</html>
<hr>
<?php
$pizza = "piece1 piece2 piece3 piece4 piece5 piece6";
$pieces = explode(" ", $pizza);
echo $pieces[0];
echo $pieces[1];
?>

<hr>
<?php
echo md5("apple");
?>


<hr>
<?php
date_default_timezone_set("PRC");
var_dump(checkdate(12, 31, 2000)) . "<br>";
var_dump(checkdate(2, 31, 2000)) . "<br>";
echo time() . "<br>";
echo mktime(0, 0, 0, 12, 25, 2016) . "<br>";
echo "今天距2023年国庆节还有" . ceil((mktime(0, 0, 0, 10, 1, 2023) - time()) / (24 * 60 * 60)) . "天<br>";
echo "现在是:" . date('Y-m-d H:i:s');
?>

<hr>
<?php
$array1 = array(1, 2, 3, 4);
$array2 = array("color" => "red", "name" => "Mike", "number" => "01");
$arrar3 = array(1 => 2, 2 => 4, 5 => 6, 8, 10);
?>


<hr>
<?php
$array1 = array("a" => 5, "b" => 10, 20);
print_r($array1);
?>

<hr>
<?php
$color = array("red", "green", "blue");
echo $color[0] . "<br>";
$color[3] = "black";
echo $color[3] . "<br>";
$age = array("Peter" => "40", "Ben" => "38", "Joe" => "4");
echo "Peter的年龄为：" . $age["Peter"] . "<br>";
?>

<hr>
<?php
$family = array(
    "Father" => array("name" => "Peter", "age" => "40"),
    "Mother" => array("name" => "Ben", "age" => "38"),
    "Son" => array("name" => "Joe", "age" => "4")
);
echo "父亲的姓名：" . $family["Father"]["name"];
echo "孩子的年龄：" . $family["Son"]["age"];
?>

<hr>
<?php
$array = array(1, 2, 3, 5 => 7, 8, 9);
echo count($array);
echo sizeof($array);
?>


<hr>
<?php
$n = 15;
$str = "hello";
$array = array(1, 2, 3);
$newarray = compact("n", "str", "array");
print_r($newarray);
?>

<hr>
<?php
$result = print "ok";
echo $result . "<br>";
echo "I", "Love", "PHP" . "<br>";
?>

<hr>
<?php
printf("%b", 5);
printf("%c", 65);
echo "<br>";
printf("%b,%c,%.2f", 5, 65, 3.1415);
echo "<br>";
printf("%d,%o,%e,%x", 12, 20, 123.45, 345);
?>

<hr>
<?php
header("Content-Type:text/html;charset=utf8");
$str = "I Love PHP";
echo strlen($str);
echo strlen("中国");
?>

<hr>
<?php
$str = "I Love PHP";
echo strtolower($str) . "<br>";
echo strtoupper($str) . "<br>";
?>