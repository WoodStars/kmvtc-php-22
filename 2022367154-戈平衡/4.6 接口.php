//4.6 接口
<?php
header("content-type:text/html;charset=utf-8");
//定义animal接口
interface animal{
    function run();
    function shout();
}
//定义landannimal接口
interface landanimal{
    public function liveonland();
}
//定义dog类，实现animal接口和landanimal接口
class dog implements animal, landanimal{
    //实现接口中的抽象方法
    public function run();
echo "小狗在奔跑<br>";
}
public function shout() {
    echo "玩玩......<br>";
}
class cat implements animal, landanimal{
    public function run();
echo "小猫在奔跑<br>";
}
public function shout() {
    echo "兔兔......<br>";
}
$dog=new dog();
$dog->run();
$dog->shout();
$cat=new cat();
$cat->run();
?>