<?php
class animal
{
    public $name = "jjj";
    public $color = "";
    public $age = "";
    function getIfo()
    {
        return $this->name;
    }
    function setInfo($name)
    {
        $this->name = $name;
    }
}
$daina = new animal();
$crow = new animal();
$shark = new animal();
$daina->setInfo('戴拿');
$name = $daina->getIfo();
echo $name;
$crow->setInfo('霸王龙');
$name = $crow->getIfo();
echo $name;
$shark->setInfo('鲨鱼');
$name = $shark->getIfo();
echo $name;
?>

<?php
class hero
{
    public $name;
    public $color;
    public $skill;
    public $age;
    public function __construct($name, $color, $skill, $age)
    {
        $this->name = $name;
        $this->color = $color;
        $this->skill = $skill;
        $this->age = $age;
    }
    public function getInfo()
    {
        echo "光之国凹凸曼：" . $this->name . "<br>";
        echo "凹凸曼颜色：" . $this->color . "<br>";
        echo "凹凸曼技能：" . $this->skill . "<br>";
        echo "凹凸曼光龄：" . $this->age . "<br>";
    }
}
$aoteman1 = new hero("迪迦", "红蓝白", "手刀切割+空中飞踢+细胞转换光束", 10000);
$aoteman1->getInfo();
$aoteman2 = new hero('泰罗', '红白', '手刀切割+空中飞踢+细胞转换光束', 9999);
$aoteman2->getInfo();
?>