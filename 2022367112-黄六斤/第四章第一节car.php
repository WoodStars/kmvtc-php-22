<?php
class car {
    public $brand;
    public $drive;
    public $colour;
    public $price;

    public function __construct($brand,$drive,$colour,$price) {
        $this->brand = $brand;
        $this->drive = $drive;
        $this->colour = $colour;
        $this->price = $price;
    }
    public function getInfo() {
        echo "花的品牌:" . $this->brand . "<br>" ;
        echo "花的种类:" . $this->drive . "<br>" ;
        echo "花的颜色:" . $this->colour . "<br>" ;
        echo "花的价格:" . $this->price . "<br>" ;
    }
}
$BMW =new car('百何花','种类','白色','4w');
$BMW->getInfo();
$VW =new car('玫瑰花','种类','红色','15w');
$VW->getInfo();
$ROLLS =new car('四季花','种类','五颜六色','10w');
$ROLLS->getInfo();
$AUDI =new car('','荷花','种类','3w');
$AUDI->getInfo();
?>