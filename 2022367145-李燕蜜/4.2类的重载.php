<?php
//类的继承。新建一个【水果】子类继承水果类
class pium extends  fruit{
    //成员属性：红色
    public $wing;//fruit类自有的属性$wing
    //重载：继承类的方法的继承写法【paernt::】
    public function getInfo() {
        //使用【parent：：】来获取父类的成员方法或者属性
        parent: :getInfo();
        echo "红色水果有" . $this->wing . '红色<br>';
        $this->red();
    }
    //子类[fruit]具有的成员方法
    public function red(){
    //红色水果类的自有的方法
    echo "我是红色的!!!"；   
    }
    //继承类的构造函数的继承写法【parent：：】
    public function __construct($name, $color, $age, $red){
        paernt::__construct($name, $color, $age)；
        $this->red=$red;
    }

}
$crow=new fruit("李子"， "红色"，4,"好吃的")；
$croe->getInfo();
?>