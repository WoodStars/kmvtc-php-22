<?php

public function __get($property_name){
        // 检查属性 this-> 当前类  的 property_name 属性 是否存在
        if(isset($this->$property_name)){
            return (this->$property_name);
        }else {
            return(NULL);
        }
    }
    //4.3.2.2 set  (赋值)
    public __set($property_name,$value){
        $this->$property_name = $value;
    }

    //4.3.2.3 isset  (检查)
    private function __isset($property_name) {
        return isset($this->$property_name);
    }

    //4.3.2.4 unset  (删除)
    private function __unset($property_name) {
        return unset($this->$property_name);
    }
}

$panda = new animal();

/**
 * private[私有的]
 * 1、仅针对当前class享有访问权限
 * 2、当前类的子类不允许访问
 * 3、类的外部均不允许访问
 */

 //!! __get __set 方法写了以后就会自动对private属性取值或者赋值

 // todo 需要同学测试 将__get __set 方法注释以后运行的结果和不注释运行的结果，作比较
$panda->name="狗";
$panda->color="黑白";
$panda->age=12;

echo $panda->name;
echo $panda->color;
echo $panda->age;

//todo 在类的外部使用isset函数来判断实体的变量是否设定


//4.3.2.4 unset 测试用例
echo $panda->name;//"狗";
unset($panda->name);
echo $panda->name;// 空输出
