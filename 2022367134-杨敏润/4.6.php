<?php
header("content-type:text/html;charset=utf-8");
interface animal{
    function run();
    function shout();
}
interface landanimal{
    public function liveonland();
}
class dog implements animal,  landanimal{
    public function run() {
        echo "小狗在奔跑<br>";
    }
    publicfunction shout() {
        echo "旺旺·····<br>";
    }
    public function liveonland() {
        echo "小狗在陆地上生活<br>";
    }
}
$dog=new dog();
$dog->run();
$dog->shout();
$dog->liveonland();
?>
